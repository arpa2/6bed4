/* 6bed4tftp.c is a test program that sends a file using TFTP.
 *
 * This program compiles to binaries 6bed4send and 6bed4recv.
 *
 * Use this between any pair of IPv6 hosts to test traffic.
 * The lock-step nature of TFTP is helpful in analysing it.
 * Because messages are sent in both directions, the Seen flag
 * can also be transferred; this is more realistic than what
 * ping6 can do.  Furthermore, the Traffic Class in the IPv6
 * header will be set on outgoing TFTP packets, with options
 * to indicate the peering policy to use in 6bed4 routing.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>

#include <signal.h>
#include <errno.h>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <arpa/tftp.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/uio.h>
#include <sys/stat.h>
#include <sys/time.h>


#ifndef IPPORT_TFTP
#define IPPORT_TFTP 69
#endif

char *program = "6bed4send";

int send_traffic_class = -1;		/* -1 uses the kernel default */

int recv_traffic_class [5] = { 0, 0, 0, 0, 0 };		/* 4 for 6bed4, +1 for other */
int recv_seen = 0;
int recv_all = 0;

bool slowly = false;

char remot_name [INET6_ADDRSTRLEN+1+5+1];
uint8_t remot_ipv6 [16];
struct sockaddr_in6 remot_sockaddr;

char local_name [INET6_ADDRSTRLEN+1+5+1];
uint8_t local_ipv6 [16];
struct sockaddr_in6 local_sockaddr;

/* Initialise the local_ or remot_ values from a socket address.
 */
void setup_peeraddr (char *opt_info, struct sockaddr_in6 *in,
			uint8_t *out_ip6, char *out_name) {
	assert (in->sin6_family == AF_INET6);
	assert (in->sin6_port != 0);
	memcpy (out_ip6, &in->sin6_addr, 16);
	inet_ntop (AF_INET6, out_ip6, out_name, INET6_ADDRSTRLEN);
	int port = htons (in->sin6_port);
	if (port != IPPORT_TFTP) {
		sprintf (out_name + strlen (out_name), " %d", port);
	}
	if (opt_info != NULL) {
		printf ("%s %s\n", opt_info, out_name);
	}
}

/* The last command is buffered, so it can deliver strings in argv[] style */
#define CMD_ARGN 10
uint8_t last_cmd [1024+1];
struct msghdr *last_mh = NULL;
int last_argc = 0;
char *last_argv [CMD_ARGN];

int datablk = 0;
uint8_t databuf [512];
int datalen = -1;


#define AS_SA (struct sockaddr *)
#define SZ_SA sizeof (struct sockaddr_in6)

#define REPT -1
#define LAST -2
#define TOUT -3


/* Suspension processing with single-step sending. */

bool suspended = false;
bool suspended_single_step = false;

void wait_if_suspended (void) {
	while (suspended && !suspended_single_step) {
		pause ();
	}
	if (suspended) {
		suspended_single_step = false;
	}
}


/* Character-based logging and future newline handling. */

bool char_logging = false;

void log_char (char c) {
	char_logging = true;
	write (1, &c, 1);
}

void log_command (int cmd) {
	switch (cmd) {
	case RRQ:
		log_char ('R');
		break;
	case WRQ:
		log_char ('W');
		break;
	case DATA:
		log_char ('.');
		break;
	case ACK:
		// log_char ('A');
		break;
	case ERROR:
		log_char ('E');
		break;
	case REPT:
		log_char ('+');
		break;
	case LAST:
		log_char ('!');
		break;
	case TOUT:
		log_char ('@');
		break;
	default:
		log_char ('?');
		break;
	}
}

void newline () {
	if (char_logging) {
		write (1, "\n", 1);
	}
	char_logging = false;
}


struct msghdr *makemsg (void *msg, int msglen, uint16_t cmd, void **traffic_class_memory) {
	assert (cmd < 6);
	//
	// Construct the message with a Traffic Class header
	static struct iovec iov [6];
	static uint8_t ancil [6] [CMSG_SPACE (sizeof (int))];
	static struct msghdr mh [6];
	mh [cmd].msg_name = AS_SA &remot_sockaddr;
	mh [cmd].msg_namelen = sizeof (remot_sockaddr);
	mh [cmd].msg_iov = &iov [cmd];
	mh [cmd].msg_iovlen = 1;
	mh [cmd].msg_control = &ancil [cmd];
	mh [cmd].msg_controllen = sizeof (ancil [cmd]);
	mh [cmd].msg_flags = 0;
	iov [cmd].iov_base = msg;
	iov [cmd].iov_len  = msglen;
	struct cmsghdr *cmh = CMSG_FIRSTHDR (&mh [cmd]);
	cmh->cmsg_len = CMSG_LEN (sizeof (int));
	cmh->cmsg_level = IPPROTO_IPV6;
	cmh->cmsg_type = IPV6_TCLASS;
	*traffic_class_memory = CMSG_DATA (cmh);
	//
	// Return this as the last message constructed
	return &mh [cmd];
}


void classify_traffic (void *traffic_class_memory) {
	int rtc;
	memcpy (&rtc, traffic_class_memory, sizeof (int));
	if (rtc == -1) {
		rtc = 0;
	} else if ((rtc < 0) || (rtc > 255)) {
		newline ();
		fprintf (stderr, "Impossible header value Traffic Class %d\n", rtc);
		exit (1);
	}
	recv_all++;
	if ((rtc & 0x1c) != 0x00) {
		recv_traffic_class [4]++;
	} else {
		recv_traffic_class [rtc >> 6]++;
		if ((rtc & 0x20) != 0x00) {
			recv_seen++;
		}
	}
}


bool await_recv (int sox) {
	//
	// Setup for waiting for input -or- data
	fd_set fds;
	FD_ZERO (&fds);
	FD_SET (sox, &fds);
	struct timeval tout;
	tout.tv_sec = 0;
	tout.tv_usec = 750000;
	//
	// Now wait for data or timeout
	if (select (sox+1, &fds, NULL, NULL, &tout) == -1) {
		perror ("Error waiting for input");
		exit (1);
	}
	//
	// Return whether data is available
	return FD_ISSET (sox, &fds);
}


void send_ack (int sox) {
	//
	// Construct the message
	uint16_t msg [2];
	msg [0] = htons (ACK);
	msg [1] = htons (datablk);
	void *tcbuf;
	struct msghdr *mh = makemsg (msg, sizeof (msg), ACK, &tcbuf);
	memcpy (tcbuf, &send_traffic_class, sizeof (int));
	//
	// Send the ACK message -- no frills, because this is just ACK
	log_command (ACK);
	wait_if_suspended ();
	sendmsg (sox, mh, 0);
}


bool recv_ack (int sox, bool accept_RRQ) {
	uint8_t msg [1024+1];
	if (!await_recv (sox)) {
		log_command (TOUT);
		return false;
	}
	socklen_t sz = sizeof (remot_name);
	void *tcbuf;
	struct msghdr *mh = makemsg (msg, sizeof (msg), ACK, &tcbuf);
	int acksz;
again:
	acksz = recvmsg (sox, mh, 0);
	if (acksz == -1) {
		if (errno == ECONNREFUSED) {
			await_recv (sox);
			goto again;
		}
		newline ();
		perror ("Error instead of ACK");
		exit (1);
	}
	classify_traffic (tcbuf);
	if (acksz < 4) {
		newline ();
		fprintf (stderr, "Received ACK is too small, got %d/4\n", acksz);
		exit (1);
	}
	int cmd = htons (* (uint16_t *) msg);
	log_command (cmd);
	if (accept_RRQ && (cmd == RRQ)) {
		newline ();
		printf ("Accepting simultaneous connect (RRQ in response to WRQ)\n");
		return true;
	} else if (cmd != ACK) {
		newline ();
		fprintf (stderr, "Expected ACK\n");
		exit (1);
	}
	int recvblk = htons (* (uint16_t *) (msg + 2));
	if (recvblk < datablk) {
		return false;
	} else if (recvblk > datablk) {
		newline ();
		fprintf (stderr, "ACK on future block, expected %d got %d\n", datablk, recvblk);
		exit (1);
	}
	return true;
}


void send_request (int sox, int cmd, char *filename) {
	//
	// Construct the command message in last_cmd
	int fnlen = strlen (filename);
	int cmdlen = 2 + fnlen + 1 + 5 + 1;
	if (cmdlen > sizeof (last_cmd)) {
		fprintf (stderr, "File name too long to send\n");
		exit (1);
	}
	uint8_t msg [2 + fnlen + 1 + 5 + 1];
	* (uint16_t *) last_cmd = htons (cmd);
	strcpy (last_cmd + 2, filename);
	strcpy (last_cmd + 2 + fnlen + 1, "octet");
	void *tcbuf;
	last_mh = makemsg (last_cmd, cmdlen, cmd, &tcbuf);
	memcpy (tcbuf, &send_traffic_class, sizeof (int));
	//
	// Send the command message to the remote
	do {
		log_command (cmd);
		wait_if_suspended ();
		sendmsg (sox, last_mh, 0);
		if (cmd == RRQ) {
			break;	/* DATA will be the acknowledgement */
		}
		cmd = REPT;
	} while (!recv_ack (sox, true));
}


void recv_request (int sox, int cmd) {
	//
	// Reset the last-command parsed information
	last_argc = 0;
	memset (last_argv, 0, sizeof (last_argv));
	//
	// Receive the command message from a remote
	void *tcbuf;
	int recv;
again:
	last_mh = makemsg (last_cmd, sizeof (last_cmd), cmd, &tcbuf);
	recv = recvmsg (sox, last_mh, 0);
	if (recv < 0) {
		if (errno == ECONNREFUSED) {
			recv = 0;
			/* handled below */
		} else {
			perror ("Error on receiving");
			exit (1);
		}
	}
	classify_traffic (tcbuf);
	if (recv == 0) {
		await_recv (sox);
		goto again;
	}
	if (recv > 1024) {
		fprintf (stderr, "Received more than expected\n");
		exit (1);
	}
	if ((recv < 3) || (last_cmd [recv-1] != '\0')) {
		fprintf (stderr, "Command format error\n");
		exit (1);
	}
	setup_peeraddr ("Receiving from", &remot_sockaddr, remot_ipv6, remot_name);
	//
	// Process and print the command
	int incmd = ntohs (* (uint16_t *) last_cmd);
	log_command (incmd);
	if (incmd != cmd) {
		newline ();
		fprintf (stderr, "Wrong command: expected %d, got %d\n", cmd, incmd);
		exit (1);
	}
	//
	// Acknowledge proper reception for WRQ so that sending may commence
	if (cmd == WRQ) {
		send_ack (sox);
	}
	//
	// Parse the arguments into last_argc, last_argv
	int ofs = 2;
	while (ofs < recv) {
		if (last_argc >= CMD_ARGN) {
			newline ();
			fprintf (stderr, "Unexpected got more than %d command words\n", CMD_ARGN);
		}
		last_argv [last_argc++] = last_cmd + ofs;
		ofs += strlen (last_cmd + ofs) + 1;
	}
}


bool send_data (int sox, int fd) {
	//
	// Read the next block from file
	datablk++;
	datalen = read (fd, databuf, 512);
	if (datalen < 0) {
		perror ("Error reading from file");
		exit (1);
	}
	//
	// Construct the command to send
	uint8_t msg [2 + 2 + 512];
	* (uint16_t *) (msg + 0) = htons (DATA);
	* (uint16_t *) (msg + 2) = htons (datablk);
	memcpy (msg + 4, databuf, datalen);
	void *tcbuf;
	struct msghdr *mh = makemsg (msg, 4 + datalen, DATA, &tcbuf);
	memcpy (tcbuf, &send_traffic_class, sizeof (int));
	//
	// Send the data block until acknowledged
	int cmd = (datalen < 512) ? LAST : DATA;
	do {
		log_command (cmd);
		wait_if_suspended ();
		sendmsg (sox, mh, 0);
		cmd = REPT;
	} while (!recv_ack (sox, false));
	//
	// Return whether there is more to send
	return datalen == 512;
}


bool recv_data (int sox, int fd) {
	//
	// Receive the next transfer of a data block
	uint8_t msg [1024+1];
wait_again:
	while (!await_recv (sox)) {
		/* Timeout; resend ACK (or command) to trigger resend */
		if (datablk == 0) {
			sendmsg (sox, last_mh, 0);
		} else {
			send_ack (sox);
		}
	}
	void *tcbuf;
	struct msghdr *mh;
	int recv;
again:
	mh = makemsg (msg, sizeof (msg), DATA, &tcbuf);
	recv = recvmsg (sox, mh, 0);
	if (recv < 0) {
		if (errno == ECONNREFUSED) {
			log_char ('X');
			goto wait_again;
		} else {
			newline ();
			perror ("Nothing received");
			exit (1);
		}
	}
	classify_traffic (tcbuf);
	if (recv == 0) {
		log_char ('Y');
		goto wait_again;
	}
	if (recv < 4) {
		newline ();
		fprintf (stderr, "Received too little to form a DATA block\n");
		exit (1);
	}
	int cmd = htons (* (uint16_t *) msg);
	if ((datablk == 0) && (cmd == WRQ)) {
		newline ();
		printf ("Accepting simultaneous connect (WRQ in response to RRQ)\n");
		send_ack (sox);
		goto again;
	}
	if (cmd != DATA) {
		newline ();
		fprintf (stderr, "Not a DATA block (got %d instead of %d) in %d bytes\n", cmd, DATA, recv);
		exit (1);
	}
	if (recv > 4 + 512) {
		newline ();
		fprintf (stderr, "Received DATA has unexpected size, wanted 4..516 got %d\n", recv);
		exit (1);
	}
	//
	// Check the protocol data
	datablk++;
	int recvblk = ntohs (* (uint16_t *) (msg + 2));
	if (recvblk < datablk) {
		// It looks like our ACK was missed
		datablk--;
		send_ack (sox);
		goto wait_again;
	} else if (recvblk > datablk) {
		newline ();
		fprintf (stderr, "Wrong block number, expected %d got %d\n", datablk, recvblk);
		exit (1);
	}
	int datalen = recv - 4;
	cmd = (datalen == 512) ? DATA : LAST;
	log_command (cmd);
	//
	// Write the block to file
	if (write (fd, msg + 4, datalen) != datalen) {
		newline ();
		fprintf (stderr, "Incomplete file write\n");
		exit (1);
	}
	//
	// Acknowledge reception
	send_ack (sox);
	//
	// Return whether there is more to be read
	return cmd != LAST;
}


/* Print statistics about the Traffic Class.
 */
void statistics (void) {
	//
	// Report traffic classes as received
	newline ();
	char *hdr = "Received packets in Traffic Classes:\n";
	char *tcs [] = { "--proper-peering (0)", "--prohibited-peering (1)", "--presumptious-peering (2)", "--persistent-peering (3)", "(non-6bed4)" };
	for (int i=0; i<5; i++) {
		if (recv_traffic_class [i] > 0) {
			printf ("%s%5d %s\n", hdr, recv_traffic_class [i], tcs [i]);
			hdr = "";
		}
	}
	printf ("Seen flagged on %d out of %d packets\n", recv_seen, recv_all);
}


/* Signal Handling.  TODO: Does not seem to work for anything but ctrl_c.
 *   Ctrl-C -> stops after printing statistics.
 *   Ctrl-S -> suspends sending or, if already suspended, sends one message.
 *   Ctrl-Q -> resumes sending.
 *   Ctrl-? -> prints statistics.
 */

void ctrl_u (int signum) {
	printf ("^U");
	statistics ();
}

void ctrl_c (int signum) {
	printf ("\n");
	statistics ();
	exit (1);
}

void ctrl_s (int signum) {
	printf ("^S");
	if (suspended) {
		suspended_single_step = true;
	} else {
		suspended = true;
	}
	statistics ();
}

void ctrl_q (int signum) {
	printf ("^Q");
	if (suspended) {
		suspended = false;
		suspended_single_step = false;
	} else {
		statistics ();
	}
}


/* Option descriptions, both short and long */

char *short_opt = "0123";

struct option long_opt [] = {
	{ "proper-peering", 0, NULL, '0' },
	{ "prohibited-peering", 0, NULL, '1' },
	{ "presumptious-peering", 0, NULL, '2' },
	{ "persistent-peering", 0, NULL, '3' },
	{ NULL, 0, NULL, 0 }	/* Array termination */
};

/* Parse host and/or port in argv[0] and possibly argv[1], and return how many
 * args were parsed, with zero for nothing.  What is found goes into saddr, to
 * override the default.  Parsing errors report 0.
 */
int args2peeraddr (int argc, char *argv [], struct sockaddr_in6 *saddr) {
	int argi = 0;
	//
	// First try parsing an address
	if ((argc > argi) && (strchr (argv [argi], ':')) != NULL) {
		if (inet_pton (AF_INET6, argv [argi++], &saddr->sin6_addr) != 1) {
			// Syntax error
			return 0;
		}
	}
	//
	// Then try parsing a port
	if ((argc > argi) && (strchr (argv [argi], ':')) == NULL) {
		char *endptr = argv [argi];
		if (*endptr == '\0') {
			// Syntax error
			return 0;
		}
		unsigned long port = strtoul (argv [argi++], &endptr, 10);
		if (*endptr != '\0') {
			// Syntax error
			return 0;
		}
		if ((port == 0) || (port > 65535)) {
			// Invalid port number
			return 0;
		}
		saddr->sin6_port = htons (port);
	}
	//
	// Return the number of parameters that we managed to parse
	return argi;
}

int main (int argc, char *argv []) {
	//
	// Store the program name for printing and action choice
	program = strrchr (argv [0], '/');
	if (program != NULL) {
		program++;
	} else {
		program = argv [0];
	}
	//
	// Parse commandline options
	bool gotmore = true;
	int argi;
	while (gotmore) {
		int opt = getopt_long (argc, argv, short_opt, long_opt, NULL);
		switch (opt) {
		case -1:
			gotmore = false;
			argi = optind;
			break;
		case '0':
		case '1':
		case '2':
		case '3':
			if (send_traffic_class != -1) {
				fprintf (stderr, "You can only set one Traffic Class\n");
				exit (1);
			}
			send_traffic_class = (opt - '0') << 6;
			break;
		default:
			fprintf (stderr, "Unknown option %s\n", argv [optind-1]);
			exit (1);
			break;
		}
	}
	//
	// Parse remaining commandline arguments
	if ((argc - argi < 1) || (argc - argi > 4)) {
		fprintf (stderr, "Usage: %s [options] file [local_peer] [remote_peer]\n where a peer is a host IPv6 address and/or a port, and options are:\n -0 or --proper-peering, -1 of --prohibited-peering, -2 or --presumptious-peering,\n -3 or --persistent-peering select the outbound Traffic Class\n", program);
		exit (1);
	}
	bool sending = (strstr (program, "send") != NULL);
	char *filename = argv [argi++];
	if (strcmp (filename, "-") == 0) {
		filename = "/dev/zero";
		slowly = true;
	}
	struct sockaddr_in6 peers [2];
	memset (peers, 0, sizeof (peers));
	peers [0].sin6_family = peers [1].sin6_family = AF_INET6;
	peers [0].sin6_port   = peers [1].sin6_port  = htons (IPPORT_TFTP);
	peers [1].sin6_addr.s6_addr [15] = 1;
	int numpeers = 0;
	while (argi < argc) {
		if (numpeers == 2) {
			fprintf (stderr, "More than two addresses are no supported\n");
			exit (1);
		}
		int argx = args2peeraddr (argc - argi, argv + argi, &peers [numpeers++]);
		if (argx == 0) {
			fprintf (stderr, "Syntax error for peer in %s\n", argv [argi]);
			exit (1);
		}
		argi += argx;
	}
	bool active = numpeers > 0;
	bool bound  = numpeers > 1;
	//
	// Setup signal handlers -- TODO: Does not seem to work
	signal (SIGINT,  ctrl_c);
	signal (SIGSTOP, ctrl_s);
	signal (SIGTSTP, ctrl_s);
	signal (SIGCONT, ctrl_q);
	signal (SIGQUIT, ctrl_u);
	//
	// Open the commandline-named file for reading or writing
	int fileh = open (filename, sending ? (O_RDONLY) : (O_WRONLY|O_TRUNC|O_CREAT));
	if (fileh < 0) {
		perror ("Could not open file");
		exit (1);
	}
	//
	// Open a socket, in client or server mode
	int sox = socket (AF_INET6, SOCK_DGRAM, 0);
	if (sox < 0) {
		perror ("No socket");
		exit (1);
	}
	int on = 1;
	if (setsockopt (sox, IPPROTO_IPV6, IPV6_TCLASS, &on, sizeof (on)) != 0) {
		perror ("Cannot send traffic class");
		exit (1);
	}
	if (setsockopt (sox, IPPROTO_IPV6, IPV6_RECVTCLASS, &on, sizeof (on)) != 0) {
		perror ("Cannot receive traffic class");
		exit (1);
	}
	//
	// Setup the locally bound address
	if (bound) {
		memcpy (&local_sockaddr, &peers [0], SZ_SA);
	} else {
		memset (&local_sockaddr, 0, SZ_SA);
		local_sockaddr.sin6_port = IPPORT_TFTP;
	}
	if (bind (sox, AS_SA &peers [0], SZ_SA) != 0) {
		perror ("Failed to bind");
		exit (1);
	}
	socklen_t sz = SZ_SA;
	if (getsockname (sox, AS_SA &local_sockaddr, &sz) != 0) {
		perror ("Failed to get local address");
		exit (1);
	}
	setup_peeraddr ("Listening on", &local_sockaddr, local_ipv6, local_name);
	//
	// Setup the remote peer address, if any
	if (active) {
		memcpy (&remot_sockaddr, &peers [numpeers-1], SZ_SA);
		setup_peeraddr ("Sending to", &remot_sockaddr, remot_ipv6, remot_name);
	} else {
		memset (&remot_sockaddr, 0, SZ_SA);
		remot_sockaddr.sin6_family = AF_INET6;
		remot_sockaddr.sin6_port = IPPORT_TFTP;
		memset (&remot_ipv6, 0, 16);
		strcpy (remot_name, "(awaiting reqeust)");
	}
	//
	// Exchange the command request (be active or passive)
	if (active) {
		int cmd = sending ? WRQ : RRQ;
		send_request (sox, cmd, filename);
	} else {
		//
		// Wait for incoming traffic
		int cmd = sending ? RRQ : WRQ;
		recv_request (sox,  cmd);
	}
	//
	// Perform the actual data exchange
	if (sending) {
		while (send_data (sox, fileh)) {
			if (slowly) {
				usleep (750000);
			}
		}
	} else {
		while (recv_data (sox, fileh)) {
			if (slowly) {
				usleep (750000);
			}
		}
	}
	statistics ();
	//
	// Cleanup and return from main()
	newline ();
	if (fileh >= 0) {
		close (fileh);
		fileh = -1;
	}
	if (sox >= 0) {
		close (sox);
		sox = -1;
	}
	return 0;
}

