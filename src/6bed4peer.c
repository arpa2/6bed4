/* 6bed4/peer.c -- Peer-to-Peer IPv6-anywhere with 6bed4 -- peer.c
 *
 * This is an implementation of neighbour and router discovery over a
 * tunnel that packs IPv6 inside UDP/IPv4.  This tunnel mechanism is
 * so efficient that the server administrators need not mind if it is
 * distributed widely.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <assert.h>

#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <getopt.h>
#include <fcntl.h>
#include <time.h>

#include <signal.h>
#include <syslog.h>
#ifndef LOG_PERROR
#define LOG_PERROR LOG_CONS		/* LOG_PERROR is non-POSIX, LOG_CONS is */
#endif

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/select.h>
#include <sys/ioctl.h>

#include <net/if.h>

#define _GNU_SOURCE
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/ip6.h>
#include <netinet/udp.h>
#include <netinet/icmp6.h>
#include <arpa/inet.h>

#include <asm/types.h>
//#include <linux/if.h>
#include <linux/if_tun.h>
#include <linux/if_ether.h>
#include <linux/netlink.h>
#include <linux/rtnetlink.h>

/* The following will initially fail, due to an IANA obligation to avoid
 * default builds with non-standard options.
 */
#include "nonstd.h"

/* The UThash.h and UTlist.h headers support hash table and list.
 * They are not _completely_ optimal, as we'd rather split the prev,next
 * doubly-linked list that comes with the hash, but the simplicity is
 * more of a concern for now, so we'll accept the storage overhead.
 */
#include "utlist.h"
#include "uthash.h"


#define MTU 1280
#define PREFIX_SIZE 114


/*
 * Peering Policies, as defined by the TRAFFIC-CLASS.MD document.
 *  - "proper" peering uses 6bed4router as a fallback, but tries to
 *    switch to direct peering as soon as possible;
 *  - "prohibited" peering always goes through the 6bed4router and
 *    may deliver higher delay but less jitter;
 *  - "presumptions" peering aims for lower delay by trying more
 *    agressively to connect to the peer, falling back only after
 *    a few seconds of failure;
 *  - "persistent" peering refuses to go through the 6bed4router,
 *    and will send occasional ICMP errors after a few seconds of
 *    failure; it should have the lowest delay and jitter and the
 *    highest privacy, but at the risk of maybe not connecting
 *    properly to all peers; close to Teredo, in fact.
 */
#define PP_PROPER       ((uint8_t) 0x00)
#define PP_PROHIBITED   ((uint8_t) 0x40)
#define PP_PRESUMPTIOUS ((uint8_t) 0x80)
#define PP_PERSISTENT   ((uint8_t) 0xc0)


/*
 * The HAVE_SETUP_TUNNEL variable is used to determine whether absense of
 * the -d option leads to an error, or to an attempt to setup the tunnel.
 * The setup_tunnel() function used for that is defined per platform, such
 * as for LINUX.  Remember to maintain the manpage's optionality for -d.
 */
#undef HAVE_SETUP_TUNNEL


#ifndef MAX_ROUTABLE_PREFIXES
#define MAX_ROUTABLE_PREFIXES 10
#endif


/* Global variables */

/* Path constants, pointing to (Linux) system-configuration programs. */
static const char sbin_ip[] = LOCAL_SBIN_IP;
static const char sbin_sysctl[] = LOCAL_SBIN_SYSCTL;

char *program;
char *exthook;

volatile int signalnum = 0;

int v4sox = -1;
int v6sox = -1;

uint8_t v4qos = 0;		/* Current QoS setting on UDP/IPv4 socket */
uint8_t v6tc = 0;		/* Current QoS used by the IPv6 socket */
uint8_t v4ttl = 64;		/* Default TTL setting on UDP/IPv4 socket */
int v4ttl_mcast = -1;		/* Multicast TTL for LAN explorations */

char *v4server = NULL;
char v6prefix [INET6_ADDRSTRLEN];
uint8_t v6lladdr [6];

struct in6_addr v6route_addr [MAX_ROUTABLE_PREFIXES];
uint8_t         v6route_pfix [MAX_ROUTABLE_PREFIXES];
int             v6route_count = 0;

const uint8_t v6listen_linklocal [16] = { 0xfe, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
uint8_t v6listen_linklocal_complete [16] = { 0xfe, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

struct sockaddr_in  v4name;
struct sockaddr_in  v4peer;
struct sockaddr_in6 v6name;

struct sockaddr_in v4bind;
struct sockaddr_in v4allnodes;

struct in6_addr v6listen;
//TODO:NEEDNOT// struct in6_addr v6listen_complete;
struct in_addr  v4listen;


struct {
	struct tun_pi tun;
	union {
		struct {
			struct ip6_hdr v6hdr;
			uint8_t data [MTU - sizeof (struct ip6_hdr)];
		} idata;
		struct {
			struct ip6_hdr v6hdr;
			struct icmp6_hdr v6icmphdr;
		} ndata;
	} udata;
} __attribute__((packed)) v4data6;

#define v4tunpi6 	(v4data6.tun)
#define v4data		((uint8_t *) &v4data6.udata)
#define v4hdr6		(&v4data6.udata.idata.v6hdr)
#define v4src6		(&v4data6.udata.idata.v6hdr.ip6_src)
#define v4dst6		(&v4data6.udata.idata.v6hdr.ip6_dst)

#define v4v6trfcls	((ntohs (* (uint16_t *) &v4data6.udata.ndata.v6hdr) >> 4) & 0xff)
#define v4v6plen	(v4data6.udata.ndata.v6hdr.ip6_plen)
#define v4v6nexthdr	(v4data6.udata.ndata.v6hdr.ip6_nxt)
#define v4v6hoplimit	(v4data6.udata.ndata.v6hdr.ip6_hops)

#define v4icmp6		(&v4data6.udata.ndata.v6icmphdr)
#define v4v6icmpdata	(v4data6.udata.ndata.v6icmphdr.icmp6_data8)
#define v4v6icmptype	(v4data6.udata.ndata.v6icmphdr.icmp6_type)
#define v4v6icmpcode	(v4data6.udata.ndata.v6icmphdr.icmp6_code)
#define v4v6icmpcksum	(v4data6.udata.ndata.v6icmphdr.icmp6_cksum)
#define v4v6ndtarget	(&v4data6.udata.ndata.v6icmphdr.icmp6_data8 [4])


struct {
	struct tun_pi tun;
	union {
		uint8_t data [MTU];
		struct {
			struct ip6_hdr v6hdr;
			struct icmp6_hdr v6icmp;
		} __attribute__((packed)) ndata;
	} udata;
}  __attribute__((packed)) v6data6;

#define v6tuncmd	(v6data6.tun)
#define v6data		(v6data6.udata.data)
#define v6hdr6		(&v6data6.udata.ndata.v6hdr)
#define v6trfcls	((ntohs (* (uint16_t *) &v6data6.udata.ndata.v6hdr) >> 4) & 0xff)
#define v6hops		(v6data6.udata.ndata.v6hdr.ip6_hops)
#define v6type		(v6data6.udata.ndata.v6hdr.ip6_nxt)
#define v6plen		(v6data6.udata.ndata.v6hdr.ip6_plen)
#define v6src6		(&v6data6.udata.ndata.v6hdr.ip6_src)
#define v6dst6		(&v6data6.udata.ndata.v6hdr.ip6_dst)
#define v6icmp6type	(v6data6.udata.ndata.v6icmp.icmp6_type)
#define v6icmp6code	(v6data6.udata.ndata.v6icmp.icmp6_code)
#define v6icmp6data	(v6data6.udata.ndata.v6icmp.icmp6_data8)
#define v6icmp6csum	(v6data6.udata.ndata.v6icmp.icmp6_cksum)
#define v6ndtarget	(&v6data6.udata.ndata.v6icmp.icmp6_data16[2])

#define HDR_SIZE        (sizeof(struct tun_pi))

/* The time for the next scheduled maintenance: routersol or keepalive.
 * The milliseconds are always 0 for maintenance tasks.
 */
time_t maintenance_time_cycle = 0;
time_t maintenance_time_cycle_max = 30;
int32_t maintenance_next;
bool got_lladdr = false;
time_t keepalive_period = 30;
time_t keepalive_ttl = -1;



/*
 * Peer NAT is represented with two timers and a state.  The state is
 * composed in groups that enable quick inspection.
 *
 * Timers are close to local time and are stored with millisecond
 * resolution and wrap-around.  They can be set to expire by adding
 * milliseconds to "now", and tested by subtracting "now" later.
 * A positive subtraction outcome counts as expiration.  Note how
 * this relies on C/assembly wrap-around of integers on subtraction!
 */
#define PNS_COUNT_MASK		0x0f
#define PNS_LOOP_25S		0x10
#define PNS_DIRECT		0x20
#define PNS_FAILED		0x40
//
#define PNS_COUNTER_DONE(c) (((c+1) & PNS_COUNT_MASK) == 0)
#define PNS_COUNTER(c) ((c) & PNS_COUNT_MASK)
//
#define PNS_INIT0 ( PNS_COUNTER(-2)                             )
#define PNS_INIT1 ( PNS_COUNTER(-1)                             )
#define PNS_FAIL0 ( PNS_COUNTER(-2) | PNS_LOOP_25S | PNS_FAILED )
#define PNS_FAIL1 ( PNS_COUNTER(-1) | PNS_LOOP_25S | PNS_FAILED )
#define PNS_POLL0 ( PNS_COUNTER(-2)                | PNS_DIRECT )
#define PNS_POLL1 ( PNS_COUNTER(-1)                | PNS_DIRECT )
#define PNS_PEER  ( PNS_COUNTER(-1) | PNS_LOOP_25S | PNS_DIRECT )
//
struct pns_t {
	/*
	 * The current state.  By default, increment the state and
	 * perform a minor action and schedule at the back of the
	 * same queue.  When the counter wraps, perform a major
	 * action and expect to change state.
	 */
	uint8_t pns_state;
	/*
	 * Timers for a few purposes.
	 *  - pns_state_until expires when the state should change
	 *  - pns_seen_until  marks how long Seen can be sent
	 *  - pns_last_send   marks the timing of the last 4to6 packet
	 *  - pns_last_direct marks the last direct pns_last_send
	 *  - pns_icmp_wait   limits ICMPv6 error rate until a time
	 */
	int32_t pns_state_until;
	int32_t pns_seen_until;
	int32_t pns_last_send;
	int32_t pns_last_direct;
	int32_t pns_icmp_wait;
	/*
	 * The hash structure, built around the key pair formed
	 * by an IPv4 address and a UDP port number.  This is how
	 * peers are identified in this queue, and how their state
	 * will be found and updated.  Together these are known as
	 * the "link local address" for the peer.
	 */
	UT_hash_handle hh;
	uint8_t pns_ip4udp [6];
	/*
	 * The timer queue for pns_state is always active.  When
	 * it expires, a new state is entered and the corresponding
	 * queue is appended with this structure.  The queue is
	 * doubly linked for constant-time removal of elements.
	 */
	struct pns_t *pns_next, *pns_prev;
};

/* The hash entry to find peers' NAT states.
 */
struct pns_t *pns_hash = NULL;

/* The queue head and tail for the 01s/25s queues.
 */
struct pns_t  *pns_q01s = NULL;
struct pns_t  *pns_q25s = NULL;



/* The network packet structure of a 6bed4 Router Solicitation */

uint8_t ipv6_router_solicitation [] = {
	// IPv6 header
	0x60, 0x00, 0x00, 0x00,
	16 >> 8, 16 & 0xff, IPPROTO_ICMPV6, 255,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,		 // unspecd src
	0xff, 0x02, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x02, // all-rtr tgt
	// ICMPv6 header: router solicitation
	ND_ROUTER_SOLICIT, 0, 0x7a, 0xae,	// Checksum courtesy of WireShark :)
	// ICMPv6 body: reserved
	0, 0, 0, 0,
	// ICMPv6 option: source link layer address 0x0001 (end-aligned)
	0x01, 0x01, 0, 0, 0, 0, 0x00, 0x01,
};

uint8_t ipv6_defaultrouter_neighbor_advertisement [] = {
	// IPv6 header
	0x60, 0x00, 0x00, 0x00,
	32 >> 8, 32 & 0xff, IPPROTO_ICMPV6, 255,
	0xfe, 0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	// src is default router
	0xff, 0x02, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x01,// dst is all-nodes multicast, portable?
	// ICMPv6 header: neighbor solicitation
	ND_NEIGHBOR_ADVERT, 0, 0x36, 0xf2,		// Checksum courtesy of WireShark :)
	// ICMPv6 Neighbor Advertisement: flags
	0x40, 0, 0, 0,
	// Target: fe80::
	0xfe, 0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,	// the targeted neighbor
	// ICMPv6 option: target link layer address
	2, 1,
	0,0, 0,0,0,0 /* pointed-at by lladdr, below, and filled later */
};

uint8_t *lladdr = ipv6_defaultrouter_neighbor_advertisement + 40 + 4 + 4 + 16 + 2;

//TODO// Figure out if this has no overlap; (but note, lladdr is udpipv4)
uint8_t ip4udp_6bed4router [6];

#ifdef OLD_STYLE_ROUTER_ADVERTISEMENT
uint8_t router_linklocal_address [] = {
	0xfe, 0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x00, 0x00,
};
#endif

//TODO// Complete with the if-id of the 6bed4 Router:
uint8_t router_linklocal_address_complete [] = {
	0xfe, 0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x00, 0x00,
};

uint8_t client1_linklocal_address [] = {
	0xfe, 0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x00, 0x01,
};

uint8_t allnodes_linklocal_address [] = {
	0xff, 0x02, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x00, 0x01,
};

#if INCLUDE_UNUSED_OLD
uint8_t allrouters_linklocal_address [] = {
	0xff, 0x02, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x00, 0x02,
};
#endif

#if INCLUDE_UNUSED_OLD
uint8_t solicitednodes_linklocal_prefix [13] = {
	0xff, 0x02, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x01, 0xff
};
#endif

bool default_route = false;

bool foreground = false;

bool log_to_stderr = false;

bool multicast = true;


/*
 *
 * Driver routines
 *
 */

#ifndef INTERFACE_NAME_6BED4
#define INTERFACE_NAME_6BED4 "6bed4"
#endif
char *interface_name = INTERFACE_NAME_6BED4;

#ifdef LINUX
#define HAVE_SETUP_TUNNEL
static struct ifreq ifreq;
static bool have_tunnel = false;
/* Implement the setup_tunnel() command for Linux.
 * Return true on success, false on failure.
 */
bool setup_tunnel (void) {
	if (v6sox == -1) {
		v6sox = open ("/dev/net/tun", O_RDWR);
	}
	if (v6sox == -1) {
		syslog (LOG_ERR, "%s: Failed to access tunnel driver on /dev/net/tun: %s\n", program, strerror (errno));
		return 0;
	}
	bool ok = true;
	int flags = fcntl (v6sox, F_GETFL, 0);
	if (flags == -1) {
		syslog (LOG_CRIT, "Failed to retrieve flags for the tunnel file descriptor: %s\n", strerror (errno));
		ok = false;
	}
	if (!have_tunnel) {
		memset (&ifreq, 0, sizeof (ifreq));
		strncpy (ifreq.ifr_name, interface_name, IFNAMSIZ);
		ifreq.ifr_flags = IFF_TUN;
		if (ok && (ioctl (v6sox, TUNSETIFF, (void *) &ifreq) == -1)) {
			syslog (LOG_CRIT, "Failed to set interface name: %s\n", strerror (errno));
			ok = false;
		} else {
			have_tunnel = ok;
		}
		ifreq.ifr_name [IFNAMSIZ-1] = 0; /* ensure NUL termination, strncpy() does not guarantee it */
		ifreq.ifr_ifindex = if_nametoindex (ifreq.ifr_name);
		syslog (LOG_DEBUG, "Found Interface Index %d for name %s\n", ifreq.ifr_ifindex, ifreq.ifr_name);
		ok = ok & (ifreq.ifr_ifindex != 0);
	}
	char cmd [512];
	snprintf (cmd, sizeof(cmd), "%s -q -w net.ipv6.conf.%s.forwarding=0", sbin_sysctl, ifreq.ifr_name);
	if (ok && system (cmd) != 0) {
		fprintf (stderr, "Failed command: %s\n", cmd);
		ok = false;
	}
	snprintf (cmd, sizeof(cmd), "%s -q -w net.ipv6.conf.%s.accept_dad=0", sbin_sysctl, ifreq.ifr_name);
	if (ok && system (cmd) != 0) {
		fprintf (stderr, "Failed command: %s\n", cmd);
		ok = false;
	}
	if (!ok) {
		close (v6sox);	/* This removes the tunnel interface */
		v6sox = -1;
	}
	return ok;
}

bool setup_tunnel_address (void) {
	bool ok = have_tunnel;
	char cmd [512];

	snprintf (cmd, sizeof(cmd), "%s -q -w net.ipv6.conf.%s.autoconf=0", sbin_sysctl, ifreq.ifr_name);
	if (ok && system (cmd) != 0) {
		ok = 0;
	}
	snprintf (cmd, sizeof(cmd), "%s link set %s up mtu %d", sbin_ip, ifreq.ifr_name, MTU);
	if (ok && system (cmd) != 0) {
		fprintf (stderr, "Failed command: %s\n", cmd);
		ok = false;
	}
	snprintf (cmd, sizeof(cmd), "%s -6 addr add %s/114 dev %s", sbin_ip, v6prefix, ifreq.ifr_name);
	if (ok && system (cmd) != 0) {
		fprintf (stderr, "Failed command: %s\n", cmd);
		ok = false;
	}
	if (default_route) {
		snprintf (cmd, sizeof(cmd), "%s -6 route add default via fe80:: dev %s metric 1042", sbin_ip, ifreq.ifr_name);
	} else {
		int pflen = 64;
		if (v6listen.s6_addr16 [0] == htons (0xfc64)) {
			pflen = 16;
		//TODO// } else if (v6listen.s6_addr32 [0] == htons (TBD1)) {
		//TODO// 	pflen = 32;
		}
		snprintf (cmd, sizeof(cmd), "%s -6 route add %x:%x:%x:%x::/%d via fe80:: dev %s", sbin_ip, ntohs (v6listen.s6_addr16 [0]), ntohs (v6listen.s6_addr16 [1]), ntohs (v6listen.s6_addr16 [2]), ntohs (v6listen.s6_addr16 [3]), pflen, ifreq.ifr_name);
	}
	syslog (LOG_INFO, "Major Route: %s", cmd);
	if (ok && system (cmd) != 0) {
		fprintf (stderr, "Failed command: %s\n", cmd);
		ok = false;
	}
	for (int i = 0; i < v6route_count; i++) {
		snprintf (cmd, sizeof(cmd), "%s -6 route add %x:%x:%x:%x:%x:%x:%x:%x/%d via fe80:: dev %s metric 1052", sbin_ip, ntohs (v6route_addr [i].s6_addr16 [0]), ntohs (v6route_addr [i].s6_addr16 [1]), ntohs (v6route_addr [i].s6_addr16 [2]), ntohs (v6route_addr [i].s6_addr16 [3]), ntohs (v6route_addr [i].s6_addr16 [4]), ntohs (v6route_addr [i].s6_addr16 [5]), ntohs (v6route_addr [i].s6_addr16 [6]), ntohs (v6route_addr [i].s6_addr16 [7]), v6route_pfix [i], ifreq.ifr_name);
		if (ok && system (cmd) != 0) {
			fprintf (stderr, "Failed command: %s\n", cmd);
			ok = false;
		}
	}
	return ok;
}
#endif /* LINUX */



/*
 *
 * Extension Hook Functions
 *
 */


/* Extension hook to deliver a new IPv6 address range that others may use.
 */
void exthook_add_range_offer (struct in6_addr *start, struct in6_addr *end) {
	if (exthook == NULL) {
		return;
	}
	char startstr [INET6_ADDRSTRLEN+1];
	char endstr   [INET6_ADDRSTRLEN+1];
	inet_ntop (AF_INET6, start, startstr, INET6_ADDRSTRLEN);
	inet_ntop (AF_INET6, end,   endstr,   INET6_ADDRSTRLEN);
	char cmd [512];
	snprintf (cmd, sizeof(cmd), "%s %d add-range-offer %s %s",
				exthook, getpid (),
				startstr, endstr);
	if (system (cmd) != 0) {
		fprintf (stderr, "Hook failed: %s\n", cmd);
	}
}


/* Extension hook to deliver a new IPv6 route.
 */
void exthook_add_route_offer (struct in6_addr *pfaddr, uint8_t pflen,
				struct in6_addr *router) {
	if (exthook == NULL) {
		return;
	}
	char pfaddrstr [INET6_ADDRSTRLEN+1];
	char routerstr [INET6_ADDRSTRLEN+1];
	inet_ntop (AF_INET6, pfaddr, pfaddrstr, INET6_ADDRSTRLEN);
	inet_ntop (AF_INET6, router, routerstr, INET6_ADDRSTRLEN);
	char cmd [512];
	snprintf (cmd, sizeof(cmd), "%s %d add-route-offer %s %d %s",
				exthook, getpid (),
				pfaddrstr, pflen, routerstr);
	if (system (cmd) != 0) {
		fprintf (stderr, "Hook failed: %s\n", cmd);
	}
}


/* Extension hook to remove past offers by this process.
 * This one is special; it is run during orderly shutdown.
 */
void exthook_del_offers (void) {
	if (exthook == NULL) {
		return;
	}
	char cmd [512];
	snprintf (cmd, sizeof(cmd), "%s %d del-offers",
				exthook, getpid ());
	if (system (cmd) != 0) {
		fprintf (stderr, "Hook failed: %s\n", cmd);
	}
}


/*
 *
 * Utility functions
 *
 */



/* Produce an IPv6 address following the 6bed4 structures.
 *  - The top half is taken from v6listen
 *  - The bottom contains IPv4 address and port from v4name
 *  - The last 14 bits are filled with the lanip parameter
 */
void addr_6bed4 (struct in6_addr *dst_ip6, uint16_t lanip) {
	memcpy (&dst_ip6->s6_addr [0], &v6listen, 8);
	dst_ip6->s6_addr32 [2] = v4name.sin_addr.s_addr;
	dst_ip6->s6_addr16 [6] = v4name.sin_port;
	dst_ip6->s6_addr  [14] = ((dst_ip6->s6_addr [8] & 0x03) << 6)
	                       | ((lanip >> 8) & 0x3f);
	dst_ip6->s6_addr  [15] = (lanip & 0xff);
	dst_ip6->s6_addr  [8] &= 0xfc;
}


/*
 * Test if the provided IPv6 address matches the /64 prefix that we
 * offer routes for (as set with the -L option or derived for it).
 */
static inline bool is_mine (struct in6_addr *ip6) {
	return memcmp (&v6listen, ip6->s6_addr, 8) == 0;
}


/*
 * Test if the provided IPv6 address matches the prefix used for 6bed4.
 */
static inline bool is_6bed4 (struct in6_addr *ip6) {
	if (ip6->s6_addr16 [0] == htons (0xfc64)) {
		return true;
	}
#ifdef TBD1
	if (ip6->s6_addr32 [0] == htonl (TBD1)) {
		return true;
	}
#endif
	return false;
}


/*
 * Test if the provided IPv6 address matches the prefix used for 6bed4
 * and is ours because the IPv4 matches too.  A <netid> is untested.
 */
static inline bool is_6bed4_mine (struct in6_addr *ip6) {
	if (ip6->s6_addr32 [1] != v6listen.s6_addr32 [1]) {
		return false;
	}
	return is_6bed4 (ip6);
}


/*
 * Test if the provided IPv6 address matches the prefix used for 6bed4
 * but is not ours because the IPv4 does not match.
 */
static inline bool is_6bed4_other (struct in6_addr *ip6) {
	if (ip6->s6_addr32 [1] != v6listen.s6_addr32 [1]) {
		return true;
	}
	return is_6bed4 (ip6);
}


/* Find the current time as a 32-bit cycling integer with
 * roughly millisecond resolution.
 *
 * We subtract these and learn from the wrapped difference
 * how it compares to 0 to decide what to do.
 *
 * It is a good * principle to ask for this value once per
 * event cycle, to save on system calls as well as have a
 * stable time.  This routine internally stores and reports
 * the last time until the "reload" flag is set.
 *
 * The function can look ahead in time with a positive
 * "offset" value, or look back when it is negative.
 */
int32_t int32time (bool reload, int32_t offset) {
	static int32_t now;
	if (reload) {
		struct timeval sample;
		gettimeofday (&sample, NULL);
		// Embedded form: Would also work with <<10 and >>10
		now = (sample.tv_sec * 1000) + (sample.tv_usec / 1000);
	}
	return now + offset;
}


/* Add a peer NAT state to the right queue, based on its state.
 * This does not change the hash table entrance.
 */
void pns_enqueue (struct pns_t *new) {
	struct pns_t **hd;
	int32_t ofs;
	if (new->pns_state & PNS_LOOP_25S) {
		hd = &pns_q25s;
		ofs = 25000;
	} else {
		hd = &pns_q01s;
		ofs =  1000;
	}
	new->pns_state_until = int32time (false, ofs);
	syslog (LOG_DEBUG, "Enqueued in state 0x%02x for %dms", new->pns_state, ofs);
	DL_APPEND2 (*hd, new, pns_prev, pns_next);
}


/* Remove a peer NAT state from its current timer queue.
 * This does not change the hash table entrance.
 */
void pns_dequeue (struct pns_t *old) {
	struct pns_t **hd;
	if (old->pns_state & PNS_LOOP_25S) {
		hd = &pns_q25s;
	} else {
		hd = &pns_q01s;
	}
	syslog (LOG_DEBUG, "Dequeued in state 0x%02x", old->pns_state);
	DL_DELETE2 (*hd, old, pns_prev, pns_next);
}


/* Create a new peer NAT state for the given ip4udp.
 * Initialise everything and start in INIT0 state, added
 * to the right timer queue and enlisted in the hash.
 */
struct pns_t *pns_addpeer (uint8_t ip4udp [6]) {
	struct pns_t *new = malloc (sizeof (struct pns_t));
	if (new == NULL) {
		return NULL;
	}
	memset (new, 0, sizeof (struct pns_t));
	memcpy (new->pns_ip4udp, ip4udp, 6);
	int32_t expired = int32time (false, -60000);
	new->pns_seen_until  = expired;
	new->pns_last_send   = expired;
	new->pns_last_direct = expired;
	new->pns_icmp_wait   = expired;
	new->pns_state = PNS_INIT0;
	HASH_ADD (hh, pns_hash, pns_ip4udp, 6, new);
	pns_enqueue (new);
	return new;
}


/* Find a peer NAT state for the given ip4udp.  Return
 * NULL when it is not yet known.
 */
struct pns_t *pns_findpeer (uint8_t ip4udp [6]) {
	struct pns_t *retval = NULL;
	HASH_FIND (hh, pns_hash, ip4udp, 6, retval);
	return retval;
}


/* Delete the peer NAT state for the given ip4udp.
 * This does not involve removal from the timer queue,
 * but it involves removal from the hash.
 */
void pns_delpeer_bypns (struct pns_t *old) {
	if (old == NULL) {
		return;
	}
	//NOT_TWICE// pns_dequeue (old);
	HASH_DEL (pns_hash, old);
	free (old);
}
//
void pns_delpeer (uint8_t ip4udp [6]) {
	struct pns_t *old = pns_findpeer (ip4udp);
	pns_delpeer_bypns (old);
}


/* Find all the expired elements in a given queue and
 * either recycle them with minor action, or take them
 * out for a major action.  Return the new timeout as
 * it appears to the queues.
 */
int32_t pns_expire (int32_t now) {
	void probe (uint8_t ip4udp [6]);
	struct pns_t *this, *tmp;
	struct pns_t **hd = &pns_q01s;
	int32_t nextstop = now + 60000;
	while (hd != NULL) {
		DL_FOREACH_SAFE2 (*hd, this, tmp, pns_next) {
			// Is this the first list entry to expire in the future?
			if ((this->pns_state_until - now) > 0) {
				if ((this->pns_state_until - nextstop) < 0) {
					nextstop = this->pns_state_until;
				}
				break;
			}
			// Test whether we are making a minor or major transition
			bool major = PNS_COUNTER_DONE (this->pns_state);
			// pns_dequeue() -- in a loop-safe variant
			DL_DELETE2 (*hd, this, pns_prev, pns_next);
			// The peer NAT state expired and was taken off its
			// timer queue; we now decide if/how to proceed.
			bool cont = true;
			switch (this->pns_state) {
				// Normally, we keep the peer NAT state and add
				// it to a timer.  But before doing that, we
				// can stop that by resetting "cont", and we can
				// do something other than simply incrementing
				// the state, to cause other state transitions.
			case PNS_INIT1:
			case PNS_POLL1:
				// Continue as FAIL (but do send Probe below)
				this->pns_state = PNS_FAIL0;
				break;
			case PNS_PEER:
				// Continue into POLL (and send Probe below)
				this->pns_state = PNS_POLL0;
				break;
			case PNS_FAIL1:
				// Continue in FAIL1 if we didn't send for ~45s but
				// drop the peer NAT state after longer inactivity.
				// (Inaccurate timing, it's just "pretty darn long".)
				// As long as we're sending, we'll stay in FAIL1
				// and send occasional Probes, just to allow us to
				// bootstrap direct peering somewhat quickly.
				cont = (this->pns_last_send + 45000 - now) > 0;
				break;
			default:
				// Minor changes, INIT0 -> INIT1 or POLL0 -> POLL1
				this->pns_state++;
			}
			// Requeue with a timer, or erase the peer from memory
			if (cont) {
				pns_enqueue (this);
				probe (this->pns_ip4udp);
				syslog (LOG_INFO, "Sent Probe to Peer %d.%d.%d.%d:%d and state := 0x%02x\n", this->pns_ip4udp [0], this->pns_ip4udp [1], this->pns_ip4udp [2], this->pns_ip4udp [3], htons (* (uint16_t *) (this->pns_ip4udp + 4)), this->pns_state);
			//TODO// else if (this->keepalive) { keepalive (); }
			} else {
				pns_delpeer_bypns (this);
			}
		}
		// Iterate over the two queues: pns_q01s, pns_q25s
		if (hd != &pns_q25s) {
			hd = &pns_q25s;
		} else {
			hd = NULL;
		}
	}
	return nextstop;
}


/* Process an incoming Traffic Class into a peer NAT state.
 */
static inline void pns_4to6_tc (struct pns_t *pns) {
	uint8_t traffic_class = v4v6trfcls;
	syslog (LOG_INFO, "Receiving with Traffic Class 0x%02x", traffic_class);
	// Check if the Traffic Class matches our style and flags "Seen"
	if ((traffic_class & 0x3c) != 0x20) {
		return;
	}
	// Only proceed for existing peer NAT state
	if (pns == NULL) {
		uint8_t ip4udp [6];
		ip4udp [0] = (v4src6->s6_addr [8] & 0xfc) | (v4src6->s6_addr [14] >> 6);
		memcpy (ip4udp + 1, v4src6->s6_addr + 9, 5);
		pns = pns_findpeer (ip4udp);
		if (pns == NULL) {
			syslog (LOG_INFO, "Received \"Seen\" flag from %d.%d.%d.%d:%d, but peer is unknown\n", ip4udp [0], ip4udp [1], ip4udp [2], ip4udp [3], ntohs (* (uint16_t *) &ip4udp [4]));
			return;
		}
	}
	// A valid byte with a "Seen" flag, reset to 25s of direct peering
	syslog (LOG_INFO, "Received \"Seen\" flag from %d.%d.%d.%d:%d, resetting to 25s of PEER\n", pns->pns_ip4udp [0], pns->pns_ip4udp [1], pns->pns_ip4udp [2], pns->pns_ip4udp [3], ntohs (* (uint16_t *) &pns->pns_ip4udp [4]));
	pns_dequeue (pns);
	pns->pns_state = PNS_PEER;
	pns->pns_state_until = int32time (false, 25000);
	pns_enqueue (pns);
}


/* Process incoming direct traffic into peer NAT state.
 */
static inline void pns_4to6_direct (struct pns_t *pns) {
	// Raise the reverse Seen flag after direct traffic
	int32_t seenperiod = 1000 * (keepalive_period - 28);
	pns->pns_seen_until = pns->pns_last_direct + seenperiod;
}


/* Process an outgoing Traffic Class, and set its Seen flag.
 * Also take note of the fact that we did send a message.
 */
static inline void pns_6to4_tc_seen (struct pns_t *pns) {
	// Reset the last outgoing traffic timer
	int32_t now = int32time (false, 0);
	pns->pns_last_send = now;
	// Check if the Traffic Class matches our style
	if (((v6data [1] & 0xc0) != 0x00) || ((v6data [0] & 0x01) != 0x00)) {
		return;
	}
	// Set or reset the Seen flag
	if ((pns->pns_seen_until - now) > 0) {
		v6data [0] |= 0x02;
	} else {
		v6data [0] &= 0xfd;
	}
}


/* Choose whether to send as direct peering or not.  Mostly, the
 * output is either to send through the 6bed4router or directly;
 * only for one case can it result in dropping or ICMPv6 errors.
 *
 * The return value is one of:
 *   SEND_ROUTER  to send via the 6bed4router (see below);
 *   SEND_PEER    to send directly to the peer;
 *   SEND_OBLIVIA to drop the message.
 *   SEND_ERROR   to return an ICMPv6 error;
 *
 * The intention of SEND_ROUTER is to address not our own but the
 * peer's 6bed4router, which the peer keeps open through regular
 * keepalive messages.  They may be the same, of course.  But it
 * is only possible to determine the peer's 6bed4router when it
 * has a prefix TBD1::/32 or fc64::/16 which is known to contain
 * the 6bed4router IPv4 address in the last 32 bits of its /64.
 * In all other cases, 6bed4 routing is done through the IPv6
 * native route (which may or may not include internal addresses
 * that may or may not be networked to a larger network than the
 * 6bed4router servicing this 6bed4peer).
 *
 * The "foreign" flag is set to true on non-6bed4 Traffic Classes.
 */
#define SEND_ROUTER  ((uint8_t) 0)
#define SEND_PEER    ((uint8_t) 1)
#define SEND_OBLIVIA ((uint8_t) 2)
#define SEND_ERROR   ((uint8_t) 3)
//
uint8_t pns_6to4_where (struct pns_t *pns, uint8_t traffic_class, bool *foreign) {
	// Ignore the "Seen" flag; For foreign Traffic Classes, use PP_PROPER
	uint8_t peerpol;
	*foreign = (traffic_class & 0x1c) != 0x00;
	if (*foreign) {
		peerpol = PP_PROPER;
printf ("Switching on proper peer policy due to unknown traffic class 0x%02x\n", traffic_class);
	} else {
		peerpol = traffic_class & 0xc0;
printf ("Switching on proper peer policy 0x%02x from traffic class 0x%02x\n", peerpol, traffic_class);
	}
	// Pickup peer NAT state, or default to INIT0
	uint8_t peerstate;
	if (pns == NULL) {
		peerstate = PNS_INIT0;
	} else {
		peerstate = pns->pns_state;
	}
	// Switch to behaviour that depends on the peering policy
	switch (peerpol) {
	case PP_PROPER:
		// Route FAIL and INIT traffic through the 6bed4router
		return (peerstate & PNS_DIRECT) ? SEND_PEER : SEND_ROUTER;
	case PP_PROHIBITED:
		// Route all traffic through the 6bed4router
		return SEND_ROUTER;
	case PP_PRESUMPTIOUS:
		// Route only FAIL traffic through the 6bed4router
		return (peerstate & PNS_FAILED) ? SEND_ROUTER : SEND_PEER;
	case PP_PERSISTENT:
		// No traffic through the 6bed4router, FAIL is ICMP
		if (peerstate & PNS_FAILED) {
			int32_t now = int32time (false, 0);
			if (pns->pns_icmp_wait < now) {
				pns->pns_icmp_wait = now + 1000;
				return SEND_ERROR;
			} else {
				return SEND_OBLIVIA;
			}
		}
		return SEND_PEER;
	}
}


/*
 * Calculate the ICMPv6 checksum field
 */
uint16_t icmp6_checksum (uint8_t *ipv6hdr, size_t payloadlen) {
	uint16_t plenword = htons (payloadlen);
	uint16_t nxthword = htons (IPPROTO_ICMPV6);
	uint16_t *areaptr [] = { (uint16_t *) &ipv6hdr [8], (uint16_t *) &ipv6hdr [24], &plenword, &nxthword, (uint16_t *) &ipv6hdr [40], (uint16_t *) &ipv6hdr [40 + 4] };
	uint8_t areawords [] = { 8, 8, 1, 1, 1, payloadlen/2 - 2 };
	uint32_t csum = 0;
	u_int8_t i, j;
	for (i=0; i < 6; i++) {
		uint16_t *area = areaptr [i];
		for (j=0; j<areawords [i]; j++) {
			csum += ntohs (area [j]);
		}
	}
	csum = (csum & 0xffff) + (csum >> 16);
	csum = (csum & 0xffff) + (csum >> 16);
	csum = htons (~csum);
	return csum;
}


/* Append the current prefix to an ICMPv6 message.  Incoming optidx
 * and return values signify original and new offset for ICMPv6 options.
 * The endlife parameter must be set to obtain zero lifetimes, thus
 * instructing the tunnel client to stop using an invalid prefix.
 */
size_t icmp6_prefix (size_t optidx, uint8_t endlife) {
	v6icmp6data [optidx++] = 3;	// Type
	v6icmp6data [optidx++] = 4;	// Length
	v6icmp6data [optidx++] = 114;	// This is a /114 prefix
	v6icmp6data [optidx++] = 0xc0;	// L=1, A=1, Reserved1=0
	memset (v6icmp6data + optidx, endlife? 0x00: 0xff, 8);
	optidx += 8;
					// Valid Lifetime: Zero / Infinite
					// Preferred Lifetime: Zero / Infinite
	memset (v6icmp6data + optidx, 0, 4);
	optidx += 4;
					// Reserved2=0
	addr_6bed4 ((struct in6_addr *) (v6icmp6data + optidx), 0);
					// Set IPv6 prefix
	optidx += 16;
	return optidx;
}


/* Send an ICMPv6 reply.  This is constructed at the tunnel end, from
 * the incoming message.  The parameter indicates how many bytes the
 * ICMPv6 package counts after the ICMPv6 header.  It must be 4 (mod 8).
 *
 * Actions: v4/udp src becomes dest, set v4/udp/v6 src, len, cksum, send.
 *          reply is always to v4src6, except that if it starts with
 *	    0x00,0x00 it will be replaced with allnodes_linklocal_address.
 */
void icmp6_reply (size_t icmp6bodylen) {
	//TODO// Rewrite or Reset the Traffic Class
	v4v6hoplimit = 255;
	if ((icmp6bodylen & 0x07) != 4) {
		return;   /* illegal length, drop */
	}
	v4v6plen = htons (icmp6bodylen + 4);
	memcpy (v4dst6,
		(v4src6->s6_addr16 [0])
			? (uint8_t *) v4src6
			: allnodes_linklocal_address,
		16);
	memcpy (v4src6, router_linklocal_address_complete, 16);
	v4v6icmpcksum = icmp6_checksum ((uint8_t *) v4hdr6, ntohs (v4v6plen));
	//
	// Send the message to the IPv4 originator port
printf ("Sending ICMPv6-IPv6-UDP-IPv4 to %d.%d.%d.%d:%d, result = %zd\n",
((uint8_t *) &v4name.sin_addr.s_addr) [0],
((uint8_t *) &v4name.sin_addr.s_addr) [1],
((uint8_t *) &v4name.sin_addr.s_addr) [2],
((uint8_t *) &v4name.sin_addr.s_addr) [3],
ntohs (v4name.sin_port),
	sendto (v4sox,
			v4data,
			sizeof (struct ip6_hdr) + 4 + icmp6bodylen,
			MSG_DONTWAIT,
			(struct sockaddr *) &v4name, sizeof (v4name)));
}


/*
 * Respond to a Router Solicitation received over the 6bed4 Network.
 */
void advertise_route (void) {
	v4v6icmptype = ND_ROUTER_ADVERT;
	v4v6icmpdata [0] = 0;			// Cur Hop Limit: unspec
	v4v6icmpdata [1] = 0x18;		// M=0, O=0
						// H=0, Prf=11=Low
						// Reserved=0
// TODO: wire says 0x44 for router_adv.flags
	size_t writepos = 2;
	memset (v4v6icmpdata+writepos, 0xff, 2+4+4);
					// Router Lifetime: max, 18.2h
					// Reachable Time: max
					// Retrans Timer: max
	writepos += 2+4+4;
	writepos = icmp6_prefix (writepos, 0);
	icmp6_reply (writepos);
}


/*
 * Validate the originator's IPv6 address.  It should match the
 * UDP/IPv4 coordinates of the receiving 6bed4 socket.  Also,
 * the /64 prefix (but not the /114 prefix!) must match v6listen.
 */
bool validate_originator (struct in6_addr *src6) {
	uint32_t addr;
	//
	// Communication from the configured router is welcomed
	// even when it comes from native/other addresses
	if ((v4name.sin_addr.s_addr == v4peer.sin_addr.s_addr)
			&& (v4name.sin_port == v4peer.sin_port)) {
		return true;
	}
	//
	// Require non-local top halves to match our v6listen_linklocal address
	// This matches fc64::/16, TBD1::/32 with our 6bed4router's IPv4 in it;
	// it also matches native /64 prefixes.  Finally, link local prefixes
	// may be used for ICMPv6 messages.
	if (memcmp (src6, v6listen_linklocal, 8) != 0) {
		if (memcmp (&v6listen, src6->s6_addr, 8) != 0) {
			return false;
		}
	}
	//
	// Require the sender port to appear in its IPv6 address
	bool oklow = v4name.sin_port == src6->s6_addr16 [6];
	//
	// Require the sender address to appear in its IPv6 address
	if (oklow) {
		addr = ntohl (src6->s6_addr32 [2]) & 0xfcffffff;
		addr |= ((uint32_t) (src6->s6_addr [14] & 0xc0)) << (24-6);
		oklow = addr == ntohl (v4name.sin_addr.s_addr);
	}
	//
	// If we received a proper /64 with incorrect completion to /114
	// we spontaneously respond with a Router Advertisement
	if (!oklow) {
		advertise_route ();
	}
	//
	// Return the overall result
	return oklow;
}


/* Validing bypass traffic that was initiated over trapezium routing.
 * This means that the top half holds the IPv4 address of a 6bed4router,
 * both in source and destination IPv6 address, and our router is in
 * the target whereas the source's router is in the source IPv6 address.
 * In addition, the lower half logic applies.
 */
bool validate_trapezium_bypass (struct in6_addr *src6, struct in6_addr *dst6) {
	//
	// Require the source and destination to have a 6bed4 top half,
	// though they may use different prefixes and/or <netid> values.
	if ((!is_6bed4 (src6)) || (!is_6bed4 (dst6))) {
		return false;
	}
	//
	// Ignore the top half IPv4 address in the source IPv6 address
	//
	// The IPv4 in the destination top half IPv6 must be our router
	if (dst6->s6_addr32 [1] != v4listen.s_addr) {
		return false;
	}
	//
	// Match the source IPv4 address and port in the lower half
	uint8_t lladdr [6];
	lladdr [0] = (src6->s6_addr [8] & 0xfc) | (src6->s6_addr [14] >> 6);
	memcpy (&lladdr [1], &src6->s6_addr [9], 5);
	if (* (uint32_t *) &lladdr [0] != v4name.sin_addr.s_addr) {
		return false;
	}
	if (* (uint16_t *) &lladdr [4] != v4name.sin_port) {
		return false;
	}
	//
	// Match the desintation IPv4 address and port with ours
	if (memcmp (&dst6->s6_addr [8], &v6listen.s6_addr [8], 6) != 0) {
		return false;
	}
	if (((dst6->s6_addr [14] ^ v6listen.s6_addr [14]) & 0xc0) != 0) {
		return false;
	}
	//
	// All tests succeeded
	return true;
}



/*
 * Major packet processing functions
 */


//TODO// void probe_4to4 (...)


//TODO// void handle_4to4_plain (...)


/* Handle the IPv4 message pointed at by msg, checking if (TODO:HUH?) the IPv4:port
 * data matches the lower half of the IPv6 sender address.  Drop silently
 * if this is not the case.  TODO: or send ICMP?
 */
void handle_4to6_plain (ssize_t v4datalen, struct sockaddr_in *sin) {
	//
	// Send the unwrapped IPv6 message out over v6sox
	syslog (LOG_INFO, "Writing IPv6, result = %zd\n",
	write (v6sox, &v4data6, HDR_SIZE + v4datalen)
)
	;
}


/* Handle the IPv4 message pointed at by msg as a neighbouring command.
 *
 * Type	Code	ICMPv6 meaning			Handling
 * ----	----	-----------------------------	----------------------------
 * 133	0	Router Solicitation		Ignore
 * 134	0	Router Advertisement		Setup Tunnel with Prefix
 * 135	0	Neighbour Solicitation		Send Neighbour Advertisement
 * 136	0	Neighbour Advertisement		Ignore
 * 137	0	Redirect			Ignore
 */
void handle_4to6_nd (struct sockaddr_in *sin, ssize_t v4ngbcmdlen) {
	uint16_t srclinklayer;
	uint8_t *destprefix = NULL;
	struct ndqueue *ndq;
	if (v4ngbcmdlen < sizeof (struct ip6_hdr) + sizeof (struct icmp6_hdr)) {
		return;
	}
	//
	if (v4v6icmpcode != 0) {
		return;
	}
	if (icmp6_checksum (v4data, v4ngbcmdlen - sizeof (struct ip6_hdr)) != v4v6icmpcksum) {
		return;
	}
	//
	// Approved.  Perform neighbourly courtesy.
	switch (v4v6icmptype) {
	case ND_ROUTER_SOLICIT:
		return;		/* this is not a router, drop */
	case ND_ROUTER_ADVERT:
		//
		// Validate Router Advertisement
		if (ntohs (v4v6plen) < sizeof (struct icmp6_hdr) + 16) {
			return;   /* strange length, drop */
		}
		if ((v4v6icmpdata [1] & 0x80) != 0x00) {
			return;   /* indecent proposal to use DHCPv6, drop */
		}
		if (memcmp (&v4src6->s6_addr, router_linklocal_address_complete, 16) != 0) {
			return;   /* not from router, drop */
		}
		if (memcmp (&v4dst6->s6_addr, client1_linklocal_address, 8) != 0) {
			if (memcmp (&v4dst6->s6_addr, allnodes_linklocal_address, 16) != 0) {
				return;   /* no address setup for me, drop */
			}
		}
		//
		// Parse the Router Advertisement
		size_t rdofs = 12;
		int v6route_count_old = v6route_count;
		//TODO:+4_WRONG?// while (rdofs <= ntohs (v4v6plen) + 4) { ... }
		while (rdofs + 4 < ntohs (v4v6plen)) {
			if (v4v6icmpdata [rdofs + 1] == 0) {
				return;   /* zero length option */
			}
			if (v4v6icmpdata [rdofs + 0] != ND_OPT_PREFIX_INFORMATION) {
				/* skip to next option */
			} else if (v4v6icmpdata [rdofs + 1] != 4) {
				return;   /* bad length field */
			} else if (rdofs + (v4v6icmpdata [rdofs + 1] << 3) > ntohs (v4v6plen) + 4) {
				return;   /* out of packet length */
			} else if ((v4v6icmpdata [rdofs + 3] & 0xc0) != 0xc0) {
				/* no on-link autoconfig, but routable prefix */
				printf ("Received a routable prefix %x%02x:%x%02x:%x%02x:%x%02x:.../%d\n", v4v6icmpdata [rdofs+16], v4v6icmpdata [rdofs+17], v4v6icmpdata [rdofs+18], v4v6icmpdata [rdofs+19], v4v6icmpdata [rdofs+20], v4v6icmpdata [rdofs+21], v4v6icmpdata [rdofs+22], v4v6icmpdata [rdofs+22], v4v6icmpdata [rdofs+2]);
				if (v6route_count_old > 0) {
					v6route_count = v6route_count_old = 0;
				}
				if ((v6route_count < MAX_ROUTABLE_PREFIXES) &&
						(v4v6icmpdata [rdofs + 2] <= 128) &&
						(v4v6icmpdata [rdofs + 2] >= 16)) {
					memcpy (
						v6route_addr [v6route_count].s6_addr,
						&v4v6icmpdata [rdofs+16],
						16);
					v6route_pfix [v6route_count] =
						v4v6icmpdata [rdofs + 2];
					v6route_count++;
				}
			} else if (v4v6icmpdata [rdofs + 2] != PREFIX_SIZE) {
				/* not a /114 prefix, so no 6bed4 offer */
				return;
			} else {
				destprefix = &v4v6icmpdata [rdofs + 16];
			}
			rdofs += (v4v6icmpdata [rdofs + 1] << 3);
		}
		if (destprefix) {
			if (v6route_count_old > 0) {
				v6route_count = 0;
			}
			memcpy (v6listen.s6_addr + 0, destprefix, 16);
			v6listen.s6_addr [14] &= 0xc0;
			v6listen.s6_addr [15]  = 0x01;	// choose client 1
			memcpy (v6listen_linklocal_complete + 0, v6listen_linklocal, 8);
			memcpy (v6listen_linklocal_complete + 8, v6listen.s6_addr + 8, 8);
			memcpy (v6lladdr, destprefix + 8, 6);
			//TODO// Is v6lladdr useful?  Should it include lanip?
			v6lladdr [0] &= 0xfc;
			v6lladdr [0] |= (destprefix [14] >> 6);
			inet_ntop (AF_INET6,
				&v6listen,
				v6prefix,
				sizeof (v6prefix));
			syslog (LOG_INFO, "%s: Assigning address %s to tunnel\n", program, v6prefix);
			setup_tunnel_address ();  //TODO// parameters?
			got_lladdr = true;
			maintenance_time_cycle = maintenance_time_cycle_max;
			struct in6_addr start, end;
			memcpy (&start, destprefix, 16);
			memcpy (&end,   destprefix, 16);
			start.s6_addr [15] |= 0x02;
			end  .s6_addr [14] |= 0x3f;
			end  .s6_addr [15] |= 0xff;
			/* Update the extension hooks */
			exthook_del_offers ();
			exthook_add_range_offer (&start, &end);
			for (int i = 0; i < v6route_count; i++) {
				exthook_add_route_offer (
						&v6route_addr [i],
						v6route_pfix [i],
						(struct in6_addr *) destprefix);
			}
		}
		return;
	case ND_NEIGHBOR_SOLICIT:
		//
		// Drop Neigbour Solicitation
		return;
	case ND_NEIGHBOR_ADVERT:
		//
		// Drop Neigbor Advertisement
		return;
	case ND_REDIRECT:
		//
		// Redirect indicates that a more efficient bypass exists than
		// the currently used route.  The remote peer has established
		// this and wants to share that information to retain a
		// symmetric communication, which is helpful in keeping holes
		// in NAT and firewalls open.
		//
		// BE EXTREMELY SELECTIVE BEFORE ACCEPTING REDIRECT!!!
		return;
	}
}


/* Receive a tunnel package, and route it to either the handler for the
 * tunnel protocol, or to the handler that checks and then unpacks the
 * contained IPv6.
 */
void handle_4to6 (int v4in) {
	uint8_t buf [1501];
	ssize_t buflen;
	socklen_t adrlen = sizeof (v4name);
	//
	// Receive IPv4 package, which may be tunneled or a tunnel request
	buflen = recvfrom (v4in,
			v4data, MTU,
			MSG_DONTWAIT,
			(struct sockaddr *) &v4name, &adrlen
		);
	if (buflen == -1) {
		syslog (LOG_INFO, "%s: WARNING: Error receiving IPv4-side package: %s\n",
				program, strerror (errno));
		return;		/* receiving error, drop */
	}
	/* This may be a Probe or full packet that made it in over direct peering */
	uint8_t ip4udp [6];
	memcpy (ip4udp + 0, &v4name.sin_addr, 4);
	memcpy (ip4udp + 4, &v4name.sin_port, 2);
	struct pns_t *pns = NULL;
	if (memcmp (ip4udp, ip4udp_6bed4router, 6) != 0) {
		/* Indirect traffic comes in from _our_ 6bed4router */
		pns = pns_findpeer (ip4udp);
		if (pns != NULL) {
			pns_4to6_direct (pns);
		}
	}
	/* Probe having been noticed, fend off poorly formed packets */
	if (buflen <= sizeof (struct ip6_hdr)) {
		return;		/* received too little data, drop */
	}
	if ((v4data [0] & 0xf0) != 0x60) {
		return;		/* not an IPv6 packet, drop */
	}
	if (!validate_originator (v4src6)) {
		/* source appears fake, but it may be a trapezium bypass */
		if (!validate_trapezium_bypass (v4src6, v4dst6)) {
			return;		/* invalid both ways, so drop */
		}
	}
	/* Process any incoming "Seen" flag from already-known peers */
	pns_4to6_tc (pns);
	/*
	 * Distinguish types of traffic:
	 * Non-plain, Plain Unicast, Plain Multicast
	 */
	if ((v4v6nexthdr == IPPROTO_ICMPV6) &&
			(v4v6icmptype >= 133) && (v4v6icmptype <= 137)) {
		//
		// Not Plain: Router Adv/Sol, Neighbor Adv/Sol, Redirect
		if (v4v6hoplimit != 255) {
			return;
		}
		handle_4to6_nd (&v4name, buflen);
	} else {
		//
		// Plain Unicast or Plain Multicast (both may enter)
		if (v4v6hoplimit-- <= 1) {
			return;
		}
		handle_4to6_plain (buflen, &v4name);
	}
}


/*
 * Relay an IPv6 package to 6bed4, using the link-local address as it
 * is found in the Ethernet header.  Trust the local IPv6 stack to have
 * properly obtained this destination address through Neighbor Discovery
 * over 6bed4.
 */
//TO_LIB// Entire function as _send6bed4_plain_unicast() with lladdr taken from src6
void handle_6to4_plain_unicast (const ssize_t pktlen, const uint8_t *lladdr) {
	void probe (uint8_t lladdr [6]);
	struct sockaddr_in v4dest;
	uint8_t ip4udp [6];
	struct pns_t *pns;
	bool dst6bed4 = is_6bed4 (v6dst6);
	bool newpeer = false;
	if (dst6bed4) {
		ip4udp [0] = v6dst6->s6_addr [8] & 0xfc | (v6dst6->s6_addr [14] >> 6);
		memcpy (&ip4udp [1], &v6dst6->s6_addr [9], 5);
		pns = pns_findpeer (ip4udp);
		newpeer = (pns == NULL);
		if (newpeer) {
			syslog (LOG_INFO, "Adding a peer for %d.%d.%d.%d:%d\n", ip4udp [0], ip4udp [1], ip4udp [2], ip4udp [3], ntohs (* (uint16_t *) &ip4udp [4]));
			pns = pns_addpeer (ip4udp);
		}
	} else {
		pns = NULL;
	}
	bool foreign;
	uint8_t trfcls = v6trfcls;
	memset (&v4dest, 0, sizeof (v4dest));	/* TODO: NOT EVERY TIME? */
	v4dest.sin_family = AF_INET;		/* TODO: NOT EVERY TIME? */
	uint8_t whereto = pns_6to4_where (pns, trfcls, &foreign);
	pns_6to4_tc_seen (pns);
	switch (whereto) {
	case SEND_PEER:
		if (dst6bed4) {
			memcpy (&v4dest.sin_addr, ip4udp + 0, 4);
			memcpy (&v4dest.sin_port, ip4udp + 4, 2);
			/* Just updated pns_last_send, copy to pns_last_direct */
			pns->pns_last_direct = pns->pns_last_send;
		} else {
			; /* No 6bed4 -- no ip4udp -- cannot route */
		}
		break;
	case SEND_ROUTER:
		/* We always route to _our_ 6bed4router.  We do not try
		   to avoid trapezium routing other than hoping that
		   direct peering will arrive soon.  No half measures.
		   Clients may opt for opening more than one tunnel.
		 */
		memcpy (&v4dest.sin_addr, lladdr + 2, 4);
		memcpy (&v4dest.sin_port, lladdr + 0, 2);
		/* Every 6bed4 address has a lower half; send a Probe */
		/* TODO: But is the recipient a 6bed4 address, or native?!? */
		if (newpeer) {
			probe (ip4udp);
			syslog (LOG_INFO, "Sent Probe to New Peer %d.%d.%d.%d:%d\n", ip4udp [0], ip4udp [1], ip4udp [2], ip4udp [3], htons (* (uint16_t *) (ip4udp + 4)));
		}
		break;
	case SEND_OBLIVIA:
		return;
	case SEND_ERROR:
		//TODO// Send back ICMPv6 error Host Unreachable
		return;
		break;
	}
	trfcls = v6trfcls;
	if (v6tc != trfcls) {
		v6tc = trfcls;
		v4qos = trfcls;
		if (setsockopt (v4sox, IPPROTO_IP, IP_TOS, &v4qos, sizeof (v4qos)) == -1) {
			syslog (LOG_ERR, "Failed to switch IPv4 socket to QoS setting 0x%02x\n", v4qos);
		}
	}
	syslog (LOG_DEBUG, "%s: Sending IPv6-UDP-IPv4 to %d.%d.%d.%d:%d, result = %zd\n", program,
	((uint8_t *) &v4dest.sin_addr.s_addr) [0],
	((uint8_t *) &v4dest.sin_addr.s_addr) [1],
	((uint8_t *) &v4dest.sin_addr.s_addr) [2],
	((uint8_t *) &v4dest.sin_addr.s_addr) [3],
	ntohs (v4dest.sin_port),
		sendto (v4sox,
				v6data,
				pktlen - HDR_SIZE,
				MSG_DONTWAIT,
				(struct sockaddr *) &v4dest,
				sizeof (struct sockaddr_in))
	)
				;
}


/*
 * Handle a request for Neighbor Discovery over the 6bed4 Link.
 */
void handle_6to4_nd (ssize_t pktlen) {
	uint8_t lldest [6];
	//
	// Validate ICMPv6 message -- trivial, trust local generation
	//
	// Handle the message dependent on its type
	switch (v6icmp6type) {
	case ND_ROUTER_SOLICIT:
		v6icmp6type = ND_ROUTER_ADVERT;
		v6icmp6code = 0;
		v6icmp6data [0] = 0;		// Cur Hop Limit: unspec
		v6icmp6data [1] = 0x18;		// M=0, O=0,
						// H=0, Prf=11=Low
						// Reserved=0
		//TODO: wire says 0x44 for router_adv.flags
		size_t writepos = 2;
		memset (v6icmp6data + writepos,
				default_route? 0xff: 0x00,
				2);		// Router Lifetime
		writepos += 2;
		memcpy (v6icmp6data + writepos,
				"\x00\x00\x80\x00",
				4);		// Reachable Time: 32s
		writepos += 4;
		memcpy (v6icmp6data + writepos,
				"\x00\x00\x01\x00",
				4);		// Retrans Timer: .25s
		writepos += 4;
		writepos = icmp6_prefix (writepos, 0);
		v6plen = htons (4 + writepos);
		memcpy (v6dst6, v6src6, 16);
		memcpy (v6src6, v6listen_linklocal_complete, 16);
		v6icmp6csum = icmp6_checksum ((uint8_t *) v6hdr6, 4 + writepos);
		syslog (LOG_INFO, "Replying Router Advertisement to the IPv6 Link, result = %zd\n",
			write (v6sox, &v6data6, HDR_SIZE + sizeof (struct ip6_hdr) + 4 + writepos)
		)
			;
		break;
	case ND_ROUTER_ADVERT:
		return;		/* the IPv6 Link is no router, drop */
	case ND_NEIGHBOR_SOLICIT:
		//
		// Drop Neighbor Solicitation
		break;
	case ND_NEIGHBOR_ADVERT:
		//
		// Drop Neighbor Advertisements
		break;
	case ND_REDIRECT:
		//
		// Redirect indicates that a more efficient bypass exists than
		// the currently used route.  The remote peer has established
		// this and wants to share that information to retain a
		// symmetric communication, which is helpful in keeping holes
		// in NAT and firewalls open.
		//
		// BE EXTREMELY CAREFUL ABOUT ACCEPTING ANY REDIRECT MESSAGES!!
		//
		return;
	}
}


/*
 * Receive an IPv6 package, check its address and pickup IPv4 address and
 * port, then package it as a tunnel message and forward it to IPv4:port.
 * Rely on the proper formatting of the incoming IPv6 packet, as it is
 * locally generated.
 */
void handle_6to4 (void) {
	//
	// Receive the IPv6 package and ensure a consistent size
	size_t rawlen = read (v6sox, &v6data6, sizeof (v6data6));
	if (rawlen == -1) {
		return;		/* failure to read, drop */
	}
//TO_LIB// Remainder of this function
	if (rawlen < HDR_SIZE + sizeof (struct ip6_hdr) + 1) {
		return;		/* packet too small, drop */
	}
printf ("ip6_hdr first word 0x%x\n", ntohl (*(uint32_t*)&v4data6.udata.ndata.v6hdr));
	/*
	 * Distinguish types of traffic:
	 * Non-plain, Plain Unicast, Plain Multicast
	 */
	if ((v6type == IPPROTO_ICMPV6) &&
			(v6icmp6type >= 133) && (v6icmp6type <= 137)) {
		//
		// Not Plain: Router Adv/Sol, Neighbor Adv/Sol, Redirect
		syslog (LOG_DEBUG, "Forwarding non-plain unicast from IPv6 to 6bed4\n");
		handle_6to4_nd (rawlen);
	} else if ((v6dst6->s6_addr [0] != 0xff) && !(v6dst6->s6_addr [8] & 0x01)) {
		//
		// Plain Unicast
		if (v6hops-- <= 1) {
			return;
		}
		syslog (LOG_DEBUG, "Forwarding plain unicast from IPv6 to 6bed4\n");
		handle_6to4_plain_unicast (rawlen, lladdr);
	} else {
		//
		// Plain Multicast
		//TODO:IGNORE_MULTICAST//
		//TODO// syslog (LOG_DEBUG, "%s: Plain multicast from 6bed4 Link to 6bed4 Network is not supported\n", program);
	}
}


/*
 * Perform Router Solicitation.  This is the usual mechanism that is used
 * on ethernet links as well, except that the 6bed4 permits fixed interface
 * identifiers; for this client, the interface identifier will be 0x0001.
 * The router always has interface identifier 0x0000 but it will now be
 * addressed at the all-routers IPv6 address 0xff02::2 with the general
 * source IPv6 address ::
 */
void solicit_router (void) {
	v4name.sin_family = AF_INET;
	memcpy (&v4name.sin_addr.s_addr, &v4listen, 4);
	v4name.sin_port = htons (STANDARD_6BED4_UDP_PORT);
	int done = 0;
	int secs = 1;
// syslog (LOG_DEBUG, "%s: Sending RouterSolicitation-IPv6-UDP-IPv4 to %d.%d.%d.%d:%d, result = %d\n", program,
// ((uint8_t *) &v4name.sin_addr.s_addr) [0],
// ((uint8_t *) &v4name.sin_addr.s_addr) [1],
// ((uint8_t *) &v4name.sin_addr.s_addr) [2],
// ((uint8_t *) &v4name.sin_addr.s_addr) [3],
// ntohs (v4name.sin_port),
(
	sendto (v4sox,
			ipv6_router_solicitation,
			sizeof (ipv6_router_solicitation),
			MSG_DONTWAIT,
			(struct sockaddr *) &v4name, sizeof (v4name)));
}


/*
 * Send a Probe message.  This is an UDP/IPv4 message with no contents.
 * The remote peer will not respond, but that is okay; outgoing traffic is the
 * way to keep holes in NAT and firewalls open.
 */
void probe (uint8_t ip4udp [6]) {
	v4name.sin_family = AF_INET;
	memcpy (&v4name.sin_addr.s_addr, ip4udp + 0, 4);
	memcpy (&v4name.sin_port,        ip4udp + 4, 2);
	sendto (v4sox,
			"",
			0,
			MSG_DONTWAIT,
			(struct sockaddr *) &v4name, sizeof (v4name));
}


/*
 * Send a KeepAlive message.  This is an UDP/IPv4 message with no contents.
 * The router will not respond, but that is okay; outgoing traffic is the
 * way to keep holes in NAT and firewalls open.
 */
void keepalive (void) {
	v4name.sin_family = AF_INET;
	memcpy (&v4name.sin_addr.s_addr, &v4listen, 4);
	v4name.sin_port = htons (STANDARD_6BED4_UDP_PORT);
	int done = 0;
	int secs = 1;
	setsockopt (v4sox, IPPROTO_IP, IP_TTL, &keepalive_ttl, sizeof (keepalive_ttl));
	sendto (v4sox,
			"",
			0,
			MSG_DONTWAIT,
			(struct sockaddr *) &v4name, sizeof (v4name));
	setsockopt (v4sox, IPPROTO_IP, IP_TTL, &v4ttl, sizeof (v4ttl));
}


/* Regular maintenance is a routine that runs regularly to do one of two
 * generic tasks: either it sends Router Solicitation messages to the
 * Public 6bed4 Service, or it sends an empty UDP message somewhat in its
 * direction to keep NAT/firewall holes open.
 */
void regular_maintenance (void) {
	if (!got_lladdr) {
		solicit_router ();
		maintenance_time_cycle <<= 1;
		maintenance_time_cycle += 1;
		if (maintenance_time_cycle > maintenance_time_cycle_max) {
			maintenance_time_cycle = maintenance_time_cycle_max;
		}
		syslog (LOG_INFO, "Sent Router Advertisement to Public 6bed4 Service, next attempt in %ld seconds\n", maintenance_time_cycle);
	} else {
		syslog (LOG_INFO, "Sending a KeepAlive message (empty UDP) to the 6bed4 Router\n");
		keepalive ();
		maintenance_time_cycle = maintenance_time_cycle_max;
	}
	maintenance_next += 1000 * maintenance_time_cycle;
}


/* Indicate that the running daemon should stop.  This is intended as a
 * signal handler function, and would instruct the select() loop to
 * terminate.
 */
void signal_daemon (int signum) {
	signalnum = signum;
}


/* Run the daemon core code, passing information between IPv4 and IPv6 and
 * responding to tunnel requests if so requested.  The loop ends when a
 * suitable signal is received: HUP, INT, KILL, TERM.
 */
void run_daemon (void) {
	atexit (exthook_del_offers);
	fd_set io;
	bool keep;
	maintenance_next = int32time (true, -1000);
	signal (SIGHUP,  signal_daemon);
	signal (SIGINT,  signal_daemon);
	signal (SIGKILL, signal_daemon);
	signal (SIGTERM, signal_daemon);
	FD_ZERO (&io);
	FD_SET (v4sox, &io);
	FD_SET (v6sox, &io);
	int nfds = (v4sox < v6sox)? (v6sox + 1): (v4sox + 1);
	while (signalnum == 0) {
		int32_t now32 = int32time (true, 0);
		if ((now32 - maintenance_next) > 0) {
			regular_maintenance ();
		}
		now32 = pns_expire (now32);
		if ((now32 - maintenance_next) > 0) {
			now32 = maintenance_next;
		}
		now32 -= int32time (true, 0);
		if (now32 < 0) {
			// Return from select() immediately
			now32 = 0;
		}
		struct timeval tout;
		tout.tv_sec = now32 / 1000;
		tout.tv_usec = (now32 % 1000) * 1000;
		syslog (LOG_INFO, "Patient for %ds, %dus (now32 = %d)", (int)tout.tv_sec, (int)tout.tv_usec, now32);
		if (select (nfds, &io, NULL, NULL, &tout) < 0) {
			if (errno == EINTR) {
				return;
			}
			syslog (LOG_ERR, "Select failed: %s\n", strerror (errno));
		}
		if (FD_ISSET (v4sox, &io)) {
			syslog (LOG_DEBUG, "Got unicast input\n");
			handle_4to6 (v4sox);
		} else {
			FD_SET (v4sox, &io);
		}
		if (FD_ISSET (v6sox, &io)) {
			handle_6to4 ();
		} else {
			FD_SET (v6sox, &io);
		}
//fflush (stdout);
	}
}


/* Option descriptive data structures */

char *short_opt = "s:t:d:D:l:p:rk:fex:h";

struct option long_opt [] = {
	{ "v4server", 1, NULL, 's' },
	{ "tundev", 1, NULL, 'd' },
	{ "linkdev", 1, NULL, 'D' },
	{ "default-route", 0, NULL, 'r' },
	{ "listen", 1, NULL, 'l' },
	{ "port", 1, NULL, 'p' },
	{ "ttl", 1, NULL, 't' },
	{ "foreground", 0, NULL, 'f' },
	{ "fork-not", 0, NULL, 'f' },
	{ "keepalive", 1, NULL, 'k' },
	{ "keepalive-period-ttl", 1, NULL, 'k' },
	{ "error-console", 0, NULL, 'e' },
	{ "extension-hook", 1, NULL, 'x' },
	{ "help", 0, NULL, 'h' },
	{ NULL, 0, NULL, 0 }	/* Array termination */
};


/* Parse commandline arguments (and start to process them).
 * Return 1 on success, 0 on failure.
 */
int process_args (int argc, char *argv []) {
	int ok = 1;
	int help = 0;
	int done = 0;
	unsigned long tmpport;
	char *endarg;
	default_route = false;
	while (!done) {
		switch (getopt_long (argc, argv, short_opt, long_opt, NULL)) {
		case -1:
			done = 1;
			if (optind != argc) {
				fprintf (stderr, "%s: Extra arguments not permitted: %s...\n", program, argv [optind]);
				ok = 0;
			}
			break;
		case 's':
			if (v4sox != -1) {
				ok = 0;
				fprintf (stderr, "%s: You can only specify a single server address\n", program);
				continue;
			}
			v4server = optarg;
			if (inet_pton (AF_INET, optarg, &v4peer.sin_addr) <= 0) {
				ok = 0;
				fprintf (stderr, "%s: Failed to parse IPv4 address %s\n", program, optarg);
				break;
			}
			memcpy (&v4listen, &v4peer.sin_addr, 4);
			memcpy (lladdr + 2, &v4listen, 4);
			v4sox = socket (AF_INET, SOCK_DGRAM, 0);
			if (v4sox == -1) {
				ok = 0;
				fprintf (stderr, "%s: Failed to allocate UDPv4 socket: %s\n", program, strerror (errno));
				break;
			}
			break;
		case 'd':
			if (v6sox != -1) {
				ok = 0;
				fprintf (stderr, "%s: Multiple -d arguments are not permitted\n", program);
				break;
			}
			v6sox = open (optarg, O_RDWR);
			if (v6sox == -1) {
				ok = 0;
				fprintf (stderr, "%s: Failed to open tunnel device %s: %s\n", program, optarg, strerror (errno));
				break;
			}
			break;
		case 'D':
			if (interface_name != INTERFACE_NAME_6BED4) {
				ok = 0;
				fprintf (stderr, "%s: Multiple -D arguments are not permitted\n", program);
				break;
			}
			interface_name = optarg;
			break;
		case 'r':
			if (default_route) {
				fprintf (stderr, "%s: You can only request default route setup once\n", program);
				ok = 0;
				break;
			}
			default_route = true;
			break;
		case 'l':
			if (inet_pton (AF_INET, optarg, &v4bind.sin_addr.s_addr) != 1) {
				fprintf (stderr, "%s: IPv4 address %s is not valid\n", program, optarg);
				ok = 0;
				break;
			}
			break;
		case 'p':
			tmpport = strtoul (optarg, &endarg, 10);
			if ((*endarg) || (tmpport > 65535)) {
				fprintf (stderr, "%s: UDP port number %s is not valid\n", program, optarg);
				ok = 0;
				break;
			}
			if (tmpport & 0x0001) {
				fprintf (stderr, "%s: UDP port number %ld is odd, which is not permitted\n", program, tmpport);
				ok = 0;
				break;
			}
			v4bind.sin_port = htons (tmpport);
			break;
		case 'f':
			if (foreground) {
				fprintf (stderr, "%s: You can only request foreground operation once\n", program);
				ok = 0;
				break;
			}
			foreground = true;
			break;
		case 'e':
			if (log_to_stderr) {
				fprintf (stderr, "%s: You can only specify logging to stderr once\n", program);
				ok = 0;
				break;
			}
			log_to_stderr = true;
			break;
		case 't':
			if (v4ttl_mcast != -1) {
				fprintf (stderr, "%s: You can set the ttl for multicast once\n", program);
				ok = 0;
				break;
			}
			char *zero;
			long setting = strtol(optarg, &zero, 10);
			if (*zero || (setting < 0) || (setting > 255)) {
				fprintf (stderr, "%s: Multicast radius must be a number in the range 0 to 255, inclusive, not %s\n", program, optarg);
				ok = 0;
				break;
			}
			v4ttl_mcast = setting;
			break;
		case 'k':
			if (keepalive_ttl != -1) {
				fprintf (stderr, "%s: You can only set the keepalive period and TTL once\n", program);
				ok = 0;
				break;
			}
			char *rest;
			keepalive_period = strtol (optarg, &rest, 10);
			if (*rest == ',') {
				rest++;
				keepalive_ttl = strtol (rest, &rest, 10);
				if ((keepalive_ttl < 0) || (keepalive_ttl > 255)) {
					fprintf (stderr, "%s: The keepalive TTL must be in the range 0 to 255, inclusive\n", program);
					ok = 0;
					break;
				}
			} else {
				keepalive_ttl = 3;
			}
			if (*rest) {
				fprintf (stderr, "%s: The format for keepalive configuration is 'period,ttl' or just 'period', but not %s\n", program, optarg);
				exit (1);
			}
			break;
		case 'x':
			if (exthook != NULL) {
				fprintf (stderr, "%s: You can only specify one extension hook\n", program);
				ok = 0;
				break;
			}
			exthook = optarg;
			break;
		default:
			ok = 0;
			/* continue into 'h' to produce usage information */
		case 'h':
			help = 1;
			break;
		}
		if (help || !ok) {
			done = 1;
		}
	}
	if (help) {
#ifdef HAVE_SETUP_TUNNEL
		fprintf (stderr, "Usage: %s [-r] [-f] [-t <hops>] [-d /dev/tunX] [-D <ifname>] [-l <v4addr>] [-p <port>] [-x <script>] [-s <v4addr>]\n       %s -h\n", program, program);
#else
		fprintf (stderr, "Usage: %s [-r] [-f] [-t <hops>] -d /dev/tunX [-D <ifname>] [-l <v4addr>] [-p <port>] [-x <script>] [-s <v4addr>]\n       %s -h\n", program, program);
#endif
		return 0;
	}
	if (!ok) {
		return 0;
	}
	if (keepalive_ttl != -1) {
		maintenance_time_cycle_max = keepalive_period;
	} else {
		keepalive_ttl = 3;
	}
#ifdef HAVE_SETUP_TUNNEL
	if (v6sox == -1) {
		if (geteuid () != 0) {
			fprintf (stderr, "%s: You should be root, or use -d to specify an accessible tunnel device\n", program);
			return 0;
		} else {
			ok = setup_tunnel ();
			if (!ok) {
				fprintf (stderr, "Failed to setup tunnel\n");
			}
		}
	}
#else /* ! HAVE_SETUP_TUNNEL */
	if (v6sox == -1) {
		fprintf (stderr, "%s: You must specify a tunnel device with -d that is accessible to you\n", program);
		return 0;
	}
#endif /* HAVE_SETUP_TUNNEL */
	return ok;
}


/* The main program parses commandline arguments and forks off the daemon
 */
int main (int argc, char *argv []) {
	//
	// Initialise
	program = strrchr (argv [0], '/');
	if (program) {
		program++;
	} else {
		program = argv [0];
	}
	memset (&v4name, 0, sizeof (v4name));
	memset (&v4peer, 0, sizeof (v4peer));
	memset (&v6name, 0, sizeof (v6name));
	v4name.sin_family  = AF_INET ;
	v4peer.sin_family  = AF_INET ;
	v6name.sin6_family = AF_INET6;
	// Fixed public server data, IPv4 and UDP:
	v4server = DEFAULT_6BED4_SERVER_IPV4;
	assert (inet_pton (AF_INET, v4server, &v4peer.sin_addr) > 0);
	v4name.sin_port = v4peer.sin_port = htons (STANDARD_6BED4_UDP_PORT);
	memcpy (&v4listen, &v4peer.sin_addr, 4);
	memcpy (lladdr + 0, &v4peer.sin_port, 2);
	memcpy (lladdr + 2, &v4peer.sin_addr, 4);
	memset (&v4bind, 0, sizeof (v4bind));
	v4bind.sin_family = AF_INET;
	v4tunpi6.flags = 0;
	v4tunpi6.proto = htons (ETH_P_IPV6);
	//
	// Parse commandline arguments
	openlog (program, LOG_NDELAY | LOG_PID | LOG_PERROR, LOG_DAEMON);
	if (!process_args (argc, argv)) {
		fprintf (stderr, "Argument processing failed\n");
		exit (1);
	}
	if (!log_to_stderr) {
		closelog ();
		openlog (program, LOG_NDELAY | LOG_PID, LOG_DAEMON);
	}
	//
	// Construct the 6bed4 Router's complete link-layer address
	memcpy (router_linklocal_address_complete + 8, &v4peer.sin_addr.s_addr, 4);
	router_linklocal_address_complete [12] = STANDARD_6BED4_UDP_PORT >> 8;
	router_linklocal_address_complete [13] = STANDARD_6BED4_UDP_PORT & 0xff;
	router_linklocal_address_complete [14] = router_linklocal_address_complete [12] << 6;
	router_linklocal_address_complete [12] &= 0xfc;
	//
	// Create socket for normal outgoing (and return) 6bed4 traffic
	if (v4sox == -1) {
		v4sox = socket (AF_INET, SOCK_DGRAM, 0);
		if (v4sox == -1) {
			syslog (LOG_CRIT, "%s: Failed to open a local IPv4 socket -- does your system still support IPv4?\n", program);
			exit (1);
		}
	}
	struct sockaddr_in tmpaddr;
	memset (&tmpaddr, 0, sizeof (tmpaddr));
	tmpaddr.sin_family = AF_INET;
	srand (getpid ());
	uint16_t portn = rand () & 0x3ffe;
	uint16_t port0 = portn + 16384;
	//TODO// Move port iteration + allocation to separate function
	while (portn < port0) {
		tmpaddr.sin_port = htons ((portn & 0x3ffe) + 49152);
		if (bind (v4sox, (struct sockaddr *) &tmpaddr, sizeof (tmpaddr)) == 0) {
			break;
		}
		portn += 2;
	}
	if (portn < port0) {
		syslog (LOG_DEBUG, "Bound to UDP port %d\n", ntohs (tmpaddr.sin_port));
	} else {
		fprintf (stderr, "%s: All even dynamic ports rejected binding: %s\n", program, strerror (errno));
		exit (1);
	}
	//
	// Setup fragmentation, QoS and TTL options
	u_int yes = 1;
	u_int no = 0;
#if defined(IP_DONTFRAG)
	if (setsockopt (v4sox, IPPROTO_IP, IP_DONTFRAG, no, sizeof (no)) == -1) {
		syslog (LOG_ERR, "Failed to permit fragmentation -- not all peers may be accessible with MTU 1280");
	}
#elif defined(IP_MTU_DISCOVER) && defined(IP_PMTUDISC_DONT)
	int pmtuflag = IP_PMTUDISC_DONT;
	if (setsockopt (v4sox, IP_MTU_DISCOVER, IP_MTU_DISCOVER, &pmtuflag, sizeof (pmtuflag)) == -1) {
		syslog (LOG_ERR, "Failed to permit fragmentation -- not all peers may be accessible with MTU 1280");
	}
#else
#warning "Target system lacks support for controlling packet fragmentation"
#endif
	socklen_t soxlen = sizeof (v4qos);
	if (getsockopt (v4sox, IPPROTO_IP, IP_TOS, &v4qos, &soxlen) == -1) {
		syslog (LOG_ERR, "Quality of Service is not supported by the IPv4-side socket");
		v4qos = 0;
	}
	v6tc = v4qos;
	soxlen = sizeof (v4ttl);
	if (getsockopt (v4sox, IPPROTO_IP, IP_TTL, &v4ttl, &soxlen) == -1) {
		syslog (LOG_ERR, "Time To Live cannot be varied on the IPv4 socket");
		v4ttl = 64;
	}
	//
	// Bind to the IPv4 all-nodes local multicast address
	memset (&v4allnodes, 0, sizeof (v4allnodes));
	v4allnodes.sin_family = AF_INET;
	v4allnodes.sin_port = htons (STANDARD_6BED4_UDP_PORT);
	v4allnodes.sin_addr.s_addr = htonl ( INADDR_ANY );
	//
	// If port and/or listen arguments were provided, bind to them
	if ((v4bind.sin_addr.s_addr != INADDR_ANY) || (v4bind.sin_port != 0)) {
		if (bind (v4sox, (struct sockaddr *) &v4bind, sizeof (v4bind)) != 0) {
			syslog (LOG_CRIT, "%s: Failed to bind to local socket -- did you specify both address and port?\n", program);
			exit (1);
		}
	}
	//
	// Run the daemon
	if (foreground) {
		run_daemon ();
	} else {
		if (setsid () != -1) {
			syslog (LOG_CRIT, "%s: Failure to detach from parent session: %s\n", program, strerror (errno));
			exit (1);
		}
		switch (fork ()) {
		case -1:
			syslog (LOG_CRIT, "%s: Failure to fork daemon process: %s\n", program, strerror (errno));
			exit (1);
		case 0:
			close (0);
			if (! log_to_stderr) {
				close (1);
				close (2);
			}
			run_daemon ();
			break;
		default:
			break;
		}
	}
	//
	// Report successful creation of the daemon
	closelog ();
	exit (0);
}

