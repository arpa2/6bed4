/* Socket(ish) API for 6bed4 -- empowering IPv6 everywhere.
 *
 * This is a library module to implement 6bed4 logic.
 * It is inspired on the POSIX socket API, but modified
 * to come closer to the modern use of event loops.
 *
 * General facilities supported here are:
 *  - raw IPv6 sockets with recv/send functions
 *  - event listening: UDP/IPv4 socket, next timer to expire
 *  - looking for other 6bed4router and 6bed4peer
 *  - binding to a suitable IPv6 address
 *  - callback hooks for prefix updates
 *  - traffic class to control routing policies
 *
 * This socketish API should be an application's entry
 * into 6bed4-based peer-to-peer networking, thanks to
 * abstractions (like IPv6) that simplify NAT traversal.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#ifndef NETINET_6BED4
#define NETINET_6BED4


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <netinet/in.h>
#include <netinet/ip6.h>
#include <netinet/6bed4.h>

// Local definitions, notably STANDARD_6BED4_UDP_PORT
#include "nonstd.h"



/*
 * Socket6bed4 is an abstract data type, and used in a similar
 * fashion to integer file/socket descriptors.  This abstraction
 * is used everywhere in the API.  We also define an invalid
 * value, to be used instead of NULL.  Think of it as a way to
 * update to integers in another time or on another platform.
 */
typedef struct socket6bed4 *Socket6bed4;
#define NO6BED4 NULL


/*
 * Allocate a new Socket6bed4 context.  This is a raw IPv6 socket to
 * which many local IPv6 addresses can be bound, along with many routes
 * to different 6bed4peer and 6bed4router remotes.  At the time of
 * creation, no 6bed4router is assigned; for each prefix to which reliable
 * peering is desired, you should add its 6bed4router.  You may supply a
 * local IPv4 address and/or UDP port to bind to.
 *
 * On successful return, the socket is stored in soxvar and 0 is returned.
 * On failure, -1 is returned and errno is set.
 */
int socket6bed4 (Socket6bed4 *soxvar, const struct sockaddr_in *opt_bindaddr);


/*
 * Close the Socket6bed4 and related resources.
 */
void close6bed4 (Socket6bed4 sox);


/*
 * Obtain the file/socket descriptor int to which an event loop can
 * subscribe to learn about work to do.
 */
int listenfd6bed4 (Socket6bed4 sox);


/* Link-local addresses as used by 6bed4 are a 6-byte sequence of the
 * IPv4 address and UDP port, both in network byte order.  They may be
 * used to decribe a 6bed4router or 6bed4peer.
 */
typedef struct lladdr6bed4 {
	union {
		uint8_t all [6];
		struct {
			uint32_t ip4;
			uint16_t udp;
		} _s;
	} _u;
} lladdr6bed4;
//
#define ll6bed4_all _u.all
#define ll6bed4_ip4 _u._s.ip4
#define ll6bed4_udp _u._s.udp


/*
 * The MTU for 6bed4 frames matches the IPv6 minimum.
 */
#define MTU6BED4 1280



/*
 * Peering Policies, as defined by the TRAFFIC-CLASS.MD document.
 *  - "proper" peering uses 6bed4router as a fallback, but tries to
 *    switch to direct peering as soon as possible;
 *  - "prohibited" peering always goes through the 6bed4router and
 *    may deliver higher delay but less jitter;
 *  - "presumptions" peering aims for lower delay by trying more
 *    agressively to connect to the peer, falling back only after
 *    a few seconds of failure;
 *  - "persistent" peering refuses to go through the 6bed4router,
 *    and will send occasional ICMP errors after a few seconds of
 *    failure; it should have the lowest delay and jitter and the
 *    highest privacy, but at the risk of maybe not connecting
 *    properly to all peers; close to Teredo, in fact.
 */
#define PEERPOL_6BED4_PROPER       ((uint8_t) 0x00)
#define PEERPOL_6BED4_PROHIBITED   ((uint8_t) 0x40)
#define PEERPOL_6BED4_PRESUMPTIOUS ((uint8_t) 0x80)
#define PEERPOL_6BED4_PERSISTENT   ((uint8_t) 0xc0)




/*
 * Retrieve the link-local address (in 6bed4 style) from the IPv6 address.
 * Set the flag to extract the 6bed4router address instead of the 6bed4peer.
 */
int linkaddr6bed4 (const struct in6_addr *in_ipv6, bool extract_router, lladdr6bed4 *out_lladdr);



/*
 * Allocate a new Socket6bed4 context.  This is a raw IPv6 socket to
 * which many local IPv6 addresses can be bound, along with many routes
 * to different 6bed4peer and 6bed4router remotes.  At the time of
 * creation, no 6bed4router is assigned; for each prefix to which reliable
 * peering is desired, you should add its 6bed4router.  You may supply a
 * local IPv4 address and/or UDP port to bind to.
 *
 * On successful return, the socket is stored in soxvar and 0 is returned.
 * On failure, -1 is returned and errno is set.
 */
int socket6bed4 (Socket6bed4 *soxvar, const struct sockaddr_in *opt_bindaddr);


/*
 * Close the Socket6bed4 and related resources.
 */
void close6bed4 (Socket6bed4 sox);


/*
 * Obtain the file/socket descriptor int to which an event loop can
 * subscribe with select() and variants to learn about work to do.
 */
int listenfd6bed4 (Socket6bed4 sox);


/*
 * Return the first timer expiration moment.  This is useful
 * for an external event loop as a wakeup moment.  The time
 * returned is a number of milliseconds relative to the current
 * time, and it is at least 0.
 */
int32_t nextalarm6bed4 (Socket6bed4 sox);


/*
 * Handle timer expiration.  This is usually called by an external
 * event loop shortly after the nextalarm6bed4() has passed.  This
 * allows timing to a millisecond scale.
 *
 * The maintenance done here is for timers of:
 *  - Keepalives towards 6bed4routers to which we have bound addresses
 *  - Probes within the Peer NAT State timing and state diagram
 *  - ???
 *
 * Use the nextalarm6bed4() call after this one finishes and perhaps
 * other work (on 6bed4 or other elements) has completed.  This
 * routine does not return the next time to allow maximum accuracy;
 * it does prepare absolute wakeup times to allow fast completion
 * of nextarlarm6bed4() to not interfere with accuracy of other
 * tasks in the external event loop.
 */
void alarm6bed4 (Socket6bed4 sox);


/*
 * This callback type is used when a Router Advertisement arrives for a given
 * 6bed4lladdr.  It informs the application of the (old and) new prefix with a
 * fixed /114 length, and passes along any routes that were advertised.  It is
 * also helpful to trigger further processing, such as the ability to use
 * bind6bed4() to bind IPv6 addresses under the given lladdr.
 *
 * A first callback is usually triggered by the solicit6bed4route() call, but
 * a remote may send back a corrective Router Advertisement at any time it
 * sees fit; usually, repeats will be to instruct us that our NAT mapping has
 * changed and we need to change our IPv6 address; under proper 6bed4 procedures
 * such repeated notifications should be avoided, but it is possible to use
 * other, more experimental procedures and learn through this callback if it
 * worked out.
 *
 * The old_prefix is NULL when no old prefix was known.  Routes are only
 * presented in response to an incoming Router Advertisement; canned calls
 * will set routes to NULL and num_routes to -1 to signal that existing
 * routing information should not be overwritten.  If a default route is
 * provided, it will be included as ::/0 in the routes and be counted in
 * num_routes.
 */
typedef void (*advertise6bed4route_cb) (
		Socket6bed4 sox, void *cbdata,
		lladdr6bed4 *lladdr,
		struct in6_addr *old_prefix, struct in6_addr *new_prefix,
		int num_routes, struct in6_addr *routes, uint8_t *prefixlens);


/*
 * Setup the callback function if it is not NULL, and attempt to have it called.
 * If the callback function is NULL, it will simply be removed.  Construct a new
 * router entry in the Peer NAT State tables if none exists yet.
 *
 * An attempt to have the callback function called will initially be done by
 * sending a Router Solicitation.  If a Router Advertisement was received before,
 * the callback will be invoked immediately with the information at hand.
 *
 * The function returns -1/errno on failure.  A return value 0 indicates a
 * direct/trivial success including callback if appropriate, whereas 1 indicates
 * that a Router Solicitation was actually sent and the application may want
 * to wait until a timeout for the callback to be triggered (or else resend).
 */
int solicit6bed4route (Socket6bed4 sox,
				advertise6bed4route_cb cbfun, void *cbdata,
				lladdr6bed4 *lladdr);


/*
 * Bind an address for a given remote address, based on the prefix given by
 * the 6bed4router or, in case direct peering is flagged, by the 6bed4peer.
 * Use the indicated lanid, which must be in the range 1..16383 to fit.
 * You must have received an advertise6bed4() callback for the remote
 * before you can use this function.
 *
 * Address binding replaces the bind() call on a normal socket, and may be
 * used many times to obtain many addresses.  The addresses are based on
 * Router Solicitation, even for direct peering, and Keepalives will be
 * turned on.
 *
 * Direct peering is not a reliable technique; it is a desparate one.
 * When your NAT is symmetric, it would assign different external ports
 * for different remotes, so a 6bed4peer and its 6bed4router would end
 * up having different IPv6 addresses and you could not switch from the
 * 6bed4router to direct routing to the 6bed4peer.  It is not clear if
 * and how you would bootstrap your connection to the 6bed4peer in this
 * case, and you would need to do more work in the application to know
 * that various 6bed4peers in fact belong to a 6bed4router.  Only if
 * you wish to completely avoid going through the 6bed4router and are
 * willing to sacrifice reliability or expend a lot of work for it is
 * direct peering really an option -- and even then, a confusing one.
 *
 * The Keepalives turn a bound address into a resource, so you should
 * use unbind6bed4() to free the resources for Keepalive sending.
 */
int bind6bed4 (Socket6bed4 sox, const struct in6_addr *remote, bool direct, uint16_t lanid, struct in6_addr *local);
//
int unbind6bed4 (Socket6bed4 sox, const struct in6_addr *local);


/*
 * Send a raw IPv6 packet out through the Socket6bed4 network.
 * The interface is similar to send() and returns the number of bytes
 * sent (starting from the IPv6 header) or -1 with errno on error.
 *
 * Specifically worth noting are these errors:
 *  - ENOTCONN is reported when a route to the remote does not exist;
 *  - ECONNRESET may be reported when NAT state is no longer valid.
 *
 * Among the flags, you probably want to use MSG_DONTWAIT, to stave
 * off synchronous blocking on the UDP/IPv4 socket.
 *
 * Note: IPv6 headers may be modified.  This breaks with tradition,
 *       but saves buffer copies just-to-make-sure.
 */
int send6bed4 (Socket6bed4 sox, struct ip6_hdr *raw6, size_t len, int flags);


/*
 * Receive a raw IPv6 packet that comes in over the Socket6bed4 network.
 * The interface is similar to recv() and returns the number of bytes
 * sent (starting from the IPv6 header) or -1 with errno on error.
 *
 * Specifically worth noting are these errors:
 *  - ENOTCONN is reported when a route to the remote does not exist;
 *  - ECONNRESET may be reported when NAT state is no longer valid.
 *
 * Among the flags, you probably want to use MSG_DONTWAIT, to stave
 * off synchronous blocking on the UDP/IPv4 socket.  The routine will
 * loop if multiple messages may be received, but the loop is broken
 * when either an actual message arrives or an error condition occurs
 * that should be reported to the user (including EAGAIN/EWOULDBLOCK).
 */
int recv6bed4 (Socket6bed4 sox, struct ip6_hdr *raw6, size_t maxlen, int flags);



#endif /* NETINET_6BED4 */
