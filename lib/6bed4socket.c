/* Socket(ish) API for 6bed4 -- empowering IPv6 everywhere.
 *
 * This is a library module to implement 6bed4 logic.
 * It is inspired on the POSIX socket API, but modified
 * to come closer to the modern use of event loops.
 *
 * General facilities supported here are:
 *  - raw IPv6 sockets with recv/send functions
 *  - event listening: UDP/IPv4 socket, next timer to expire
 *  - looking for other 6bed4router and 6bed4peer
 *  - binding to a suitable IPv6 address
 *  - callback hooks for prefix updates
 *  - traffic class to control routing policies
 *
 * This socketish API should be an application's entry
 * into 6bed4-based peer-to-peer networking, thanks to
 * abstractions (like IPv6) that simplify NAT traversal.
 *
 * From: Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>

#include <unistd.h>
#include <syslog.h>
#include <errno.h>

#include <sys/time.h>

#include <netinet/in.h>
#include <netinet/ip6.h>
#include <netinet/icmp6.h>
#include <netinet/6bed4.h>


/*
 * The UThash.h and UTlist.h headers support hash table and list.
 * They are not _completely_ optimal, as we'd rather split the prev,next
 * doubly-linked list that comes with the hash, but the simplicity is
 * more of a concern for now, so we'll accept the storage overhead.
 */
#include "utlist.h"
#include "uthash.h"



/*
 *
 * DATA STRUCTURES
 *
 */


/*
 * Internal structure for the abstract datatype Socket6bed4.
 */
struct socket6bed4 {
	//
	// udpsox is the UDP/IPv4 socket used for all 6bed4 communication.
	// It has a one-to-one relation to the scope of a Socket6bed4.
	int udpsox;
	//
	// The UDP/IPv4 address for this 6bed4 socket.
	lladdr6bed4 listen4;
	//
	// The last Traffic Class sent over this socket.  This is used to
	// call setsockopt() for TOS changes on udpsox only when needed.
	uint8_t last_trfcls_sent;
	//
	// Normal hop limit value
	uint8_t normal_hoplimit;
	//
	// The last time sampled in ms, cut back to a 32-bit signed int.
	int32_t now32time;
	//
	// The next timeout in int32 time notation (see below).
	int32_t next_timeout;
	//
	// Following Peer NAT State structures are defined below.
	//
	// The hash entry to find peers' NAT states.
	struct pns_t *pns_hash;
	//
	// The queue heads for the 01s/25s and Keepalive queues.
	struct pns_t  *pns_q01s;
	struct pns_t  *pns_q25s;
	struct pns_t  *pns_qeep;
	//
	// Keepalive settings: Interval and hop limit
	int32_t keepalive_interval;
	uint8_t keepalive_hoplimit;
};


/*
 * Peer NAT is represented with two timers and a state.  The state is
 * composed in groups that enable quick inspection.
 *
 * Timers are close to local time and are stored with millisecond
 * resolution and wrap-around.  They can be set to expire by adding
 * milliseconds to "now", and tested by subtracting "now" later.
 * A positive subtraction outcome counts as expiration.  Note how
 * this relies on C/assembly wrap-around of integers on subtraction!
 *
 * Routers are a class of their own.  They are mentioned with a
 * Peer NAT State that flags them as a router, with direct routing
 * and normally on the slowest loop.
 */
#define PNS_COUNT_MASK		0x0f
#define PNS_LOOP_25S		0x10
#define PNS_DIRECT		0x20
#define PNS_FAILED		0x40
#define PNS_ROUTER		0x80
//
#define PNS_COUNTER_DONE(c) (((c+1) & PNS_COUNT_MASK) == 0)
#define PNS_COUNTER(c) ((c) & PNS_COUNT_MASK)
//
#define PNS_INIT0 ( PNS_COUNTER(-2)                             )
#define PNS_INIT1 ( PNS_COUNTER(-1)                             )
#define PNS_FAIL0 ( PNS_COUNTER(-2) | PNS_LOOP_25S | PNS_FAILED )
#define PNS_FAIL1 ( PNS_COUNTER(-1) | PNS_LOOP_25S | PNS_FAILED )
#define PNS_POLL0 ( PNS_COUNTER(-2)                | PNS_DIRECT )
#define PNS_POLL1 ( PNS_COUNTER(-1)                | PNS_DIRECT )
#define PNS_PEER  ( PNS_COUNTER(-1) | PNS_LOOP_25S | PNS_DIRECT )
#define PNS_RSOL0 ( PNS_COUNTER(-4)                | PNS_ROUTER )
#define PNS_RSOL1 ( PNS_COUNTER(-3)                | PNS_ROUTER )
#define PNS_RSOL2 ( PNS_COUNTER(-2) | PNS_LOOP_25S | PNS_ROUTER )
#define PNS_ROUTE ( PNS_COUNTER(-1)                | PNS_ROUTER )
//
struct pns_t {
	//
	// The current state.  By default, increment the state and
	// perform a minor action and schedule at the back of the
	// same queue.  When the counter wraps, perform a major
	// action and expect to change state.
	//
	uint8_t pns_state;
	//
	// Timers for a few purposes.
	//  - pns_state_until expires when the state should change
	//  - pns_seen_until  marks how long Seen can be sent
	//  - pns_last_send   marks the timing of the last 4to6 packet
	//  - pns_last_direct marks the last direct pns_last_send
	//
	int32_t pns_state_until;
	int32_t pns_seen_until;
	int32_t pns_last_send;
	int32_t pns_last_direct;
	//
	// The hash structure, built around the key pair formed
	// by an IPv4 address and a UDP port number.  This is how
	// peers are identified in this queue, and how their state
	// will be found and updated.  Together these are known as
	// the "link local address" for the peer.
	//
	UT_hash_handle hh;
	lladdr6bed4 pns_ip4udp;
	//
	// The timer queue for pns_state is always active.  When
	// it expires, a new state is entered and the corresponding
	// queue is appended with this structure.  The queue is
	// doubly linked for constant-time removal of elements.
	struct pns_t *pns_next, *pns_prev;
	//
	// As long as the keepalive flag is set, regular messages
	// continue to be sent until outside local NAT layers.
	// This is done to keep a hole in NAT open.
#if DIRECT_KEEPALIVE_INSTEAD_OF_ADDRESS_BINDING
	bool keepalive;
#endif
	//
	// As long as at least one address is bound, regular
	// Keepalives are being sent to this lladdr to keep a
	// hole in NAT open.  This is a simple counter that
	// tracks the number of bind6bed4() calls that were
	// not mirrorred with unbind6bed4().
	uint32_t bindings;
	//
	// The IPv6 prefix retrieved from a Router Advertisement
	// over this link.  In case of a 6bed4peer, this would
	// be a shared resource, but for a 6bed4router it ought
	// to be uniquely owned by that party.  The prefix for
	// a 6bed4peer starts as a /64 prefix filled with 0's,
	// as can be readily seen from the lower-half UDP port.
	// Both 6bed4peer and 6bed4router can change the lower
	// half to indicate how your connection transis NAT;
	// the 6bed4router sets this immediately by sharing a
	// prefix of a /114 size.  If the prefix is 0::/8 then
	// this value is completely unassigned.
	struct in6_addr prefix;
	//
	// The callback registration for Router Advertisements.
	// This is usually setup before sending a Router
	// Solicitation, to gather both the information in it
	// and to learn about the timing of its arrival.
	// Additionall callbacks may be made when our assigned
	// prefix /114 changes, presumably due to a NAT mapping
	// change.
	advertise6bed4route_cb rtadv_cbfun;
	void                  *rtadv_cbdata;
};



/*
 *
 * GLOBALS -- SHOULD BE ON THEIR WAY OUT
 *
 */



#define netport6bed4 (htons (STANDARD_6BED4_UDP_PORT))




/*
 *
 * OFFSETS, MACROS AND SIMPLE FUNCTIONS FOR IP6 HEADERS
 *
 */



/* Macro's to get and update the traffic class in an IPv6 header.
 * Traffic Class means: The full bits 4..11 in the header.
 * This involves 6 DSCP bits as well as 2 ECN bits.
 *
 * Update means: Assuming it currently is a 6bed4 traffic class,
 * so only the first nibble matters.  The ECN is not updated.
 */
#define get_trafficclass(hdr) ( \
	((((uint8_t *) &(hdr)) [0] & 0x0f) << 4) | \
	((((uint8_t *) &(hdr)) [1] & 0xf0) >> 4) )
//
// #define upd_trafficclass(hdr,newtc) { \
// 	((uint8_t *) &(hdr)) [0] &= 0xf0; \
// 	((uint8_t *) &(hdr)) [0] |= ((newtc >> 4)); }


struct ip6icmp6 {
	struct ip6_hdr h;
	struct icmp6_hdr i;
};
//
#define icmp6type(raw6) (((struct ip6icmp6 *) (raw6))->i.icmp6_type)
#define icmp6code(raw6) (((struct ip6icmp6 *) (raw6))->i.icmp6_code)
#define icmp6csum(raw6) (((struct ip6icmp6 *) (raw6))->i.icmp6_cksum)
#define icmp6data(raw6) (((struct ip6icmp6 *) (raw6))->i.icmp6_data8)


/*
 * Calculate the ICMPv6 checksum field
 */
static uint16_t icmp6_checksum (uint8_t *frame, size_t framelen) {
	framelen -= sizeof (struct ip6_hdr);
	uint16_t plenword = htons (framelen);
	uint16_t nxthword = htons (IPPROTO_ICMPV6);
	uint16_t *areaptr [] = { (uint16_t *) &frame [8], (uint16_t *) &frame [24], &plenword, &nxthword, (uint16_t *) &frame [40], (uint16_t *) &frame [40 + 4] };
	uint8_t areawords [] = { 8, 8, 1, 1, 1, framelen/2 - 2 };
	uint32_t csum = 0;
	u_int8_t i, j;
	for (i=0; i < 6; i++) {
		uint16_t *area = areaptr [i];
		for (j=0; j<areawords [i]; j++) {
			csum += ntohs (area [j]);
		}
	}
	csum = (csum & 0xffff) + (csum >> 16);
	csum = (csum & 0xffff) + (csum >> 16);
	csum = htons (~csum);
	return csum;
}


/*
 *
 * ADDRESS AND TIME UTILITIES
 *
 */



/*
 * Find the current time as a 32-bit cycling integer with
 * roughly millisecond resolution.
 *
 * We subtract these and learn from the wrapped difference
 * how it compares to 0 to decide what to do.
 *
 * It is a good * principle to ask for this value once per
 * event cycle, to save on system calls as well as have a
 * stable time.  This routine uses the socket6bed4 to store
 * and retrieve the last time the "reload" flag was set.
 *
 * The function can look ahead in time with a positive
 * "offset" value, or look back when it is negative.
 */
//TODO// Consider a split: time32sample, time32ofs, time32cmp
static int32_t _int32time (Socket6bed4 sox, bool reload, int32_t offset) {
	if (reload) {
		struct timeval sample;
		gettimeofday (&sample, NULL);
		// Embedded form: Would also work with <<10 and >>10
		sox->now32time = (sample.tv_sec * 1000) + (sample.tv_usec / 1000);
	}
	return sox->now32time + offset;
}


/*
 * Test if the provided IPv6 address matches the /64 prefix that we
 * offer routes for (as set with the -L option or derived for it).
 */
#ifdef CLARIFIED_MEANING
static inline bool is_mine (const struct in6_addr *ip6, struct pns_t *pns) {
	if (pns == NULL) {
		return false;
	} else {
		return memcmp (ip6->s6_addr, pns->prefix, 8) == 0;
	}
}
#endif


/*
 * Test if the provided IPv6 address matches the prefix used for 6bed4.
 */
static inline bool is_6bed4 (const struct in6_addr *ip6) {
	if (ip6->s6_addr16 [0] == htons (0xfc64)) {
		return true;
	}
#ifdef TBD1
	if (ip6->s6_addr32 [0] == htonl (TBD1)) {
		return true;
	}
#endif
	return false;
}


/*
 * Test if the provided IPv6 address matches the prefix used for 6bed4
 * and is ours because the IPv4 matches too.  A <netid> is untested.
 */
#ifdef CLARIFIED_MEANING
static inline bool is_6bed4_mine (const struct in6_addr *ip6, uint32_t myv4addr_netorder) {
	if (ip6->s6_addr32 [1] != myv4addr_netorder) {
		return false;
	}
	return is_6bed4 (ip6);
}
#endif


/*
 * Test if the provided IPv6 address matches the prefix used for 6bed4
 * but is not ours because the IPv4 does not match.
 */
#ifdef CLARIFIED_MEANING
static inline bool is_6bed4_other (const struct in6_addr *ip6, uint32_t myv4addr_netorder) {
	if (ip6->s6_addr32 [1] == myv4addr_netorder) {
		return false;
	}
	return is_6bed4 (ip6);
}
#endif


/* 
 * Test if the provided IPv6 address matches the /64 prefix in the
 * Peer NAT State.  For native prefixes, this compares 64 bits as-is;
 * for 6bed4 addresses, this tests the top-half IPv4 address but not
 * a <netid> if one is used.
 */
static inline bool is_prefix64match (const struct in6_addr *ip6, const struct pns_t *pns) {
	if (pns == NULL) {
		return false;
	} else if (pns->prefix.s6_addr [0] == 0x00) {
		return false;
	} else if (ip6->s6_addr16 [0] == htons (0xfc64)) {
		// For fc64::/16 we check the IPv4 address but not the <netid>
		return (pns->prefix.s6_addr16 [0] == htons (0xfc64))
				&& (ip6->s6_addr32 [1] == pns->prefix.s6_addr32 [1]);
	} else {
		// Match either native /64 or TBD1::/32 + IPv4 address
		return memcmp (ip6, &pns->prefix, 8) == 0;
	}
}


/*
 * Macros to map the top or bottom half of an IPv6 address to an lladdr6bed4.
 * The assumption for the top half is that it is a 6bed4 address;
 * the assumption for the bottom half is that it is a 6bed4 destination address.
 */
#define top6bed4tolink(ip6,ip4udp) { \
	(ip4udp)->ll6bed4_ip4 = (ip6)->s6_addr32 [1]; \
	(ip4udp)->ll6bed4_udp = netport6bed4; }
//
#define bottom6bed4tolink(ip6,ip4udp) { \
	(ip4udp)->ll6bed4_all [0] = (ip6)->s6_addr [8] & 0xfc | ((ip6)->s6_addr [14] >> 6); \
	memcpy (&(ip4udp)->ll6bed4_all [1], &(ip6)->s6_addr [9], 5); }


/*
 * Macros to map an lladdr6bed4 to the top or bottom half of an IPv6 address.
 * The assumption for the top half is that it is a 6bed4 address and UDP is TBD2;
 * the assumption for the bottom half is that it is a 6bed4 destination address.
 */
#define link6bed4totop(ip4udp,ip6) { \
	assert ((ip4udp)->ll6bed4_udp == netport6bed4); \
	(ip6)->s6_addr32 [1] = (ip4udp)->ll6bed4_ip4; }
//
#define link6bed4tobottom(ip4udp,ip6) { \
	(ip6)->s6_addr [8] = (ip4udp)->ll6bed4_all [0] & 0xfc; \
	(ip6)->s6_addr [14] &= 0x3f; \
	(ip6)->s6_addr [14] |= ((ip4udp)->ll6bed4_all [0] & 0x03) << 6; \
	memcpy (&(ip6)->s6_addr [9], &(ip4udp)->ll6bed4_all [1], 5); }


/*
 * Retrieve the link-local address (in 6bed4 style) from the IPv6 address.
 * Set the flag to extract the 6bed4router address instead of the 6bed4peer.
 */
int linkaddr6bed4 (const struct in6_addr *in_ipv6, bool extract_router, lladdr6bed4 *out_lladdr) {
	if (extract_router) {
		if (!is_6bed4 (in_ipv6)) {
			errno = EINVAL;
			return -1;
		}
		top6bed4tolink    (in_ipv6, out_lladdr);
	} else {
		bottom6bed4tolink (in_ipv6, out_lladdr);
	}
	return 0;
}


/*
 * Validate the originator's IPv6 address.  It should match the
 * UDP/IPv4 coordinates of the receiving 6bed4 socket.  Also,
 * the /64 prefix (but not the /114 prefix!) must match v6listen.
 */
void advertise_route (Socket6bed4 sox, struct sockaddr_in *v4peer, struct in6_addr *src6, struct pns_t *pns);
//
static inline bool validate_originator (Socket6bed4 sox,
						struct pns_t *pns,
						struct sockaddr_in *v4peer,
						struct in6_addr *src6) {
	//
	// Communication from a configured router is welcomed
	// even when it comes from native/other prefixes as it may
	// route external traffic to us, either from its direct
	// backend or any public IPv6 address.  It may not even
	// have a lower half following 6bed4 rules, and if it
	// does the traffic may have been forwarded to us.
	if ((pns != NULL) && (pns->pns_state == PNS_ROUTE)) {
		return true;
	}
	//
	// Require non-local top halves to match our router's /64 prefix.
	// This matches fc64::/16 and TBD1::/32 with our 6bed4router's IPv4;
	// it also matches native /64 prefixes but not additional routes.
	// Finally, link local prefixes may be used for ICMPv6 messages.
	if (!is_prefix64match (src6, pns)) {
		return false;
	}
	//
	// Require the sender port to appear in its IPv6 address
	bool oklow = (v4peer->sin_port == src6->s6_addr16 [6]);
	//
	// Require the sender address to appear in its IPv6 address
	if (oklow) {
		uint32_t addr = ntohl (src6->s6_addr32 [2]) & 0xfcffffff;
		addr |= ((uint32_t) (src6->s6_addr [14] & 0xc0)) << (24-6);
		oklow = addr == ntohl (v4peer->sin_addr.s_addr);
	}
	//
	// If we received a proper /64 with incorrect completion to /114
	// we spontaneously respond with a Router Advertisement
	if (!oklow) {
		advertise_route (sox, v4peer, src6, pns);
	}
	//
	// Return the overall result
	return oklow;
}


/* Validing bypass traffic that was initiated over trapezium routing.
 * This means that the top half holds the IPv4 address of a 6bed4router,
 * both in source and destination IPv6 address, and our router is in
 * the target whereas the source's router is in the source IPv6 address.
 * In addition, the lower half logic applies.
 *
 * Note: This code is specific to a 6bed4peer, a 6bed4router differs.
 */
static struct pns_t *pns_findpeer (Socket6bed4 sox, lladdr6bed4 *ip4udp);
//
static inline bool validate_trapezium_bypass (Socket6bed4 sox,
						struct sockaddr_in *v4peer,
						struct in6_addr *src6,
						struct in6_addr *dst6) {
	//
	// Require the source and destination to have a 6bed4 top half,
	// though they may use different prefixes and/or <netid> values.
	if ((!is_6bed4 (src6)) || (!is_6bed4 (dst6))) {
		return false;
	}
	//
	// Ignore the top half IPv4 address in the source IPv6 address
	// as that depicts another 6bed4router, that may not be ours.
	;
	//
	// Match the source IPv4 address and port in the lower half
	// of the source IPv6 address
	lladdr6bed4 llsrcbottom;
	bottom6bed4tolink (src6, &llsrcbottom);
	if (llsrcbottom.ll6bed4_ip4 != v4peer->sin_addr.s_addr) {
		return false;
	}
	if (llsrcbottom.ll6bed4_ip4 != v4peer->sin_port) {
		return false;
	}
	//
	// The IPv4 in the top half of the IPv6 destination must be
	// the/a 6bed4router to which we deliberately connected.
	lladdr6bed4 lldsttop;
	top6bed4tolink (dst6, &lldsttop);
	struct pns_t *pnsdsttop = pns_findpeer (sox, &lldsttop);
	if (pnsdsttop == NULL) {
		return false;
	}
	if (pnsdsttop->pns_state == PNS_ROUTE) {
		return false;
	}
	//
	// Match the destination IPv4 address and port with the
	// destination IPv6 address; this information is supposed to
	// be used in trapezium routing, so it is is off we have a
	// problem.
	//TODO// Why are we loose about this for normal peering?
	lladdr6bed4 lldstbottom;
	bottom6bed4tolink (dst6, &lldstbottom);
	if (memcmp (&lldstbottom, &sox->listen4, sizeof (lladdr6bed4)) != 0) {
		return false;
	}
	//
	// All tests succeeded
	return true;
}



/*
 *
 * PEER NAT STATE MANAGEMENT
 *
 */



/*
 * Add a peer NAT state to the right queue, based on its state.
 * This does not change the hash table entrance.
 */
static void pns_enqueue (Socket6bed4 sox, struct pns_t *new) {
	struct pns_t **hd;
	int32_t ofs;
	if (new->pns_state == PNS_ROUTE) {
		hd = &sox->pns_qeep;
		ofs = sox->keepalive_interval;
	} else if (new->pns_state & PNS_LOOP_25S) {
		hd = &sox->pns_q25s;
		ofs = 25000;
	} else {
		hd = &sox->pns_q01s;
		ofs =  1000;
	}
	new->pns_state_until = _int32time (sox, false, ofs);
	DL_APPEND2 (*hd, new, pns_prev, pns_next);
}


/*
 * Remove a peer NAT state from its current timer queue.
 * This does not change the hash table entrance.
 */
static void pns_dequeue (Socket6bed4 sox, struct pns_t *old) {
	struct pns_t **hd;
	if (old->pns_state & PNS_ROUTE) {
		hd = &sox->pns_qeep;
	} else if (old->pns_state & PNS_LOOP_25S) {
		hd = &sox->pns_q25s;
	} else {
		hd = &sox->pns_q01s;
	}
	DL_DELETE2 (*hd, old, pns_prev, pns_next);
}


/*
 * Create a new peer NAT state for the given ip4udp.
 * Initialise everything and start in the given peerstate,
 * added to the right timer queue and enlisted in the hash.
 */
static struct pns_t *pns_addpeer (Socket6bed4 sox, lladdr6bed4 *ip4udp, uint8_t peerstate) {
	struct pns_t *new = malloc (sizeof (struct pns_t));
	if (new == NULL) {
		return NULL;
	}
	memset (new, 0, sizeof (struct pns_t));
	memcpy (&new->pns_ip4udp, &ip4udp, 6);
	int32_t expired = _int32time (sox, false, -60000);
	new->pns_seen_until  = expired;
	new->pns_last_send   = expired;
	new->pns_last_direct = expired;
	new->pns_state = peerstate;
	HASH_ADD (hh, sox->pns_hash, pns_ip4udp, 6, new);
	pns_enqueue (sox, new);
	return new;
}


/*
 * Find a peer NAT state for the given ip4udp.  Return
 * NULL when it is not yet known.
 */
static struct pns_t *pns_findpeer (Socket6bed4 sox, lladdr6bed4 *ip4udp) {
	struct pns_t *retval = NULL;
	HASH_FIND (hh, sox->pns_hash, ip4udp, 6, retval);
	return retval;
}


/*
 * Delete the peer NAT state for the given ip4udp.
 * This does not involve removal from the timer queue,
 * but it involves removal from the hash.
 */
static void pns_delpeer_bypns (Socket6bed4 sox, struct pns_t *old) {
	if (old == NULL) {
		return;
	}
	//NOT_TWICE// pns_dequeue (old);
	HASH_DEL (sox->pns_hash, old);
	free (old);
}
//
static void pns_delpeer (Socket6bed4 sox, lladdr6bed4 *ip4udp) {
	struct pns_t *old = pns_findpeer (sox, ip4udp);
	pns_delpeer_bypns (sox, old);
}



/*
 *
 * ELEMENTARY SEND AND RECEIVE FUNCTIONS
 *
 */



/* Send an ICMPv6 reply.  This is called with the IPv6 headers of the
 * message to which this is a reaction.  The ICMPv6 message has been
 * filled, with a given body length.  This routine updates the IPv6
 * header and computes the ICMPv6 checksum.  Then, it sends the message
 * over UDP/IPv4 to the provided socket address.
 *
 * Actions: v4/udp src becomes dest, set v4/udp/v6 src, len, cksum, send.
 *          reply is always to v4src6, except that if it starts with
 *	    0x00,0x00 it will be replaced with allnodes_linklocal_address.
 */
void icmp6_reply (Socket6bed4 sox, struct sockaddr_in *v4peer,
				struct in6_addr *replyto,
				struct ip6icmp6 *raw6, size_t icmp6bodylen) {
	if ((icmp6bodylen & 0x07) != 4) {
		return;   /* illegal length, drop */
	}
	raw6->h.ip6_flow = htonl (0x60000000);
	raw6->h.ip6_hlim = 255;
	raw6->h.ip6_plen = htons (icmp6bodylen + 4);
	raw6->h.ip6_nxt = IPPROTO_ICMPV6;
	memcpy (&raw6->h.ip6_dst, replyto, 16);
	raw6->h.ip6_src.s6_addr [0] = 0xfe;
	raw6->h.ip6_src.s6_addr [1] = 0x80;
	memset (&raw6->h.ip6_src.s6_addr [2], 0, 14);
	uint16_t raw6len = sizeof (struct ip6_hdr) + 4 + icmp6bodylen;	//TODO// +4 here and -4 in checksum???
	icmp6csum(raw6) = icmp6_checksum ((uint8_t *) raw6, ntohs (raw6len-4));
	//
	// Send the message to the IPv4 originator port
	sendto (sox->udpsox,
			raw6,
			raw6len,
			MSG_DONTWAIT,
			(struct sockaddr *) v4peer, sizeof (*v4peer));
}


/* Append the given prefix as /114 to an ICMPv6 message.  Incoming optidx
 * and return values signify original and new offset for ICMPv6 options.
 * The endlife parameter must be set to obtain zero lifetimes, thus
 * instructing the tunnel client to stop using an invalid prefix.
 */
size_t icmp6_prefix (uint8_t *icmp6data, size_t optidx, bool endlife,
				struct in6_addr *prefix,
				lladdr6bed4 ip4udp) {
	icmp6data [optidx++] = 3;	// Type
	icmp6data [optidx++] = 4;	// Length
	icmp6data [optidx++] = 114;	// This is a /114 prefix
	icmp6data [optidx++] = 0xc0;	// L=1, A=1, Reserved1=0
	memset (icmp6data + optidx, endlife? 0x00: 0xff, 8);
	optidx += 8;
					// Valid Lifetime: Zero / Infinite
					// Preferred Lifetime: Zero / Infinite
	memset (icmp6data + optidx, 0, 4);
	optidx += 4;
					// Reserved2=0
	memcpy (icmp6data + optidx + 0, prefix, 8);
	memset (icmp6data + optidx + 8, 0,      8);
	link6bed4tobottom (&ip4udp, (struct in6_addr *) (icmp6data + optidx));
	optidx += 16;
					// Set IPv6 prefix
	return optidx;
}


/*
 * Send a /114 Router Advertisement to a peer trying to contact us.
 */
void advertise_route (Socket6bed4 sox, struct sockaddr_in *v4peer, struct in6_addr *src6, struct pns_t *pns) {
	struct {
		struct ip6icmp6 ii;
		uint8_t _headroom [96];
	} raw6;
	icmp6type (&raw6.ii) = ND_ROUTER_ADVERT;
	icmp6code (&raw6.ii) = 0;
	uint8_t *data = icmp6data (&raw6.ii);
	data [0] = 0;		// Cur Hop Limit: unspec
	data [1] = 0x18;		// M=0, O=0
						// H=0, Prf=11=Low
						// Reserved=0
	//TODO// wire says 0x44 for router_adv.flags
	memset (data + 2, 0xff, 2+4+4);
					// Router Lifetime: max, 18.2h
					// Reachable Time: max
					// Retrans Timer: max
	size_t writepos = 2 + 2+4+4;
	//TODO// pns->prefix is what we use towards the peer, should we use dst6?
	writepos = icmp6_prefix (data, writepos, false,
				&pns->prefix, pns->pns_ip4udp);
	icmp6_reply (sox, v4peer, src6, &raw6.ii, writepos);
}


/*
 * Send a Probe message.  This is an UDP/IPv4 message with no contents.
 * The remote peer will not respond, but that is okay; outgoing traffic is the
 * way to keep holes in NAT and firewalls open.
 */
static void _probe (Socket6bed4 sox, lladdr6bed4 *target) {
	struct sockaddr_in v4peer;
	memset (&v4peer, 0, sizeof (v4peer));
	v4peer.sin_family = AF_INET;
	v4peer.sin_addr.s_addr = target->ll6bed4_ip4;
	v4peer.sin_port        = target->ll6bed4_udp;
	sendto (sox->udpsox, "", 0, MSG_DONTWAIT,
			(struct sockaddr *) &v4peer, sizeof (v4peer));
}


/*
 * Send a KeepAlive message.  This is an UDP/IPv4 message with no contents.
 * The router will not respond, but that is okay; outgoing traffic is the
 * way to keep holes in NAT and firewalls open.
 */
static void _keepalive (Socket6bed4 sox, lladdr6bed4 *target) {
	struct sockaddr_in v4peer;
	memset (&v4peer, 0, sizeof (v4peer));
	v4peer.sin_family = AF_INET;
	v4peer.sin_addr.s_addr = target->ll6bed4_ip4;
	v4peer.sin_port        = target->ll6bed4_udp;
	int done = 0;
	int secs = 1;
	setsockopt (sox->udpsox, IPPROTO_IP, IP_TTL, &sox->keepalive_hoplimit, 1);
	sendto (sox->udpsox, "", 0, MSG_DONTWAIT,
			(struct sockaddr *) &v4peer, sizeof (v4peer));
	setsockopt (sox->udpsox, IPPROTO_IP, IP_TTL, &sox->normal_hoplimit,    1);
}



/*
 *
 * SOCKET MANAGEMENT AND EVENT LOOP SUPPORT
 *
 */



/*
 * Allocate a new Socket6bed4 context.  This is a raw IPv6 socket to
 * which many local IPv6 addresses can be bound, along with many routes
 * to different 6bed4peer and 6bed4router remotes.  At the time of
 * creation, no 6bed4router is assigned; for each prefix to which reliable
 * peering is desired, you should add its 6bed4router.  You may supply a
 * local IPv4 address and/or UDP port to bind to.
 *
 * On successful return, the socket is stored in soxvar and 0 is returned.
 * On failure, -1 is returned and errno is set.
 */
int socket6bed4 (Socket6bed4 *soxvar, const struct sockaddr_in *opt_bindaddr) {
	int udpsox = -1;
	//
	// Check arguments
	assert (soxvar != NULL);
	//
	// Obtain a socket (and apply the optional binding)
	udpsox = socket (AF_INET, SOCK_DGRAM, 0);
	if (udpsox == -1) {
		goto fail;
	}
	if (opt_bindaddr != NULL) {
		if (bind (udpsox, (struct sockaddr *) opt_bindaddr, sizeof (*opt_bindaddr)) == -1) {
			goto fail;
		}
	}
	//
	// Allocate the socket6bed4 data structure
	Socket6bed4 sox = calloc (1, sizeof (struct socket6bed4));
	if (sox == NULL) {
		goto fail;
	}
	//
	// Fill the socket6bed4 data structure
	sox->udpsox = udpsox;
	sox->next_timeout = _int32time (sox, true, 60000);
	sox->keepalive_interval = 29000;
	sox->keepalive_hoplimit = 3;
	sox->normal_hoplimit = 64;
	//
	// Return the abstract socket6bed4 structure
	*soxvar = sox;
	return 0;
	//
	// Cleanup and report failure
fail:
	if (sox != NULL) {
		free (sox);
	}
	if (udpsox >= 0) {
		close (udpsox);
	}
	return -1;
}


/*
 * Close the Socket6bed4 and related resources.
 */
void close6bed4 (Socket6bed4 sox) {
	close (sox->udpsox);
	free (sox);
}


/*
 * Obtain the file/socket descriptor int to which an event loop can
 * subscribe with select() and variants to learn about work to do.
 */
int listenfd6bed4 (Socket6bed4 sox) {
	return sox->udpsox;
}


/* Find all the expired elements in a given queue and 
 * either recycle them with minor action, or take them
 * out for a major action.  Return the new timeout as
 * it appears to the queues.
 */
static void _alarm6bed4_pnsqueue (Socket6bed4 sox, struct pns_t **qhd) {
	struct pns_t *this, *tmp;
	int32_t now = sox->now32time;
	DL_FOREACH_SAFE2 (*qhd, this, tmp, pns_next) {
		// Is this the first list entry to expire in the future?
		if ((this->pns_state_until - now) > 0) {
			if ((this->pns_state_until - sox->next_timeout) < 0) {
				sox->next_timeout = this->pns_state_until;
			}
			break;
		}
		// Test whether we are making a minor or major transition
		bool major = PNS_COUNTER_DONE (this->pns_state);
		// pns_dequeue() -- in a loop-safe variant
		DL_DELETE2 (*qhd, this, pns_prev, pns_next);
		// The peer NAT state expired and was taken off its
		// timer queue; we now decide if/how to proceed.
		bool cont = true;
		bool qeep = false;
		switch (this->pns_state) {
			// Normally, we keep the peer NAT state and add
			// it to a timer.  But before doing that, we
			// can stop that by resetting "cont", and we can
			// do something other than simply incrementing
			// the state, to cause other state transitions.
		case PNS_INIT1:
		case PNS_POLL1:
			// Continue as FAIL (but do send Probe below)
			this->pns_state = PNS_FAIL0;
			break;
		case PNS_PEER:
			// Continue into POLL (and send Probe below)
			this->pns_state = PNS_POLL0;
			break;
		case PNS_ROUTE:
			// Remain in the PNS_ROUTE state
			qeep = true;
			break;
		case PNS_FAIL1:
			// Continue in FAIL1 if we didn't send for ~45s but
			// drop the peer NAT state after longer inactivity.
			// (Inaccurate timing, it's just "pretty darn long".)
			// As long as we're sending, we'll stay in FAIL1
			// and send occasional Probes, just to allow us to
			// bootstrap direct peering somewhat quickly.
			cont = (this->pns_last_send + 45000 - now) > 0;
			qeep = (!cont) && (this->bindings > 0);
			break;
		default:
			// Minor changes, INIT0 -> INIT1 or POLL0 -> POLL1
			this->pns_state++;
		}
		// Requeue with a timer, or erase the peer from memory
		if (cont || qeep) {
			pns_enqueue (sox, this);
			if (qeep) {
				// Only send a Keepalive, to outside local NAT
				_keepalive (sox, &this->pns_ip4udp);
			} else {
				// Send a Probe, all the way to the remote
				_probe     (sox, &this->pns_ip4udp);
			}
		} else {
			pns_delpeer_bypns (sox, this);
		}
	}
}


/*
 * Handle timer expiration.  This is usually called by an external
 * event loop shortly after the nextalarm6bed4() has passed.  This
 * allows timing to a millisecond scale.
 *
 * The maintenance done here is for timers of:
 *  - Keepalives towards 6bed4routers to which we have bound addresses
 *  - Probes within the Peer NAT State timing and state diagram
 *  - ???
 *
 * Use the nextalarm6bed4() call after this one finishes and perhaps
 * other work (on 6bed4 or other elements) has completed.  This
 * routine does not return the next time to allow maximum accuracy;
 * it does prepare absolute wakeup times to allow fast completion
 * of nextarlarm6bed4() to not interfere with accuracy of other
 * tasks in the external event loop.
 */
void alarm6bed4 (Socket6bed4 sox) {
	//
	// Sample the current time and set an unrealistically high
	// next_timeout to subsequently reduce by actual timeouts.
	sox->next_timeout = _int32time (sox, true, 60000);
	//
	// Run over the queues for 01s and 25s timeouts
	_alarm6bed4_pnsqueue (sox, &sox->pns_q01s);
	_alarm6bed4_pnsqueue (sox, &sox->pns_q25s);
	//
	// Run over the Keepalive queue
	_alarm6bed4_pnsqueue (sox, &sox->pns_qeep);
}


/*
 * Return the first timer expiration moment.  This is useful
 * for an external event loop as a wakeup moment.  The time
 * returned is a number of milliseconds relative to the current
 * time, and it is at least 0.
 */
int32_t nextalarm6bed4 (Socket6bed4 sox) {
	int32_t timeout = _int32time (sox, true, 0) - sox->next_timeout;
	if (timeout < 0) {
		timeout = 0;
	}
	return timeout;
}


/*
 * Setup the callback function if it is not NULL, and attempt to have it called.
 * If the callback function is NULL, it will simply be removed.  Construct a new
 * router entry in the Peer NAT State tables if none exists yet.
 *
 * An attempt to have the callback function called will initially be done by
 * sending a Router Solicitation.  If a Router Advertisement was received before,
 * the callback will be invoked immediately with the information at hand.
 *
 * The function returns -1/errno on failure.  A return value 0 indicates a
 * direct/trivial success including callback if appropriate, whereas 1 indicates
 * that a Router Solicitation was actually sent and the application may want
 * to wait until a timeout for the callback to be triggered (or else resend).
 */
int solicit6bed4route (Socket6bed4 sox,
				advertise6bed4route_cb cbfun, void *cbdata,
				lladdr6bed4 *lladdr) {
	//
	// The fixed content of a Router Solicitation message
	static const uint8_t ipv6_router_solicitation [] = {
		// IPv6 header
		0x60, 0x00, 0x00, 0x00,
		16 >> 8, 16 & 0xff, IPPROTO_ICMPV6, 255,
		0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,		 // unspecd src
		0xff, 0x02, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0x02, // all-rtr tgt
		// ICMPv6 header: router solicitation
		ND_ROUTER_SOLICIT, 0, 0x7a, 0xae,	// Checksum courtesy of WireShark :)
		// ICMPv6 body: reserved
		0, 0, 0, 0,
		// ICMPv6 option: source link layer address 0x0001 (end-aligned)
		0x01, 0x01, 0, 0, 0, 0, 0x00, 0x01,
	};
	//
	// Find the Peer NAT State, or add it freshly
	struct pns_t *pns = pns_findpeer (sox, lladdr);
	if (pns == NULL) {
		pns = pns_addpeer (sox, lladdr, PNS_RSOL0);
		if (pns == NULL) {
			return -1;
		}
		memset (&pns->prefix, 0, 16);
		pns->prefix.s6_addr [0] = 0xfe;
		pns->prefix.s6_addr [1] = 0x80;
		memcpy (&pns->prefix.s6_addr [8], &lladdr->ll6bed4_ip4, 4);
		pns->prefix.s6_addr [14] = pns->prefix.s6_addr [12] << 6;
		pns->prefix.s6_addr [12] &= 0xfc;
		pns->prefix.s6_addr16 [6] = lladdr->ll6bed4_udp;
	}
	//
	// Update the callback function registration
	pns->rtadv_cbfun  = cbfun;
	pns->rtadv_cbdata = cbdata;
	if (cbfun == NULL) {
		return 0;
	}
	//
	// If the prefix is known, callback immediately
	if (pns->prefix.s6_addr [0] != 0x00) {
		cbfun (sox, cbdata, lladdr, NULL, &pns->prefix, -1, NULL, NULL);
		return 0;
	}
	//
	// Fire off a Router Solicitation (application may set a timeout)
	struct sockaddr_in v4peer;
	memset (&v4peer, 0, sizeof (v4peer));
	v4peer.sin_family      = AF_INET;
	v4peer.sin_addr.s_addr = lladdr->ll6bed4_ip4;
	v4peer.sin_port        = lladdr->ll6bed4_udp;
	sendto (sox->udpsox,
			ipv6_router_solicitation, sizeof (ipv6_router_solicitation),
			MSG_DONTWAIT,
			(struct sockaddr *) &v4peer, sizeof (v4peer));
	//
	// The callback will hopefully be called from recv6bed4()
	return 1;
}



/*
 *
 * CONNECTION TO 6BED4ROUTER AND 6BED4PEER, ADDRESS BINDING
 *
 */



/*
 * NOT_TODO: IMPROVED NATIVE ROUTING
 *
 * There is no concept of a default router in 6bed4; instead, the sender
 * address can be used to determine the outgoing route.  Native prefixes
 * require a lookup table to find the definint 6bed4router.  Whether an
 * offered route is used, can be an application concern, and comes with
 * the concerns for binding to an address.
 *
 * //NOT_TODO// int default6bed4router (uint8_t *lladdr_6bed4router)
 */


/*
 * Set or reset the keepalive option on a remote party.  This is normally
 * done automatically for every 6bed4router and no 6bed4peer.  This call
 * allows the application to override that default, perhaps for continued
 * access to a 6bed4peer in spite on long-laste connections, or to allow
 * garbage collection of a 6bed4router that is no longer used.
 *
 * Note that Keepalives differ from Probes; a Probe is sent all the way
 * through to the remote, whereas a Keepalive only needs to travel far
 * enough to cross all local NAT routers.  While a peer is in active
 * use, it will send Probes instead of Keepalives.  Instead of being
 * cleaned up at the end of its useful life however, it will continue to
 * send Keepalives instead of being removed.
 *
 * The call returns 0 on success or -1/errno on failure.
 */
#if DIRECT_KEEPALIVE_INSTEAD_OF_ADDRESS_BINDING
int keepalive6bed4route (Socket6bed4 sox, lladdr6bed4 *lladdr, bool keepalive) {
	struct pns_t *pns = pns_findpeer (sox, lladdr);
	if (pns == NULL) {
		errno = ENETDOWN;
		return -1;
	}
	pns->keepalive = keepalive;
}
#endif


/*
 * Bind an address for a given remote address, based on the prefix given by
 * the 6bed4router or, in case direct peering is flagged, by the 6bed4peer.
 * Use the indicated lanid, which must be in the range 1..16383 to fit.
 * You must have received an advertise6bed4() callback for the remote
 * before you can use this function.
 *
 * Address binding replaces the bind() call on a normal socket, and may be
 * used many times to obtain many addresses.  The addresses are based on
 * Router Solicitation, even for direct peering, and Keepalives will be
 * turned on.
 *
 * Direct peering is not a reliable technique; it is a desparate one.
 * When your NAT is symmetric, it would assign different external ports
 * for different remotes, so a 6bed4peer and its 6bed4router would end
 * up having different IPv6 addresses and you could not switch from the
 * 6bed4router to direct routing to the 6bed4peer.  It is not clear if
 * and how you would bootstrap your connection to the 6bed4peer in this
 * case, and you would need to do more work in the application to know
 * that various 6bed4peers in fact belong to a 6bed4router.  Only if
 * you wish to completely avoid going through the 6bed4router and are
 * willing to sacrifice reliability or expend a lot of work for it is
 * direct peering really an option -- and even then, a confusing one.
 *
 * The Keepalives turn a bound address into a resource, so you should
 * use unbind6bed4() to free the resources for Keepalive sending.
 */
int bind6bed4 (Socket6bed4 sox, const struct in6_addr *remote, bool direct, uint16_t lanid, struct in6_addr *local) {
	if ((lanid == 0) || (lanid >= 16384)) {
		errno = EINVAL;
		return -1;
	}
	lladdr6bed4 ll;
	if (linkaddr6bed4 (remote, !direct, &ll) != 0) {
		return -1;
	}
	struct pns_t *pns = pns_findpeer (sox, &ll);
	if (pns == NULL) {
		errno = ENETDOWN;
		return -1;
	}
	if (pns->prefix.s6_addr [0] == 0x00) {
		errno = ENOTCONN;
		return -1;
	}
	memcpy (local, remote, 16);
	local->s6_addr [14] &= 0xc0;
	local->s6_addr [14] |= lanid >> 8;
	local->s6_addr [15]  = lanid & 0xff;
	if (pns->bindings = 0xffffffff) {
		errno = ERANGE;
		return -1;
	}
	pns->bindings++;
	return 0;
}
//
int unbind6bed4 (Socket6bed4 sox, const struct in6_addr *local) {
	lladdr6bed4 ll;
	bottom6bed4tolink (local, &ll);
	struct pns_t *pns = pns_findpeer (sox, &ll);
	if (pns == NULL) {
		errno = ENETDOWN;
		return -1;
	}
	if (pns->bindings == 0) {
		errno = ERANGE;
		return -1;
	}
	pns->bindings--;
	return 0;
}



/*
 *
 * SENDING TO THE 6BED4 NETWORK, OR 6TO4
 *
 */



/* Process an outgoing Traffic Class, and set its Seen flag.
 * Also take note of the fact that we did send a message.
 */
static inline void pns_6to4_tc_seen (Socket6bed4 sox, struct pns_t *pns, uint8_t *hdr6) {
	//
	// Reset the last outgoing traffic timer
	int32_t now = _int32time (sox, false, 0);
	pns->pns_last_send = now;
	//
	// Check if the Traffic Class matches our style
	if (((hdr6 [1] & 0xc0) != 0x00) || ((hdr6 [0] & 0x01) != 0x00)) {
		return;
	}
	//
	// Set or reset the Seen flag
	if ((pns->pns_seen_until - now) > 0) {
		hdr6 [0] |= 0x02;
	} else {
		hdr6 [0] &= 0xfd;
	}
}


/* Choose whether to send as direct peering or not.  Mostly, the
 * output is either to send through the 6bed4router or directly;
 * only for one case can it result in dropping or ICMPv6 errors.
 *
 * The return value is one of:
 *   SEND_ROUTER  to send via the 6bed4router (see below);
 *   SEND_PEER    to send directly to the peer;
 *   SEND_OBLIVIA to drop the message.
 *   SEND_ERROR   to return an ICMPv6 error;
 *
 * The intention of SEND_ROUTER is to address not our own but the
 * peer's 6bed4router, which the peer keeps open through regular
 * keepalive messages.  They may be the same, of course.  But it
 * is only possible to determine the peer's 6bed4router when it
 * has a prefix TBD1::/32 or fc64::/16 which is known to contain
 * the 6bed4router IPv4 address in the last 32 bits of its /64.
 * In all other cases, 6bed4 routing is done through the IPv6
 * native route (which may or may not include internal addresses
 * that may or may not be networked to a larger network than the
 * 6bed4router servicing this 6bed4peer).
 *
 * The "foreign" flag is set to true on non-6bed4 Traffic Classes.
 */
#define SEND_ROUTER  ((uint8_t) 0)
#define SEND_PEER    ((uint8_t) 1)
#define SEND_OBLIVIA ((uint8_t) 2)
#define SEND_ERROR   ((uint8_t) 3)
//
static uint8_t pns_6to4_where (Socket6bed4 sox, struct pns_t *pns, uint8_t traffic_class, bool *foreign) {
	// Ignore the "Seen" flag; For foreign Traffic Classes, use PEERPOL_6BED4_PROPER
	uint8_t peerpol;
	*foreign = (traffic_class & 0x1c) != 0x00;
	if (*foreign) {
		peerpol = PEERPOL_6BED4_PROPER;
	} else {
		peerpol = traffic_class & 0xc0;
	}
	// Pickup peer NAT state, or default to INIT0
	uint8_t peerstate;
	if (pns == NULL) {
		peerstate = PNS_INIT0;
	} else {
		peerstate = pns->pns_state;
	}
	// Switch to behaviour that depends on the peering policy
	switch (peerpol) {
	case PEERPOL_6BED4_PROPER:
		// Route FAIL and INIT traffic through the 6bed4router
		return (peerstate & PNS_DIRECT) ? SEND_PEER : SEND_ROUTER;
	case PEERPOL_6BED4_PROHIBITED:
		// Route all traffic through the 6bed4router
		return SEND_ROUTER;
	case PEERPOL_6BED4_PRESUMPTIOUS:
		// Route only FAIL traffic through the 6bed4router
		return (peerstate & PNS_FAILED) ? SEND_ROUTER : SEND_PEER;
	case PEERPOL_6BED4_PERSISTENT:
		// No traffic through the 6bed4router, FAIL is ICMP
		if (peerstate & PNS_FAILED) {
			return SEND_ERROR;
		}
		return SEND_PEER;
	}
}


/*
 * Relay an IPv6 frame to 6bed4, deriving the link-local address from the
 * source address or remote peer.  Trust the IPv6 application to have
 * properly bound its source and destination address using 6bed4 methods.
 *
 * Return as send6bed4() does: size sent or -1/errno.
 */
static int _send6bed4_plain_unicast (Socket6bed4 sox, struct ip6_hdr *raw6, size_t len, int flags) {
	struct sockaddr_in v4dest;
	struct lladdr6bed4 ip4udp;
	struct pns_t *pns;
	bool dst6bed4 = is_6bed4 (&raw6->ip6_dst);
	bool newpeer = false;
	if (dst6bed4) {
		bottom6bed4tolink (&raw6->ip6_dst, &ip4udp);
		pns = pns_findpeer (sox, &ip4udp);
		newpeer = (pns == NULL);
		if (newpeer) {
			pns = pns_addpeer (sox, &ip4udp, PNS_INIT0);
		}
	} else {
		pns = NULL;
	}
	bool foreign;
	uint8_t trfcls = get_trafficclass (raw6);
	memset (&v4dest, 0, sizeof (v4dest));
	v4dest.sin_family = AF_INET;
	uint8_t whereto = pns_6to4_where (sox, pns, trfcls, &foreign);
	pns_6to4_tc_seen (sox, pns, (uint8_t *) raw6);
	switch (whereto) {
	case SEND_PEER:
		if (!dst6bed4) {
			/* No 6bed4 -- no ip4udp -- cannot route */
			errno = ENOTCONN;
			return -1;
		}
		v4dest.sin_addr.s_addr = ip4udp.ll6bed4_ip4;
		v4dest.sin_port        = ip4udp.ll6bed4_udp;
		/* Just updated pns_last_send, copy to pns_last_direct */
		pns->pns_last_direct = pns->pns_last_send;
		break;
	case SEND_ROUTER:
		/* We always route to _our_ 6bed4router, as found in the
		   ip6_src address.  We do not try to avoid trapezium routing
		   other than hoping that direct peering will arrive soon.
		   The applications might have bound to the target prefix.
		 */
		if (!is_6bed4 (&raw6->ip6_src)) {
			/* TODO: Can we do better for native /64 sources? */
			errno = ENOTCONN;
			return -1;
		}
		v4dest.sin_addr.s_addr = raw6->ip6_src.s6_addr32 [1];
		v4dest.sin_port        = netport6bed4;
		/* Every 6bed4 address has a lower half; send a Probe */
		if (newpeer && dst6bed4) {
			_probe (sox, &ip4udp);
		}
		break;
	case SEND_OBLIVIA:
		/* Withdraw according to plan */
		return 0;
	case SEND_ERROR:
		/* We are running into real problems delivering text */
		errno = ENOTCONN;
		return -1;
	}
	trfcls = get_trafficclass (raw6);
	if (trfcls != sox->last_trfcls_sent) {
		sox->last_trfcls_sent = trfcls;
		if (setsockopt (sox->udpsox, IPPROTO_IP, IP_TOS, &trfcls, 1) == -1) {
			syslog (LOG_ERR, "Failed to switch IPv4 socket to Traffic Class 0x%02x\n", trfcls);
		}
	}
	return sendto (sox->udpsox, raw6, len, flags,
			(struct sockaddr *) &v4dest, sizeof (struct sockaddr_in));
}



/*
 * Send a raw IPv6 packet out through the Socket6bed4 network.
 * The interface is similar to send() and returns the number of bytes
 * sent (starting from the IPv6 header) or -1 with errno on error.
 *
 * Specifically worth noting are these errors:
 *  - ENOTCONN is reported when a route to the remote does not exist;
 *  - ECONNRESET may be reported when NAT state is no longer valid.
 *
 * Among the flags, you probably want to use MSG_DONTWAIT, to stave
 * off synchronous blocking on the UDP/IPv4 socket.
 *
 * Note: IPv6 headers may be modified.  This breaks with tradition,
 *       but saves buffer copies just-to-make-sure.
 */
int send6bed4 (Socket6bed4 sox, struct ip6_hdr *raw6, size_t len, int flags) {
	if (len < sizeof (struct ip6_hdr) + 1) {
		return EINVAL;
	}
#ifdef NEED_MORE_THAN_PLAIN_UNICAST
	/*
	 * Distinguish types of traffic:
	 * Non-plain, Plain Unicast, Plain Multicast
	 */
	if ((raw6->v6type == IPPROTO_ICMPV6) &&
			(raw6->v6icmp6type >= 133) && (raw6->v6icmp6type <= 137))
	{
		//
		// Not Plain: Router Adv/Sol, Neighbor Adv/Sol, Redirect
		return _send6bed4_nd (sox, raw6, len, flags);
	} else if ((raw6->ip6_dst->s6_addr [0] != 0xff) && !(raw6->ip6_dst->s6_addr [8] & 0x01)) {
		//
		// Plain Unicast
		if (raw6->v6hops-- <= 1) {
			errno = EINVAL;
			return -1;
		}
		return _send6bed4_plain_unicast (sox, raw6, len, flags);
	} else {
		//
		// Plain Multicast
		//TODO:IGNORE_MULTICAST//
		//TODO// syslog (LOG_DEBUG, "%s: Plain multicast from 6bed4 Link to 6bed4 Network is not supported\n", program);
	}
#endif
	//
	// Lower the hop count and fail if it is too low
	if (raw6->ip6_hlim-- <= 1) {
		errno = EINVAL;
		return -1;
	}
	//
	// Actually send the message as plain unicast
	return _send6bed4_plain_unicast (sox, raw6, len, flags);
}



/*
 *
 * RECEIVING FROM THE 6BED4 NETWORK, OR 4TO6
 *
 */



/* Handle the IPv4 message pointed at by msg as a neighbouring command.
 *
 * Type	Code	ICMPv6 meaning			Handling
 * ----	----	-----------------------------	----------------------------
 * 133	0	Router Solicitation		Ignore
 * 134	0	Router Advertisement		Setup Tunnel with Prefix
 * 135	0	Neighbour Solicitation		Send Neighbour Advertisement
 * 136	0	Neighbour Advertisement		Ignore
 * 137	0	Redirect			Ignore
 */
static inline int _recv6bed4_nd (Socket6bed4 sox,
					struct pns_t *pns,
					struct sockaddr_in *v4peer,
					struct ip6icmp6 *frame,
					int framelen) {
	uint16_t srclinklayer;
	uint8_t *destprefix = NULL;
	struct ndqueue *ndq;
	if (framelen < sizeof (struct ip6_hdr) + sizeof (struct icmp6_hdr)) {
		return 0;
	}
	//
	if (icmp6code (frame) != 0) {
		return 0;
	}
	if (icmp6_checksum ((uint8_t *) frame, framelen)
				!= icmp6csum (frame)) {
		return 0;
	}
	//
	// Approved.  Perform neighbourly courtesy.
	struct in6_addr *src6 = & frame->h.ip6_src;
	struct in6_addr *dst6 = & frame->h.ip6_dst;
	uint16_t plen = ntohs (frame->h.ip6_plen);
	uint8_t *icmpdata = frame->i.icmp6_data8;
	switch (icmp6type (frame)) {
	case ND_NEIGHBOR_SOLICIT:
		//
		// Drop Neigbour Solicitation
		return 0;
	case ND_NEIGHBOR_ADVERT:
		//
		// Drop Neigbor Advertisement
		return 0;
	case ND_REDIRECT:
		//
		// Redirect indicates that a more efficient bypass exists than
		// the currently used route.  The remote peer has established
		// this and wants to share that information to retain a
		// symmetric communication, which is helpful in keeping holes
		// in NAT and firewalls open.
		//
		// BE EXTREMELY SELECTIVE BEFORE ACCEPTING REDIRECT!!!
		return 0;
	case ND_ROUTER_SOLICIT:
		//
		// Peers respond to Router Solicitation with Router Advertisement
		// holding the accepted prefix and, most importantly, the UDP/IPv4
		// of the neighbour as observed in the receiving (out)side of the
		// peer's NAT.
		//
		advertise_route (sox, v4peer, src6, pns);
		//
		// Router Solicitation handled internally, continue recv6bed4() looping
		return 0;		/* this is not a router, drop */
	case ND_ROUTER_ADVERT:
		//
		// Validate Router Advertisement
		if (plen < sizeof (struct icmp6_hdr) + 16) {
			return 0;   /* strange length, drop */
		}
		if (icmpdata [1] & 0x80 != 0x00) {
			return 0;   /* indecent proposal to use DHCPv6, drop */
		}
		if ((pns->pns_state & PNS_ROUTER) == 0x00) {
			return 0;	/* not from router, drop */
		}
		//
		// Parse the Router Advertisement
		size_t rdofs = 12;
		int v6route_count_max = (plen - 16) / 32;
		struct in6_addr v6route_addr [v6route_count_max];
		uint8_t         v6route_pfix [v6route_count_max];
		int v6route_count = 0;
		bool got_prefix = false;
		struct in6_addr old_prefix;
		memcpy (&old_prefix, &pns->prefix, 16);
		while (rdofs + 4 < plen) {
			if (icmpdata [rdofs + 1] == 0) {
				return 0;   /* zero length option */
			}
			if (icmpdata [rdofs + 0] != ND_OPT_PREFIX_INFORMATION) {
				/* skip to next option */
			} else if (icmpdata [rdofs + 1] != 4) {
				return 0;   /* bad length field */
			} else if (rdofs + (icmpdata [rdofs + 1] << 3) > ntohs (plen) + 4) {
				return 0;   /* out of packet length */
			} else {
				//
				// Handle on-link autoconfiguration addresses
				if ((icmpdata [rdofs+3] & 0xc0) == 0xc0) {
					if (got_prefix) {
						// We already got a prefix, looks bad
						return 0;
					}
					got_prefix = true;
					if (icmpdata [rdofs+2] != 114) {
						// This does not look like 6bed4, error
						return 0;
					}
					memcpy (&pns->prefix, &icmpdata [rdofs+16], 15);
					pns->prefix.s6_addr [14] &= 0xc0;
					pns->prefix.s6_addr [15]  = 0x00;
				}
				//
				// Collect the routing information
				assert (v6route_count < v6route_count_max);
				memcpy (v6route_addr [v6route_count].s6_addr,
						&icmpdata [rdofs+16], 16);
				v6route_pfix [v6route_count] = icmpdata [rdofs+2];
				v6route_count++;
			}
			rdofs += (icmpdata [rdofs + 1] << 3);
		}
		if (!got_prefix) {
			// Not a useful Router Advertisement
			return 0;
		}
		//
		// Invoke the callback to report the new prefix and routes
		if (pns->rtadv_cbfun != NULL) {
			struct in6_addr *opt_old_prefix;
			if (old_prefix.s6_addr [0] == 0x00) {
				opt_old_prefix = NULL;
			} else {
				opt_old_prefix = &old_prefix;
			}
			pns->rtadv_cbfun (sox,
					pns->rtadv_cbdata,
					&pns->pns_ip4udp,
					opt_old_prefix, &pns->prefix,
					v6route_count, v6route_addr, v6route_pfix);
		}
		//
		// Switch to the Keepalive queue (for a new Router Advertisement)
		if (pns->pns_state != PNS_ROUTE) {
			_keepalive (sox, &pns->pns_ip4udp);
			pns_dequeue (sox, pns);
			pns->pns_state = PNS_ROUTE;
			pns_enqueue (sox, pns);
		}
		//
		// Router Advertisement handled internally, continue recv6bed4() looping
		return 0;
	}
	return 0;
}


/* Process an incoming Traffic Class into a peer NAT state.
 */
static inline void _recv6bed4_trfcls (Socket6bed4 sox, struct pns_t *pns, struct ip6_hdr *hdr6) {
	//
	// Retrieve the traffic class from the received IPv6 header
	uint8_t trfcls = get_trafficclass (hdr6);
	//
	// Check if the Traffic Class matches our style and flags "Seen"
	if ((trfcls & 0x3c) != 0x20) {
		return;
	}
	//
	// Only proceed for existing peer NAT state
	if (pns == NULL) {
		lladdr6bed4 ip4udp;
		bottom6bed4tolink (&hdr6->ip6_src, &ip4udp);
		pns = pns_findpeer (sox, &ip4udp);
		if (pns == NULL) {
			return;
		}
	}
	//
	// A valid byte with a "Seen" flag, reset to 25s of direct peering
	pns_dequeue (sox, pns);
	pns->pns_state = PNS_PEER;
	pns->pns_state_until = _int32time (sox, false, 25000);
	pns_enqueue (sox, pns);
}


/*
 * Receive a raw IPv6 packet that comes in over the Socket6bed4 network.
 * The interface is similar to recv() and returns the number of bytes
 * sent (starting from the IPv6 header) or -1 with errno on error.
 *
 * Specifically worth noting are these errors:
 *  - ENOTCONN is reported when a route to the remote does not exist;
 *  - ECONNRESET may be reported when NAT state is no longer valid.
 *
 * Among the flags, you probably want to use MSG_DONTWAIT, to stave
 * off synchronous blocking on the UDP/IPv4 socket.  The routine will
 * loop if multiple messages may be received, but the loop is broken
 * when either an actual message arrives or an error condition occurs
 * that should be reported to the user (including EAGAIN/EWOULDBLOCK).
 */
int recv6bed4 (Socket6bed4 sox, struct ip6_hdr *raw6, size_t maxlen, int flags) {
	//
	// Receive IPv4 package, which may be tunneled or a tunnel request
	struct sockaddr_in v4peer;
	socklen_t adrlen;
again:
	adrlen = sizeof (v4peer);
	ssize_t buflen = recvfrom (sox->udpsox,
				raw6, maxlen,
				flags,
				(struct sockaddr *) &v4peer, &adrlen);
	assert (adrlen == sizeof (v4peer));
	assert (v4peer.sin_family == AF_INET);
	if (buflen == -1) {
		return -1;
	}
	if (buflen < sizeof (struct ip6_hdr)) {
		goto again;	/* Too short, simply ignore */
	}
	//
	// This may be a Probe or full packet that made it in over direct peering
	lladdr6bed4 ip4udp;
	ip4udp.ll6bed4_ip4 = v4peer.sin_addr.s_addr;
	ip4udp.ll6bed4_udp = v4peer.sin_port;
	struct pns_t *pns = pns_findpeer (sox, &ip4udp);
	if ((pns != NULL) && ((pns->pns_state & PNS_ROUTER) == 0x00)) {
		//TODO//  - test for stdport, then address against top of IPv6
		// Raise the reverse Seen flag after direct traffic
		int32_t seenperiod = 1000 * (sox->keepalive_interval - 28);
		pns->pns_seen_until = pns->pns_last_direct + seenperiod;
	}
	//
	// Probe having been noticed, fend off poorly formed packets
	if (buflen <= sizeof (struct ip6_hdr)) {
		goto again;	/* Too little data, simply ignore */
	}
	if ((raw6->ip6_vfc & 0xf0) != 0x60) {
		goto again;	/* Not an IPv6 packet, simply ignore */
	}
	if (!validate_originator (sox, pns, &v4peer, &raw6->ip6_src)) {
		// source appears fake, but it may be a trapezium bypass
		if (!validate_trapezium_bypass (sox, &v4peer, &raw6->ip6_src, &raw6->ip6_dst)) {
			goto again;	/* Invalid both ways, so ignore */
		}
	}
	//
	// Process any incoming "Seen" flag from already-known peers
	_recv6bed4_trfcls (sox, pns, raw6);
	//
	// Distinguish types of traffic:
	// Non-plain, Plain Unicast, Plain Multicast
	int retval;
	if ((raw6->ip6_nxt == IPPROTO_ICMPV6) &&
			(icmp6type (raw6) >= 133) && (icmp6type (raw6) <= 137)) {
		//
		// Not Plain: Router Adv/Sol, Neighbor Adv/Sol, Redirect
		if (raw6->ip6_hlim != 255) {
			goto again;	/* Not direct, simply ignore */
		}
		retval = _recv6bed4_nd (sox, pns, &v4peer, (struct ip6icmp6 *) raw6, buflen);
	} else {
		//
		// Plain Unicast or Plain Multicast (both may enter)
		if (raw6->ip6_hlim-- <= 1) {
			goto again;	/* Hop limit reached, simply ignore */
		}
		//
		// Studies in Anticlimax?  We have already done it all :-)
		retval = buflen;
	}
	//
	// If properly ended but without data, loop for more
	if (retval == 0) {
		goto again;
	}
	//
	// Finally return the result, be it a size or -1/errno
	return retval;
}

