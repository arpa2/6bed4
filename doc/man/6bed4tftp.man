.TH 6BED4TFTP 1 "June 23, 2020"
.\" Please adjust this date whenever revising the manpage.
.\"
.\" Some roff macros, for reference:
.\" .nh        disable hyphenation
.\" .hy        enable hyphenation
.\" .ad l      left justify
.\" .ad b      justify to both left and right margins
.\" .nf        disable filling
.\" .fi        enable filling
.\" .br        insert line break
.\" .sp <n>    insert n+1 empty lines
.\" for manpage-specific macros, see man(7)
.SH NAME
6bed4send, 6bed4recv \- send, receive a file using TFTP over 6bed4
.SH SYNOPSYS
.B 6bed4send
[\fIoptions\fR] \fIfile\fR [\fIlocal_peer\fR] [\fIremote_peer\fR]
.PP
.B 6bed4recv
[\fIoptions\fR] \fIfile\fR [\fIlocal_peer\fR] [\fIremote_peer\fR]
.SH DESCRIPTION
.PP
The \fB6bed4send\fR and \fB6bed4recv\fR programs are mirrorred, one being used
for sending and the other for receiving a \fIfile\fR.  When the \fIfile\fR is
provided as a single dash, it will be replaced by an infinite stream of zero
bytes and a waste bin on reception; furthermore, the dash with slow down the
transmission pace to one data block per 750 milliseconds.
.PP
The protocol for file transmission is the Trivial File Transfer Protocol (TFTP)
over IPv6, though not in client-server mode but instead run between equal peers.
Each of the programs can be passive or active, depending on the occurrence of
the \fIremote_peer\fR.  An extension to the TFTP message flow allows the
peers to work in a "simultaneous connect" style; provided that long one sends
a Read Request and the other sends a Write Request the two can continue as
though one of the peers had been passive.
.PP
The programs have option support and statistics for influencing the desired
Traffic Class in both directions.  These settings are used by the
\fB6bed4peer\fR to make routing decisions; this mostly concerns the choice
between the fallback route through the \fB6bed4router\fR and a direct
route to the \fB6bed4peer\fR on the other end.  Information on received
Traffic Class is produced as statistics, along with the number of packets
that had the "Seen" flag set as a sign that direct connections ought to
be possible.
.PP
.SH OPTIONS
.TP
\fB\-0\fR
.TP
\fB\-\-proper\-peering\fR
This is the default Traffic Class, and is also used when it is not setup
by any application.  It initiailly selects reliable peering through the
\fB6bed4router\fR but, after being formed about a reliable direct peering
option, it will switch to that mode.
.TP
\fB\-1\fR
.TP
\fB\-\-prohibited\-peering\fR
This sets up the Traffic Class as an instruction to pass all traffic
through the \fB6bed4router\fR and never through direct peering.  It is
the most conservative mode in terms of peering, but it also means that
traffic has a more constant delay than swapping between options.
.TP
\fB\-2\fR
.TP
\fB\-\-presumptious\-peering\fR
This tries harder to use direct peering, at the possible discomfort of
a two-second delay in actually getting connected.  During those first
two seconds, no traffic is passed to the \fB6bed4router\fR but, given
that the peer is unresponsive, the presumptious peering decision is
withdrawn and traffic may pass through the \fB6bed4router\fR.
.TP
\fB\-3\fR
.TP
\fB\-\-persistent\-peering\fR
This forbids any traffic routing through the \fB6bed4peer\fR and
instead persists in direct peering traffic.  When this attempt
fails for more than two seconds, the traffic will report errors
over ICMPv6.
.SH BUGS
The resend timing of \fB6bed4send\fR matches the slow-down delay that
\fB6bed4recv\fR uses for acknowledgement.  As a result, traffic patterns
with more resends than strictly required may occur.  This is harmless to
the execution of the protocol.
.SH LICENSE
Released under a BSD-style license without advertisement clause.
.SH SEE ALSO
The 0cpm project is an example of an IPv6-only SIP application
that can use \fB6bed4router\fR and comparable TSP tunnel services to
demonstrate the advantages of IPv6 to end users.  It is also
a typical example of a transitionary need for something like
\fB6bed4router\fR.
.PP
http://0cpm.org/ \- the homepage of the 0cpm project.
.PP
http://devel.0cpm.org/6bed4/ \- the homepage of \fB6bed4\fR.
.PP
.SH AUTHOR
\fB6bed4send\fR and \fB6bed4recv\fR were written by Rick van Rein from OpenFortress.
It was created to support the 0cpm project.
