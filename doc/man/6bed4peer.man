.TH 6BED4CLIENT 8 "Februari 1, 2011"
.\" Please adjust this date whenever revising the manpage.
.\"
.\" Some roff macros, for reference:
.\" .nh        disable hyphenation
.\" .hy        enable hyphenation
.\" .ad l      left justify
.\" .ad b      justify to both left and right margins
.\" .nf        disable filling
.\" .fi        enable filling
.\" .br        insert line break
.\" .sp <n>    insert n+1 empty lines
.\" for manpage-specific macros, see man(7)
.SH NAME
6bed4peer \- Tunnel client for IPv6 anywhere, with reliable peer-to-peer support
.SH SYNOPSYS
.B 6bed4peer
[\fB\-d\fR \fI/dev/tunX\fR] [\fB\-D\fR \fIifname\fR] [\fB\-r\fR] [\fB\-f\fR] [\fB\-l\fR \fIv4addr\fR] [\fB\-p\fR \fIport\fR] [\fB\-t\fR \fIhops\fR] [\fB-s\fR \fIv4addr\fR] [\fB-x\R \fIscript\fR]
.PP
.B 6bed4peer
[\fB\-h\fR]
.SH DESCRIPTION
.PP
The \fB6bed4peer\fR creates an instant-on, zero-config IPv6
communication facility.  It is designed to work behind NAT and
firewalls, and to provide mechanisms for direct connections
between peers.  Peering is reliable as long as a \fB6bed4router\fR
is considered a possible fallback.  This fallback is required to
cover the possibility of NAT traversal failure, which cannot be
completely avoided under IPv4.
.PP
The command provides a 6bed4 tunnel interface, with an IPv6
address range.  The ranges have a /114 prefix and are therefore
unsuitable for autoconfiguration, but DHCPv6 may be used to supply
up to 65,535 address to hosts on a local network.  Traffic directed
to this interface is wrapped in UDP and IPv4 and forwarded to
either another \fB6bed4peer\fR or the upstream \fB6bed4router\fR
server.
.SH OPTIMAL ROUTING
The \fB6bed4peer\fR aims for optimal routing for all packets.
The reliance on a servers running the \fB6bed4router\fR as a 
public facility ensures that IPv6 connectivity is available
anytime and anywhere, but it is often possible to establish
direct connections between \fB6bed4peer\fR processes.  Given
the variability of NAT however, this cannot be guaranteed in
general and, as a result, a fallback mechanism through a
\fB6bed4router\fR is required to guarantee reliable IPv6
traffic.
.PP
The structure of a 6bed4 IPv6 address is such that it
reveals a host's public IPv4 address and an external UDP port used
for the 6bed4 tunneling protocol.  This information can be used to
derive both local addressing information, as well as remote.  This
will only work for addresses that start with the standard prefix
under which 6bed4 addresses are created.
.PP
When attempting to find a direct route to a peer,
the \fB6bed4peer\fR will try to connect to the peer's externally
visible UDP port at its public IPv4 address.  Initially it will
send Probe messages while still passing traffic through the
\fB6bed4router\fR.  When a Probe message arrives at the peer
it will pass a "Seen" flag along with any return traffic, as
an indication that direct traffic is indeed possible for a
well-defined period after this flagging.  The same process is
conducted in the opposite direction, and the best possible
result is that two peers end up connecting directly, while
keeping their holes in NAT open so they can continue to do so.
By this time, traffic no longer passes through the \fB6bed4router\fR
service.
.PP
Choices between a few alternative peering policies is based on the
Traffic Class in the IPv6 header; the default (under Class Selector 0)
is to use the \fB6bed4router\fR when required for reliability of
the traffic flow and use direct \fB6bed4peer\fR connectivity when
this has been shown to also work reliably.  Class Selector 2 always
routes through the \fB6bed4router\fR, Class Selector 4 tries for
a few seconds to pass traffic directly and then falls back to the
\fB6bed4router\fR if this appears to fail; Class Selector 6 does
the same but never falls back to the \fB6bed4router\fR and instead
prefers to return ICMPv6 errors.  The choice between these peering
policies can be made for individual frames and is influenced by a
developing state maintained in the \fB6bed4peer\fR.  Odd values
for Class Selectors are not generally specified, but traffic
passed in the 6bed4 network and arriving over a 6bed4 tunnel
reveal these Traffic Class values with 1 added to indicate that
the sending \fB6bed4peer\fR instance observed successful direct
traffic from the receiving UDP port and IPv4 address.
.SH SPECIAL CASES
A system with access to native IPv6 can alos use 6bed4, although
it should not setup a default route over it.  The reason for using
6bed4 next to native IPv6 is twofold: At first it unloads the public server from
having to make the connection, and secondly it makes the connection
between the local and remote host as direct as is possible over
IPv4.  The mixed setup of native IPv6 and 6bed4 will not lead to
any trouble, as 6bed4 traffic is easily recognised by the target
address prefix, and the interface is setup to handle this.
.PP
It is possible to allocate a fixed 6bed4 address for a server, and
publish it in DNS.  Just as constant could be the IPv4 address
and UDP port assigned to the \fB6bed4peer\fR, because most NAT and
firewalls support port forwarding; the \fB\-p\fR option on the client
can be used to support reception of incoming 6bed4 traffic on the
forwarded port.
.SH OPTIONS
.TP
\fB\-d\fR \fI/dev/tunX\fR
.TP
\fB\-\-tundev\fR \fI/dev/tunX\fR
Instead of creating a tunnel for the duration that \fB6bed4server\fR runs,
use one that already exists and that has already been setup with
the proper IPv6 prefix.  This option makes it possible for
non-root users to run \fB6bed4server\fR.  All that is required is acccess to
the tunnel device by the user that runs \fB6bed4server\fR.  Optional on Linux.
.TP
\fB\-D\fR \fIifname\fR
.TP
\fB\-\-linkdev\fR \fiifname\fR
Override the default interface name 6bed4 with some other name.
This may be useful to run multiple \fB6bed4router\fR and/or
\fB6bed4peer\fR processes on the same machine.
.TP
\fB\-r\fR
.TP
\fB\-\-default\-route\fR
Create a default route through the 6bed4 interface.  This means that the
entire IPv6 Internet can be accessed through the 6bed4 interface.  This is
not setup by default, as 6bed4 might also be used as an add-on interface
that connects more directly to other 6bed4 hosts.
.TP
\fB\-l\fR \fIv4addr\fR
.TP
\fB\-\-listen\fR \fIv4addr\fR
Listen for 6bed4 messages on the specified IPv4 address.  This will also
be the address from which the traffic is sent.  This setting may be
used together with \fB\-p\fR to control the daemon's behaviour such that
it can be the target of a port forwarding setup in NAT or firewall.
.TP
\fB\-p\fR \fIport\fR
.TP
\fB\-\-port\fR \fIport\fR
Let the 6bed4 daemon listen to the given UDP port.  This will also be
the port from which the traffic is sent.  This setting may be used
together with \fB\-l\fR to control the daemon's behaviour such that it
can be the target of a port forwarding setup in NAT or firewall.
.TP
\fB\-s\fR
.TP
\fB\-\-v4server\fR \fIv4addr\fR
Use the given IPv4 address as the fallback 6bed4 server instead of the
default public server.  This is an experimental facility provided by
SURFnet.nl and they have very solid networking infrastructure.  Due to
limited management, it may still not have 100% uptime, not until it
develops into an anycast solution at the same address, 145.136.0.1.
.PP
Please talk to us if you want to engage in this as another anycast
partner.  Also let us know if the service is consistently down during
its experimental phase.  Until it is globally run in anycast mode it
cannot always be assumed to be online; sorry about that; but that is
why we have this option.  We are aiming for a managed form of service
at the default IPv4 address.
.TP
\fB\-f\fR
.TP
\fB\-\-foreground\fR
.TP
\fB\-\-fork\-not\fR
Do not fork to the background.  Instead, stay on the foreground and listen
to break signals.  This is primarily useful for testing, including while
rolling out 6bed4 on a site.
.TP
\fB\-e\fR
.TP
\fB\-\-error\-console\fR
Normally, debugging messages are sent tot syslog.  With this setting, the
messages are instead printed to the console.
On systems supporting the non-POSIX syslog extension LOG_PERROR, the output will be sent to stderr.
On systems without that extension, the system console is used.
.TP
\fB\-k\fR \fItime\fR[,\fIreach\fR]
.TP
\fB\-\-keepalive \fItime\fR[,\fIreach\fR]
This setting defines the keepalives sent to the Public 6bed4 Service.
The \fItime\fR indicates the number of seconds between keepalives, the
\fIreach\fR indicates the TTL to use on the traffic where supported;
it is only needed to get outside NAT and firewalls, but not to reach
the central infrastructure.  The default is \fB\-\-keepalive 30,3\fR
and may be automatically determined in future versions.
.TP
\fB\-t\fR \fIhops\fR
.TP
\fB\-\-ttl\fR \fIhops\fR
Set the multicast radius to the given number of hops, the default being 1.
This number is used as the TTL on multicast messages, thereby determining
whether routers are permitted to forward these packets.  The value 0
indicates that no multicast should be used.  Values above 1 run the risk
of deregulating the performance of 6bed4 on an unsuitable network, please
read \fBLOCAL NETWORKING\fR below for details.
.TP
\fB-x\fR \fIextension.script\fR
.TP
\fB\-\-extension\-hook\fR \fIextension.script\fR
Sets a script that will be invoked on major tunnel changes.  The script
will not be forked, so it might disrupt traffic.  Changes include the
addition of a prefix that was offered, the addition of a route that was
offered, and the removal of all offers; the change processing is scoped
by an identifier, for which the \fB6bed4peer\fR uses its own process
identifier.  Only proper teardown of the tunnel will invoke the script
for removal of offerings.  For more hands-on information, look for the
skeleton script included with 6bed4.
.PP
To be sure that a cleanup hook is run when \fB6bed4peer\fR exits, it
must be sent a HUP, KILL or TERM signal.  In addition, the keyboard can
send INT when Control-C is pressed.
.TP
\fB\-u\fR
.TP
\fB\-\-udp-variability\fR
TODO - argue, and maybe implement.
Accept variations in remote UDP ports when comparing 6bed4 address with
each other, or with an IPv6 address.  This reduces the security of the
tunnel mechanism by permitting different processes, and possibly different
users, on the remote end to take over from each other.  It also means that
remote symmetric routers stand a chance of contacting this node over direct
peering traffic.  This option is not helpful if the local node is a
symmetric router; and if both peers run a symmetric router then there is
never going to be direct traffic between the peers.
.PP
This option sets up support for remote peers that run a NAT router that is
inherently unsuitable for peer-to-peer traffic.  The default setup is not
kind to those routers, but it sends a much clearer signal about the origin
of the problems, namely at the symmetric NAT router.  Being able to
pinpoint the cause of a problem is probably more helpful than trying to
deal with a few situations but fail on certain connections, where each
end concludes that the other end must be at fault because direct connections
only fail with that other party.
.SH BUGS
.SH AUTHOR
\fB6bed4peer\fR was written by Rick van Rein from OpenFortress.
It was created to support the 0cpm project and later taken up in
the InternetWide Architecture.
