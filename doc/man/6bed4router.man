.TH 6BED4 8 "Februari 1, 2011"
.\" Please adjust this date whenever revising the manpage.
.\"
.\" Some roff macros, for reference:
.\" .nh        disable hyphenation
.\" .hy        enable hyphenation
.\" .ad l      left justify
.\" .ad b      justify to both left and right margins
.\" .nf        disable filling
.\" .fi        enable filling
.\" .br        insert line break
.\" .sp <n>    insert n+1 empty lines
.\" for manpage-specific macros, see man(7)
.SH NAME
6bed4router \- server-side daemon for 6bed4 service
.SH SYNOPSYS
.B 6bed4router
[\fB\-d\fR \fI/dev/tunX\fR] [\fB\-D\fR \fIifname\fR] \fB\-l\fR \fIv4addr\fR [\fB\-r\fR \fIv4router\fR] [\fB-n\fR \fInetid\fR] [\fB\-L\fR \fIv6prefix/len\fR] [\fB-x\fR \fIport\fR] [\fB-X\fR \fIv4addr\fR|\fIv6addr\fR] [\fB-R\fR fv6prefix/len\fR...] [\fB\-m\fR/\fB\-i\fR/\fBs\fR/\fBt\fR/\fBu\fR \fI...\fR]
.PP
.B 6bed4router
[\fB\-h\fR]
.SH DESCRIPTION
.PP
The \fB6bed4router\fR is a daemon that provides IPv6 tunnels over UDP and
IPv4.  Servers can use it to share a standard /64 prefix and routing
to native IPv6 ranges, or a server might be setup to just share access
to a local /64 prefix without offering routing to the entire Internet.
.PP
The purpose of the 6bed4 tunnel system is to allow hosts and even
single applications to ensure access to IPv6, and delegate any concern
for constraints imposed by IPv4 and NAT.  With 6bed4, anyone can have
IPv6 anywhere and, as a result, applications can make a local choice
to implement IPv6-only logic and rely on 6bed4 to support IPv6 on
any peers whose network environment only supports IPv4.
.PP
Every tunnel client receives a range of 65,535 addresses to choose from,
which it may possibly share locally through DHCPv6 and routing.  The
tunneled range includes the client's IPv4 address and UDP port as it
appears to the \fB6bed4router\fI as part of the lower address half.
The ease with which UDP ports are available to applications
translates to the ease of setting up a new tunnel, even if it is
only for use within a single application.
.PP
6bed4 clients under the same /64 prefix or under a standard 6bed4 prefix
TBD1::/32 or fc64::/16 may be assumed by 6bed4 clients to share the same
lower-half structure, which can be used for direct peering between 6bed4
clients (which are therefore referred to as 6bed4 peers).  As long as the
\fB6bed4router\fR is used as a fallback option, the 6bed4 network can
reliably support peer-to-peer connections.
.PP
These tunnels are suitable for mobile and embedded devices, to assist them
in instant changeover from being IPv4-only to having certain access to
IPv6 and able to assume the same for communication peers.  Given the
restricted resources in embedded applications, this is likely to improve
the speed of transitioning to IPv6.
.PP
The public-use aspect of this profile means that there is no requirement for
clients to register.  However, this does not mean that the use of \fB6bed4router\fR
makes it possible to browse IPv6 networks anonymously; abusive behaviour
can be traced back to the source through the IPv4 address and UDP port
of the tunnel client as included in the IPv6 address.  See ADDRESS FORMAT
below for details, and SECURITY CHECKS for precautions against abuse.
.SH OPTIONS
.TP
\fB\-d\fR \fI/dev/tunX\fR
.TP
\fB\-\-tundev\fR \fI/dev/tunX\fR
Instead of creating a tunnel for the duration that \fB6bed4router\fR runs,
use one that already exists and that has already been setup with
the proper IPv6 prefix.  This option enables non-root users to run
their own \fB6bed4router\fR service.  All that is required is acccess to
the tunnel device by the user that runs \fB6bed4router\fR.
.TP
\fB\-D\fR \fIifname\fR
.TP
\fB\-\-linkdev\fR \fIifname\fR
Override the default interface name \fI6bed4\fR with the specified name.
This may be useful to run multiple \fB6bed4router\fR and/or
\fB6bed4peer\fR processes on the same machine.  One reason for this may
be to serve 6bed4 to multiple IPv4 addresses, possibly with different
prefixes and routing properties.
.TP
\fB\-l\fR \fIv4addr\fR
.TP
\fB\-\-v4listen\fR \fIv4addr\fR
Bind to the given local IPv4 address and listen for incoming IPv6
neighbour discovery packages as well as general IPv6 traffic.  Required.
.TP
\fB\-r\fR \fIv4router\fR
.TP
/fB\-\-v4router\fR \fIv4addr\fR
Provide an upstream IPv4 router, to be used when this \fB6bed4router\fR
is not publicly accessible on the \fB-l\fR IPv4 address with the
TBD2 UDP port.  When constructing or expanding the \fB-L\fR prefix,
the \fB-r\fR address is used in the top half instead of the \fB-l\fR
address that is primarily meant to appear in the lower address half.
.TP
\fB\-n\fR \fInetid\fR
.TP
\fB\-\-netid\fR \fInetid\fR
The network identifier is a 16-bit number in hexadecimal notation
(with no prefixes like 0x) that may be used to complete some prefixes
in the \fB-L\fR option to 64 bits.  If it is provided but not used,
an error is given.  If it is used but not provided, the default value
\fIbed\fR is used.
.TP
\fB\-L\fR \fIv6prefix/len\fR
.TP
\fB\-\-v6prefix\fR \fIv6prefix/len\fR
Bind to the given IPv6 address range through a tunnel device, and
forward incoming IPv6 messages to IPv4-based UDP tunnel endpoints.
See ADDRESS FORMAT below for an explanation of the lower half of
the IPv6 addresses.
.IP
When this option is not provided, a default is used.  Given the presence
of at least one `-R` option, the default form `fc64::/16` is used.
Future versions may instead default to `TBD1::/32`, possibly depending
on other options.
.IP
When \fIlen\fR is not 64, it must be 16, 32 or 48 and will be rewritten
to a /64 form by the \fB6bed4router\fR.  Specifically, the form \fIfc64::/16\fR
is extended to \fIfc64:<netid>:<ipv4addr>::/64\fR using the network identifier
from the \fB-n\fR option and the IPv4 address from the \fB-r\fR option or,
lacking that, the \fB-l\fR option.  The form \fITBD1::/32\fR is extended
to \fITBD1:<ipv4addr>::/64\fR with that same IPv4 address.  The form
\fIfd00::/len\fR can use any \fI/len\fR values from 8 to 64; random bits
are added to reach a 64 bit prefix length; the generation of these bits
is repeatable, so the bits survive a restart.  The forms that are defined
for general unicaast may have a \fI/48\fR prefix length, in which case the
completion to a 64 bit prefix is reached by adding the same \fI<netid>\fR
as mentioned before.
.IP
If no \fB\-d\fR option is given, a tunnel will be created for the time that
\fB6bed4router\fR is running, and the (extended) \fIv6prefix/64\fR is used
as a router address on that interface.  Routing table entries will not be
setup by \fB6bed4router\fR, nor will the general ablity to forward IPv6
traffic.
.TP
\fB\-R\fR \fIv6prefix/len\fR
.TP
\fB\-\-v6route\fR \fIv6prefix/len\fR
Supply additional routing information.  Unlike the \fB-L\fR option
which indicates the prefix to the 6bed4 network, this option does
not support prefix construction or polling for neighbour presence.
It is solely meant to enrich routing tables with prefixes that may
pass through the 6bed4 interface.  Prefix lengths are more flexible
than \fB-L\fR can be.
.IP
Multiple of these prefixes may be supplied, and each of them will
be routed to the \fB6bed4router\fR backend network.  Such traffic
will use the \fB-L\fR prefix as source addressing and the \fB-R\fR
prefix as target addressing.  Return traffic will reverse this, and
that is why a central concern is that the networks provided with
the \fB-R\fR option must support routing back to the \fB6bed4router\fR
through its prefix.  Note that not all prefixes supported with
\fB-L\fR are globally routable, so this is a bit of a concern.
.IP
A good use of this pattern is to use a locally routable prefix based on
\fB-L\fR \fIfc64:bed:<ipv4addr>::/64\fR to reach a server's publicly
routable prefix set with \fB-R\fR.  The server can distinguish the
traffic from the \fB6bed4router\fR on the basis of the source address,
and will be able to direct reply traffic to it; at the same time,
native IPv6 clients can use their own prefixes and be routed through a
primary interface.
.TP
\fB\-m\fR \fIv6addr\fR
.TP
\fB\-\-masqhost\fR \fIv6addr\fR
Use the given IPv6 address as the target for masqueraded operation.  This
means that any ports setup with \fB\-\-sctp\fR, \fB\-\-tcp\fR or \fB\-\-udp\fR
are forwarded
to this address when addressed on the client's idea of the 6bed4 router
address.  Note that the router only uses a few ICMPv6 messages, so the use
of these additional ports can benefit various applications, ranging from
authentication services to honeypots.
.TP
\fB\-s\fR \fIport\fR[\fB:\fIport\fR]
.TP
\fB\-t\fR \fIport\fR[\fB:\fIport\fR]
.TP
\fB\-u\fR \fIport\fR[\fB:\fIport\fR]
.TP
\fB\-sctp\fR \fIport\fR[\fB:\fIport\fR]
.TP
\fB\-tcp\fR \fIport\fR[\fB:\fIport\fR]
.TP
\fB\-udp\fR \fIport\fR[\fB:\fIport\fR]
Apply the masquerading port or port range for SCTP, TCP or UDP to the
masquerading host set by the last preceding \fB\-\-masqhost\fR option,
or use the tunnel's IPv6-side address
if none was set yet.  Ports that have been previously bound will not make it,
so it is possible to first relay a port with \fB\-t 22\fR to a service and
then pass the rest to a honeypot with \fB\-t 1:65535\fR.
.TP
\fB\-i\fR
Apply masquerading for ICMPv6 to the masquerading host set by the last
preceding \fB\-\-masqhost\fR option, or use the tunnel's IPv6-side address
if none was set yet.  The ICMPv6 messages with a special meaning to 6bed4
are exempted (Router and Neighbor Solicitation and Advertisement, as well
as Redirect) but other messages will be masqueraded.
.TP
\fB\-x\fR \fIport\fR
.TP
\fB\-\-xlate-port\fR \fIport\fR
Assume translation from the acting \fB\-l\fR address/port combination
over which UDP traffic with encapsulated IPv6 arrives.  Translation
can be used as a firewall rule to bypass 6bed4 traffic from a stream
that is otherwise treated elsewhere, such as via NAT64.  Bypassing is
done to the \fB-X\fR address or otherwise to 127.0.0.1
and the given \fIport\fR, to which the \fB6bed4router\fR
will bind intead of the normal UDP port.  Note: The \fB\-l\fR option
must still be set, as it is published in link-local addresses inside
ICMPv6 messages.
.TP
\fB\-X\fR \fIv4addr\fR
.TP
\fB\-\-xlate-addr\fR \fIv4addr\fR
.TP
\fB\-X\fR \fIv6addr\fR
.TP
\fB\-\-xlate-addr\fR \fIv6addr\fR
Assume that the externally visible \fB\-l\fR address/port combination
is not locally available, and that the traffic is directed to another
address via some form of NAT.  The argument gives a local address to
bind to, while keeping up the appearance of \fB\-l\fR to the outside
network.  Combine this with \fB\-x\fR if the port has also changed.
.PP
The use of an IPv6 address on the UDP/IPv4 side of the 6bed4 tunnel
allows a surprisingly natural combination with NAT64.  Instead of
using firewall rules to "steal" the UDP port for 6bed4 through
redirection to a local address/port combination, it would also be
possible to first let NAT64 do its translation and then to bind on
the IPv6 side.  This simplification comes at the price of a longer
path to the \fB6bed4router\fR, especially when NAT64 is followed
by NAT66 to perform port forwarding.  The extra components delay
the processing of 6bed4 and increase the risk of service failure.
.TP
\fB\-h\fR
.TP
\fB\-\-help\fR
Print usage information and exit.
.SH ADDRESS FORMAT
.PP
An IPv6 address used from \fB6bed4router\fR reveals the IPv4 address and UDP port
over which the \fB6bed4peer\fR process connects.  This indicates a single process
and a single application on customary platforms.  The address format is checked
when UDP/IPv4 traffic enters 6bed4 nodes, and the same format is used
to reconstruct the UDP/IPv4 information to which 6bed4 relays traffic to the
peer.
.PP
The upper half of a 6bed4 address consists of a prefix, which may be either
`fc64::/16` followed by a 16-bit network identitier or `TBD1::/32`; both are
followed by the IPv4 address of the \fB6bed4router\fR process, with service
at UDP port TBD2.  This forms a /64 prefix.  Other mechanisms, including
native routes and local unique addresses may form a different /64, but
those do not allow derivation of the \fB6bed4router\fR address.  The ability
to learn about this contained IPv4 address is that peers can try to connect
directly across \fB6bed4router\fR instances; use cases are media streams
or other peer-to-peer applications between individually owned 6bed4 subnets.
.PP
The lower half of any prefix of a \fB6bed4peer\fR consists of an IPv4 address
and a UDP port, except that the lower 2 bits of the first byte of the IPv4
address are overwritten by meaningful flags in their position, so these two
bits are placed after the UDP port.  This makes up a /114 prefix that will
be supplied to tunnel clients.  The remaining 14 bits offer 65,535 addresses
free for use by this \fB6bed4peer\fR, with only value 0 reserved to identify
the \fB6bed4router\fR to which it connects.
.PP
In order to attempt direct peering, a \fB6bed4peer\fR needs to know if the
peer is supplied with a 6bed4 address.  This is the case under the standard
prefixes `fc64::/16` and `TBD1::/32` as well as the same /64 prefix under
which the \fB6bed4peer\fR resides.  Note that direct peering can be tried
even between peers that reside under different \fB6bed4router\fR servers.
.PP
Due to the IPv6 practice of assigning link-local names composed of \fBfe80::\fR
and the \fIinterfaceidentifier\fR, the router-side of a tunnel can always
be addressed as \fBfe80::\fR and clients can be found at link-local addresses
ranging from \fBfe80::1\fR to \fBfe80::ffff\fR.
.SH SECURITY CHECKS
.PP
Not everything will pass through \fB6bed4router\fR, even if this would be
technically possible.  A few security checks are applied to silently
drop traffic that looks evil.  Detected changes in NAT mapping are
not considered evil, and are actively corrected with a Router
Advertisement message.
.PP
Packets should be long enough to at least contain the IPv6 traffic
and a minimal payload size.  Also, it should not exceed a predefined
MTU of 1280 bytes for IPv6.
.PP
IPv6 traffic uploaded through the IPv4 side should reveal the proper
IPv4 settings in the IPv6 source address, as specified under
ADDRESS FORMAT above.  This is basically the tunnel aspect of BCP 38
egress filtering.
.PP
Tunnel commands should adhere to the format of RFC 5722 and may not
contain any NUL characters.
.SH BUGS
Currently, \fB6bed4router\fR does not use ICMP notifications at the IPv4
level to provide smart feedback to an IPv6 client.  It is undecided
at this point if this would add value.
.PP
The \fB6bed4router\fR daemon is a piece of highly efficient code,
and it should be able to handle very high bandwidths.  A stress
test has not been conducted yet.
.SH LICENSE
Released under a BSD-style license without advertisement clause.
.SH SEE ALSO
The work on 6bed4 started under the 0cpm project, where its
intention was specific to SIP-negotiated media streams.  The
InternetWide Architecture subsequently adopted it as a general
facilitation mechanism for peer-to-peer protocols that could
include users on IPv4-only networks.
.PP
http://0cpm.org/ \- the homepage of the 0cpm project.
.PP
http://devel.0cpm.org/6bed4/ \- the homepage of \fB6bed4\fR.
.PP
http://internetwide.org/blog/2020/05/31/net-1-ipv4-backward-compat.html
\- InternetWide use of 6bed4 and NAT64 in unison; NAT64 services IPv4-only
users who only use client-server applications and 6bed4 enables the
additional use of peer-to-peer applications.
.SH AUTHOR
\fB6bed4router\fR was written by Rick van Rein from OpenFortress.
It was created to support the 0cpm project and was subsequently
taken on board for the InternetWide Architecture.
